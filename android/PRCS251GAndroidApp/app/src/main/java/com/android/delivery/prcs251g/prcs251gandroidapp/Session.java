/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author luke/brian/adam
 */
public class Session 
{
    private String username;
    private String jobTitle;
    private String employeeID;
    private String orderID;
    private String price;
    private String customerCity;
    private int currentPosition;
    private static Session instance = new Session();
    
    private Session(){}
    
    public static Session getInstance()
    {
        return instance;
    }

    public void ClearCurrent()
    {
        this.username = null;
        this.jobTitle = null;
        this.employeeID = null;
        this.customerCity = null;
        this.price = null;
        this.currentPosition = 0;
        this.orderID = null;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getOrderID() {
        return orderID;
    }
    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public int getPosition()
    {
        return this.currentPosition;
    }

    public void setPosition(int position)
    {
        this.currentPosition = position;
    }
}
