package com.android.delivery.prcs251g.prcs251gandroidapp;

import java.util.Arrays;

/**
 *
 * @author bvyoung
 */
public class JSONString 
{
    private String json = "{";
    private int elements;
    
    public JSONString()
    {}
    
    public void SetJSON(String json)
    {
        this.json = json;
    }
    public String GetJSON()
    {
        Complete();
        return this.json;
    }
    public void SetElements(int elements)
    {
        this.elements = elements;
    }
    public int GetElements()
    {
        return this.elements;
    }
    public void AddElement(String key, String value)
    {
        if(elements != 0)
        {
            json = json.concat(",");
        }
        json = json.concat("\"" + key + "\"" + ":" + "\"" + value + "\"");
        this.elements++;
    }
    public void AddElement(String key, int value)
    {
        if(elements != 0)
        {
            json = json.concat(",");
            json = json.concat("\"" + key + "\"" + ":" + value);
        this.elements++;
        }
    }
    public void AddElement(String key, long value)
    {
        if(elements != 0)
        {
            json = json.concat(",");
            json = json.concat("\"" + key + "\"" + ":" + value);
        this.elements++;
        }
    }
    public void AddElement(String key, double value)
    {
        if(elements != 0)
        {
            json = json.concat(",");
            json = json.concat("\"" + key + "\"" + ":" + value);
        this.elements++;
        }
    }
    public void AddElement(String key, String[] value)
    {
        if(elements != 0)
        {
            json = json.concat(",");
            json = json.concat("\"" + key + "\"" + ":" + Arrays.toString(value));
        this.elements++;
        }
    }
    public void AddElement(String key, int[] value)
    {
        if(elements != 0)
        {
            json = json.concat(",");
            json = json.concat("\"" + key + "\"" + ":" + Arrays.toString(value));
        this.elements++;
        }
    }
    public void AddElement(String key, double[] value)
    {
        if(elements != 0)
        {
            json = json.concat(",");
            json = json.concat("\"" + key + "\"" + ":" + Arrays.toString(value));
        this.elements++;
        }
    }
    private void Complete()
    {
        json = json.concat("}");
    }
}
