package com.android.delivery.prcs251g.prcs251gandroidapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;

public class SelectedOrder extends AppCompatActivity implements OnClickListener
{
    Session sess = Session.getInstance();
    TextView textViewOrderID = null;
    TextView textViewPaymentType = null;
    TextView textViewPrice = null;
    TextView textViewAddress1 = null;
    TextView textViewAddress2 = null;
    TextView textViewCity = null;
    TextView textViewPostcode = null;
    TextView textViewMobileNum = null;
    Button btnConfirm = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textViewOrderID = (TextView) findViewById(R.id.textViewOrderID);
        textViewPaymentType = (TextView) findViewById(R.id.textViewPaymentType);
        textViewPrice = (TextView) findViewById(R.id.textViewPrice);
        textViewAddress1 = (TextView) findViewById(R.id.textViewAddress1);
        textViewAddress2 = (TextView) findViewById(R.id.textViewAddress2);
        textViewCity = (TextView) findViewById(R.id.textViewCity);
        textViewPostcode = (TextView) findViewById(R.id.textViewPostcode);
        textViewMobileNum = (TextView) findViewById(R.id.textViewMobileNum);
        btnConfirm = (Button) findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.btnConfirm)
        {
            SelectOrder();
        }
    }

    private void SelectOrder()
    {
        Intent intent = new Intent(this, MapWithSelectedOrder.class);
        startActivity(intent);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        try
        {
            Intent parent = getIntent();
            String orderId = parent.getExtras().getString("OrderID");
            String paymentMethod = parent.getExtras().getString("Payment");
            String price = parent.getExtras().getString("Price");
            String address1 = parent.getExtras().getString("Address1");
            String address2 = parent.getExtras().getString("Address2");
            String postcode = parent.getExtras().getString("Postcode");
            String mobileNum = parent.getExtras().getString("MobileNum");
            String city = parent.getExtras().getString("City");
            textViewOrderID.setText("Order ID: " + (int)Double.parseDouble(orderId));
            textViewPaymentType.setText("Payment Method: " + paymentMethod.toLowerCase());
            textViewPrice.setText("Price: £" + String.format("%.2f",Double.parseDouble(price)));
            textViewAddress1.setText("Address 1: " + address1);
            textViewAddress2.setText("Address 2: " + address2);
            textViewPostcode.setText("Postcode: " + postcode);
            textViewMobileNum.setText("Mobile Num: " + mobileNum.substring(0, mobileNum.length()-2));
            textViewCity.setText("City: " + city);
            sess.setOrderID(orderId);
            sess.setCustomerCity(city);
            sess.setPrice(price);
        }catch(Exception ex)
        {

        }
    }


}
