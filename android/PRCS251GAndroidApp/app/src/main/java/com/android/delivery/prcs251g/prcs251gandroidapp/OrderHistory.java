package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Luke/Brian/Adam
 */
public class OrderHistory
{
    private String orderHistoryId;
    private String price;
    private String time;
    
    public OrderHistory(String orderHistoryId, String price, String time)
    {
        this.orderHistoryId = orderHistoryId;
        this.price = price;
        this.time = time;
    }

    public String getOrderHistoryId() {
        return orderHistoryId;
    }

    public void setOrderHistoryId(String orderHistoryId) {
        this.orderHistoryId = orderHistoryId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
    
    
}
