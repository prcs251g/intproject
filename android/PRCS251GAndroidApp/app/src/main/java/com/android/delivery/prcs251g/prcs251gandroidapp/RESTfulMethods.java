/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;

/**
 *
 * @author bvyoung
 */
public class RESTfulMethods 
{
    String urlString;
    
    // Constructors and get/set methods
    public RESTfulMethods()
    {
    }
    public RESTfulMethods(String urlString)
    {
        this.urlString = urlString;
    }
    public String getUrlString()
    {
        return this.urlString;
    }
    public void setUrlString(String urlString)
    {
        this.urlString = urlString;
    }
    
    // RESTful method implementations
    
    public String GET()
    {
        String output = "";
        try
        {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200)
            {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            output = br.readLine();
            conn.disconnect();
            return output;
        }catch(MalformedURLException e)
        {}catch(IOException e)
        {}
        return output;
    }
    public String GET(String resourceIdentifier)
    {
        String output = "";
        try
        {
            URL url = new URL(urlString.concat("/" + resourceIdentifier));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");
            if (conn.getResponseCode() != 200)
            {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            output = br.readLine();
            conn.disconnect();
            return output;
        }catch(MalformedURLException e)
        {}catch(IOException e)
        {}
        return output;
    }
    public void POST(String payload)
    {
        try
        {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            os.write(payload.getBytes());
            os.flush();
            if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) 
            {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            conn.disconnect();
        }catch(MalformedURLException e)
        {}catch(IOException e)
        {}
    }
    public void DELETE(String resourceIdentifier)
    {
        try
        {
            URL url = new URL(urlString.concat("/" + resourceIdentifier));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            if (conn.getResponseCode() != 200 && conn.getResponseCode() != 204) 
            {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            conn.disconnect();
        }catch(MalformedURLException e)
        {}catch(IOException e)
        {}
    }
    public void PUT(String resourceIdentifier, String payload)
    {
        try
        {
            URL url = new URL(urlString.concat("/" + resourceIdentifier));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("PUT");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            osw.write(String.format(payload));
            osw.flush();
            osw.close();
            if (conn.getResponseCode() != 200 && conn.getResponseCode() != 204) 
            {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            conn.disconnect();
        }catch(MalformedURLException e)
        {}catch(IOException e)
        {}
    }
}
