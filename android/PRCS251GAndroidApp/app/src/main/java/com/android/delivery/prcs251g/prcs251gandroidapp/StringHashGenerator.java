/*
 *
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

import android.os.AsyncTask;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.io.UnsupportedEncodingException;

/**
 * Hash generator.
 * @author Brian young
 */
public class StringHashGenerator 
{
    
    /**
     * Hashes an inputted string using SHA-1
     * @param inputToHash Desired string to hash
     * @return Returns the hashed input string
     */
    public String SHA1(String inputToHash)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.reset();
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using SHA-1 with a salt
     * @param inputToHash Desired string to hash
     * @param salt Salt string used in the hashing process
     * @return Returns the hashed input string
     */
    public String SHA1(String inputToHash, String salt)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.reset();
            md.update(salt.getBytes("UTF-8"));
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using SHA-256
     * @param inputToHash Desired string to hash
     * @return Returns the hashed input string
     */
    public String SHA256(String inputToHash)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.reset();
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using SHA-256 with a salt
     * @param inputToHash Desired string to hash
     * @param salt Salt string used in the hashing process
     * @return Returns the hashed input string
     */
    public String SHA256(String inputToHash, String salt)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.reset();
            md.update(salt.getBytes("UTF-8"));
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using SHA-384
     * @param inputToHash Desired string to hash
     * @return Returns the hashed input string
     */
    public String SHA384(String inputToHash)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-384");
            md.reset();
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     *
     * Hashes an inputted string using SHA-384 with a salt
     * @param inputToHash Desired string to hash
     * @param salt Salt string used in the hashing process
     * @return Returns the hashed input string
     */
    public String SHA384(String inputToHash, String salt)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-384");
            md.reset();
            md.update(salt.getBytes("UTF-8"));
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using SHA-512
     * @param inputToHash Desired string to hash
     * @return Returns the hashed input string
     */
    public String SHA512(String inputToHash)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.reset();
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     *
     * Hashes an inputted string using SHA-512 with a salt
     * @param inputToHash Desired string to hash
     * @param salt Salt string used in the hashing process
     * @return Returns the hashed input string
     */
    public String SHA512(String inputToHash, String salt)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.reset();
            md.update(salt.getBytes("UTF-8"));
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using MD2
     * @param inputToHash Desired string to hash
     * @return Returns the hashed input string
     */
    public String MD2(String inputToHash)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD2");
            md.reset();
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }

    /**
     * Hashes an inputted string using MD2 with a salt
     * @param inputToHash Desired string to hash
     * @param salt Salt string used in the hashing process
     * @return Returns the hashed input string
     */
    public String MD2(String inputToHash, String salt)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD2");
            md.reset();
            md.update(salt.getBytes("UTF-8"));
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using MD5
     * @param inputToHash Desired string to hash
     * @return Returns the hashed input string
     */
    public String MD5(String inputToHash)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
                hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }
    
    /**
     * Hashes an inputted string using MD5 with a salt
     * @param inputToHash Desired string to hash
     * @param salt Salt string used in the hashing process
     * @return Returns the hashed input string
     */
    public String MD5(String inputToHash, String salt)
    {
        String hashString = "";
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(salt.getBytes("UTF-8"));
            byte[] buffer = inputToHash.getBytes("UTF-8");
            md.update(buffer);
            byte[] digest = md.digest();
            for(int i = 0; i < digest.length; i++)
            {
            hashString += Integer.toString( (digest[i] & 0xff ) + 0x100, 16).substring(1);
            }
        }catch(NoSuchAlgorithmException | UnsupportedEncodingException e)
        {}
        return hashString;
    }

    /**
     * Created by bvyoung on 10/05/2017.
     */

    public static class APICompleteOrder
    {
        String orderID;
        String price;
        private WebServiceTask task = null;

        public APICompleteOrder(String orderID, String price)
        {
               this.orderID = orderID;
               this.price = price;
        }

        public void start()
        {
            try
            {
                task = new WebServiceTask();
                task.execute();
            }catch(Exception ex)
            {

            }
        }

        public synchronized void runConnection()
        {
            RESTfulMethods restOrders = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orders");
            RESTfulMethods restOrderHistory = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orderhistories");

            restOrders.DELETE(orderID);
            JSONString js = new JSONString();

        }

        private class WebServiceTask extends AsyncTask<Void,Void,Void>
        {
            protected Void doInBackground(Void... params)
            {
                runConnection();
                return null;
            }

            @Override
            protected void onPostExecute(Void v)
            {

            }
        }
    }
}