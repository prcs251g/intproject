/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam, Brian, Luke
 */
public class Drink {
    private String drink_calories;
    private String drink_ID;
    private String drink_name;
    private String drink_price;
    private String drink_size;

    public Drink(String drink_ID, String drink_name, String drink_size,
            String drink_price, String drink_calories) {
        this.drink_ID = drink_ID;
        this.drink_name = drink_name;
        this.drink_size = drink_size;
        this.drink_price = drink_price;
        this.drink_calories = drink_calories;   
    }

    public String getDrink_calories() {
        return drink_calories;
    }

    public String getDrink_ID() {
        return drink_ID;
    }

    public String getDrink_name() {
        return drink_name;
    }

    public String getDrink_price() {
        return drink_price;
    }

    public String getDrink_size() {
        return drink_size;
    }

    public void setDrink_calories(String drink_calories) {
        this.drink_calories = drink_calories;
    }

    public void setDrink_ID(String drink_ID) {
        this.drink_ID = drink_ID;
    }

    public void setDrink_name(String drink_name) {
        this.drink_name = drink_name;
    }

    public void setDrink_price(String drink_price) {
        this.drink_price = drink_price;
    }

    public void setDrink_size(String drink_size) {
        this.drink_size = drink_size;
    }

 
      
}
