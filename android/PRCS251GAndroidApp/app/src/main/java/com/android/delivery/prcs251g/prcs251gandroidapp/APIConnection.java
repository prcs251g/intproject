package com.android.delivery.prcs251g.prcs251gandroidapp;


import android.os.AsyncTask;

/**
 * Created by bvyoung on 10/05/2017.
 */

public class APIConnection
{
    SyncAPI sync = new SyncAPI();
    private WebServiceTask task = null;
    public APIConnection()
    {
    }

    public void start()
    {
        try
        {
            task = new WebServiceTask();
            task.execute();
        }catch(Exception ex)
        {

        }
    }

    public synchronized void runConnection()
    {
        sync.EmployeeSync();
        sync.OrderSync();
        sync.CustSync();
        sync.PaymentSync();
        sync.CitySync();
    }

    private class WebServiceTask extends AsyncTask<Void,Void,Void>
    {
        protected Void doInBackground(Void... params)
        {
            runConnection();
            return null;
        }

        @Override
        protected void onPostExecute(Void v)
        {

        }
    }

}
