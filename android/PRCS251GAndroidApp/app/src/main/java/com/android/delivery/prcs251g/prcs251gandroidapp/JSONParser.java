package com.android.delivery.prcs251g.prcs251gandroidapp;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author bvyoung
 */
public class JSONParser 
{
    private String json;
    private int elements;
    private int entries;
    private ArrayList arrKeys = new ArrayList();
    private ArrayList arrValues = new ArrayList();
    
    public JSONParser()
    {}
    public JSONParser(String json)
    {
        this.json = json;
    }
    public JSONParser(String json, int elements)
    {
        this.json = json;
        this.elements = elements;
    }
    public void Parse()
    {
        json = json.substring(1, json.length()-1);
        json = json.replace("{", "");
        json = json.replace("}", "");
        //json = json.replace(" ", "");
        ArrayList arrElements = new ArrayList<>(Arrays.asList(json.split(",")));
        
        for(int i = 0; i < arrElements.size(); i++)
        {
            String curr = (String)arrElements.get(i);
            ArrayList currElement = new ArrayList<>(Arrays.asList(curr.split(":")));
            String tempKey = (String)currElement.get(0);
            tempKey = tempKey.replace("\"", "");
            String tempValue = (String)currElement.get(1);
            tempValue = tempValue.replace("\"", "");
            if(arrKeys.contains(tempKey))
            {
                entries++;
            }else
            {
                arrKeys.add(tempKey);
            }
            arrValues.add(tempValue);
        }
        entries = entries/(arrKeys.size());
        entries++;
        elements = arrKeys.size();
    }
    public String GetKeyName(int index)
    {
        if(arrKeys.size() > index)
        {
            return arrKeys.get(index).toString();
        }
        return "";
    }
    public ArrayList GetValueArray(String key)
    {
        ArrayList arrReturn = new ArrayList();
        int j = arrKeys.indexOf(key);
        for(int i = 0; i < entries; i++)
        {
            arrReturn.add(arrValues.get(j));
            j = j + elements;
        }
        return arrReturn;
    }
    public String GetValue(String keycheck, String valuecheck, String desiredkey)
    {
        String desiredValue = "";
        String tempValue = "";
        String tempKey = "";
        int keyindex = 0;
        int desiredkeyindex = 0;
        int pairnum = arrValues.size()/elements;
        for(int i = 0; i<arrKeys.size();i++)
        {
            tempKey = (String)arrKeys.get(i);
            if(tempKey.equals(keycheck))
            {
                keyindex = i;
            }else if(tempKey.equals(desiredkey))
            {
                desiredkeyindex = i;
            }
        }
        for(int i = 0;i < pairnum ;i++)
        {
            tempValue = (String)arrValues.get(FindIndexAlgorithm(i, elements, keyindex));
            if(tempValue.equals(valuecheck))
            {
                desiredValue = (String)arrValues.get(FindIndexAlgorithm(i, elements,desiredkeyindex));
            }
        }
        return desiredValue;
    }
    private int FindIndexAlgorithm(int i, int n, int e)
    {
        int index = 0;
        index = ((i * n) + e);
        return index;
    }
    public void SetJSON(String json)
    {
        this.json = json;
    }
    public String GetJSON()
    {
        return this.json;
    }
    public void SetElements(int elements)
    {
        this.elements = elements;
    }
    public int GetElements()
    {
        return this.elements;
    }
    public int GetEntries()
    {
        return this.entries;
    }
    public void Clear()
    {
        json = "";
        elements = 0;
        arrKeys.clear();
        arrValues.clear();
    }
}
