/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam, Brian, Luke
 */
public class Payment {
    private String payment_id;
    private String payment_name;

    public Payment(String payment_id, String payment_name) {
        this.payment_id = payment_id;
        this.payment_name = payment_name;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }
    
}
