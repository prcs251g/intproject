/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam, Brian, Luke
 */
public class City {
    
    private String city_ID;
    private String city_name;
    
    City(String city_ID, String city_Name)
    {
        this.city_ID = city_ID;
        this.city_name = city_Name;
    }

    public String getCity_ID() {
        return city_ID;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_ID(String city_ID) {
        this.city_ID = city_ID;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }
 
}
