/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam/Brian/Luke
 */
public class Customers {
    private String addressLine1;
    private String addressLine2;
    private String city_ID;
    private String email;
    private String cust_ID;
    private String cust_mob_num;
    private String cust_postcode;
    private String cust_salt;
    private String forename;
    private String cust_surname;
    private String cust_username;
    private String cust_password;

    public Customers(String cust_ID, String cust_surname, String addressLine1, String addressLine2, 
            String cust_postcode, String city_ID, String cust_mob_num, String email, 
            String cust_username, String cust_password, String cust_salt, String forename) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city_ID = city_ID;
        this.email = email;
        this.cust_ID = cust_ID;
        this.cust_mob_num = cust_mob_num;
        this.cust_postcode = cust_postcode;
        this.cust_salt = cust_salt;
        this.forename = forename;
        this.cust_surname = cust_surname;
        this.cust_username = cust_username;
        this.cust_password = cust_password;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city_ID;
    }

    public void setCity(String city) {
        this.city_ID = city;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCust_ID() {
        return cust_ID;
    }

    public void setCust_ID(String cust_ID) {
        this.cust_ID = cust_ID;
    }

    public String getCust_mob_num() {
        return cust_mob_num;
    }

    public void setCust_mob_num(String cust_mob_num) {
        this.cust_mob_num = cust_mob_num;
    }

    public String getCust_postcode() {
        return cust_postcode;
    }

    public void setCust_postcode(String cust_postcode) {
        this.cust_postcode = cust_postcode;
    }

    public String getCust_salt() {
        return cust_salt;
    }

    public void setCust_salt(String cust_salt) {
        this.cust_salt = cust_salt;
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getCust_surname() {
        return cust_surname;
    }

    public void setCust_surname(String cust_surname) {
        this.cust_surname = cust_surname;
    }

    public String getCust_username() {
        return cust_username;
    }

    public void setCust_username(String cust_username) {
        this.cust_username = cust_username;
    }

    public String getCust_password() {
        return cust_password;
    }

    public void setCust_password(String cust_password) {
        this.cust_password = cust_password;
    }
    
    
}
