package com.android.delivery.prcs251g.prcs251gandroidapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CurrentOrders extends AppCompatActivity implements AdapterView.OnItemClickListener
{
    Session sess = Session.getInstance();
    ImportAPI menuItems = ImportAPI.getInstance();
    ArrayList arrOrders;
    ArrayList arrFilteredOrders = new ArrayList<>();
    ArrayList arrPayment;
    ArrayList arrCustomer;
    ArrayList arrCities;
    ListView listViewOrders;
    int orderIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_orders);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        arrOrders = menuItems.getOrder();
        arrPayment = menuItems.getPayment();
        arrCustomer = menuItems.getCustomers();
        arrCities = menuItems.getCity();
        listViewOrders = (ListView) findViewById(R.id.listViewOrders);
        filterOutOrders();
        listViewOrders.setAdapter(new OrderAdapter(this,
                R.layout.list_item_layout, arrFilteredOrders));
        listViewOrders.setOnItemClickListener(this);
    }

    private void filterOutOrders()
    {
        for(int i = 0; i < arrOrders.size();i++)
        {
            Order temp = (Order) arrOrders.get(i);
            if(temp.getStatus().equals("DELIVER"))
            {
                arrFilteredOrders.add(temp);
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        orderIndex = position;

        Order selectedOrder = (Order) arrFilteredOrders.get(position);
        String orderId = selectedOrder.getOrderID();
        String paymentMethod = "";
        for(int i = 0;i < arrPayment.size(); i++)
        {
            Payment temp = (Payment) arrPayment.get(i);
            if(temp.getPayment_id().equals(selectedOrder.getPaymentID()))
            {
                paymentMethod = temp.getPayment_name();
                paymentMethod = paymentMethod.substring(0, paymentMethod.length()-2);
            }
        }
        String price = selectedOrder.getOrderPrice();

        Customers customer = null;

        for(int i = 0; i < arrCustomer.size(); i++)
        {
            Customers temp = (Customers)arrCustomer.get(i);
            if(temp.getCust_ID().equals(selectedOrder.getCustomerID()))
            {
                customer = temp;
            }
        }
        String address1 = customer.getAddressLine1();
        String address2 = customer.getAddressLine2();
        String postcode = customer.getCust_postcode();
        String mobileNum = customer.getCust_mob_num();
        String city = "";
        for(int i = 0; i < arrCities.size(); i++)
        {
            City temp = (City) arrCities.get(i);
            if(temp.getCity_ID().equals(customer.getCity()))
            {
                city = temp.getCity_name();
            }
        }
        sess.setPosition(position);

        Intent intent = new Intent(this, SelectedOrder.class);
        intent.putExtra("OrderID", orderId);
        intent.putExtra("Payment", paymentMethod);
        intent.putExtra("Price",price);
        intent.putExtra("Address1",address1);
        intent.putExtra("Address2",address2);
        intent.putExtra("Postcode",postcode);
        intent.putExtra("MobileNum",mobileNum);
        intent.putExtra("City",city);
        startActivity(intent);
    }

    private class OrderAdapter extends ArrayAdapter<Order>
    {
        private int layoutID = 0;
        public OrderAdapter(Context context, int layoutID,
                            List<Order> objects)
        {
            super(context,layoutID,objects);
            this.layoutID = layoutID;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent)
        {
            if(view == null)
            {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(layoutID, null);

            }

            TextView textViewId = (TextView) view.findViewById(R.id.textViewId);
            TextView textViewPayment = (TextView) view.findViewById(R.id.textViewPayment);
            TextView textViewPrice = (TextView) view.findViewById(R.id.textViewPrice);

            Order o = (Order) arrFilteredOrders.get(position);
            textViewId.setText("Order ID: " + (int)Double.parseDouble(o.getOrderID()));
            String paymentMethod = "";
            for(int i = 0;i < arrPayment.size(); i++)
            {
                Payment temp = (Payment) arrPayment.get(i);
                if(temp.getPayment_id().equals(o.getPaymentID()))
                {
                    paymentMethod = temp.getPayment_name();
                    paymentMethod = paymentMethod.substring(0, paymentMethod.length()-2);
                }
            }
            textViewPayment.setText("Payment Method: " + paymentMethod.toLowerCase());
            textViewPrice.setText("Price: £" + String.format("%.2f",Double.parseDouble(o.getOrderPrice())));

            return view;
        }
    }
}
