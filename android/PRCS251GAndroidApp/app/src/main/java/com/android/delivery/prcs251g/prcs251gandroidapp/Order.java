package com.android.delivery.prcs251g.prcs251gandroidapp;
/**
 *
 * @author Adam/Brian/Luke
 */
public class Order 
{
    private int noItems;
    private String orderID;
    private String customerID;
    private String employeeID;
    private String paymentID;
    private String orderPrice;
    private String item1;
    private String item2;
    private String item3;
    private String item4;
    private String item5;
    private String item6;
    private String item7;
    private String item8;
    private String item9;
    private String item10;
    private String status;
    private String time;

    public Order(String orderID, String customerID, String employeeID, String paymentID, String orderPrice, String item1, String item2, String item3, String item4, String item5, String item6, String item7, String item8, String item9, String item10, String status, String time) 
    {
        this.orderID = orderID;
        this.customerID = customerID;
        this.employeeID = employeeID;
        this.paymentID = paymentID;
        this.orderPrice = orderPrice;
        this.item1 = item1;
        if(!item1.equals("null"))
        {
            noItems++;
        }
        this.item2 = item2;
        if(!item2.equals("null"))
        {
            noItems++;
        }
        this.item3 = item3;
        if(!item3.equals("null"))
        {
            noItems++;
        }
        this.item4 = item4;
        if(!item4.equals("null"))
        {
            noItems++;
        }
        this.item5 = item5;
        if(!item5.equals("null"))
        {
            noItems++;
        }
        this.item6 = item6;
        if(!item6.equals("null"))
        {
            noItems++;
        }
        this.item7 = item7;
        if(!item7.equals("null"))
        {
            noItems++;
        }
        this.item8 = item8;
        if(!item8.equals("null"))
        {
            noItems++;
        }
        this.item9 = item9;
        if(!item9.equals("null"))
        {
            noItems++;
        }
        this.item10 = item10;
        if(!item10.equals("null"))
        {
            noItems++;
        }
        this.status = status;
        this.time = time;
    }

    public String getOrderID() 
    {
        return orderID;
    }

    public void setOrderID(String orderID) 
    {
        this.orderID = orderID;
    }

    public String getCustomerID()
    {
        return customerID;
    }

    public void setCustomerID(String customerID) 
    {
        this.customerID = customerID;
    }

    public String getEmployeeID()
    {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) 
    {
        this.employeeID = employeeID;
    }

    public String getPaymentID() 
    {
        return paymentID;
    }

    public void setPaymentID(String paymentID)
    {
        this.paymentID = paymentID;
    }

    public String getOrderPrice() 
    {
        return orderPrice;
    }

    public void setOrderPrice(String orderPrice) 
    {
        this.orderPrice = orderPrice;
    }

    public String getItem1()
    {
        return item1;
    }

    public void setItem1(String item1)
    {
        this.item1 = item1;
    }

    public String getItem2()
    {
        return item2;
    }

    public void setItem2(String item2)
    {
        this.item2 = item2;
    }

    public String getItem3() 
    {
        return item3;
    }

    public void setItem3(String item3) 
    {
        this.item3 = item3;
    }

    public String getItem4()
    {
        return item4;
    }

    public void setItem4(String item4)
    {
        this.item4 = item4;
    }

    public String getItem5()
    {
        return item5;
    }

    public void setItem5(String item5) 
    {
        this.item5 = item5;
    }

    public String getItem6() 
    {
        return item6;
    }

    public void setItem6(String item6) 
    {
        this.item6 = item6;
    }

    public String getItem7() 
    {
        return item7;
    }

    public void setItem7(String item7) 
    {
        this.item7 = item7;
    }

    public String getItem8() 
    {
        return item8;
    }

    public void setItem8(String item8)
    {
        this.item8 = item8;
    }

    public String getItem9() 
    {
        return item9;
    }

    public void setItem9(String item9) 
    {
        this.item9 = item9;
    }

    public String getItem10()
    {
        return item10;
    }

    public void setItem10(String item10) 
    {
        this.item10 = item10;
    }
    public int getNoItems()
    {
        return this.noItems;
    }
    public void setStatus(String status)
    {
        this.status = status;
    }
    public String getStatus()
    {
        return this.status;
    }
    public String getTime()
    {
        return this.time;
    }
    public void setTime(String time)
    {
        this.time = time;
    }
}
