package com.android.delivery.prcs251g.prcs251gandroidapp;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.util.ArrayList;

public class MapWithSelectedOrder extends FragmentActivity implements OnMapReadyCallback, OnClickListener
{
    Session sess = Session.getInstance();
    ImportAPI api = ImportAPI.getInstance();
    ArrayList arrOrders;
    private GoogleMap mMap;
    Button btnCompleteDelivery = null;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_with_selected_order);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        arrOrders = api.getOrder();
        btnCompleteDelivery = (Button) findViewById(R.id.btnCompleteDelivery);
        btnCompleteDelivery.setOnClickListener(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.btnCompleteDelivery)
        {
            CompleteDelivery();
        }
    }

    private void CompleteDelivery()
    {
        CompleteOrder conn = new CompleteOrder(sess.getOrderID(), sess.getPrice());
        conn.start();
        for(int j = 0; j < arrOrders.size(); j++ )
        {
            Order temp = (Order) arrOrders.get(j);
            if(temp.getOrderID().equals(sess.getOrderID()))
            {
                arrOrders.remove(j);
            }
        }
        api.SetOrderArray(arrOrders);
        Intent i = new Intent(this, CurrentOrders.class);
        startActivity(i);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        String cityName = sess.getCustomerCity();
        LatLng customerAddress;
        switch(cityName.toLowerCase())
        {
            case "plymouth":
                customerAddress = new LatLng(50.4, -4.2);
                break;
            case "london" :
                customerAddress = new LatLng(51, 0.1);
                break;
            default:
                customerAddress = new LatLng(-14, -28);
                break;
        }
        mMap.addMarker(new MarkerOptions().position(customerAddress).title("Current Destination."));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(customerAddress, 10));
    }
}
