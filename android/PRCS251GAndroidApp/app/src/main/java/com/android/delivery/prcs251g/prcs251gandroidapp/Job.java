/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam, Brian, Luke
 */
public class Job {
    private String job_id;
    private String job_name;

    public Job(String job_id, String job_name) {
        this.job_id = job_id;
        this.job_name = job_name;
    }

    public String getJob_id() {
        return job_id;
    }

    public void setJob_id(String job_id) {
        this.job_id = job_id;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }
    
}
