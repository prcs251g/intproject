/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam, Brian, Luke
 */
public class Bases {
    private String base_Calories;
    private String base_ID;
    private String base_Name;
    private String base_Price;

    public Bases(String base_ID, String base_Price, String base_Name, String base_Calories) {
        this.base_Calories = base_Calories;
        this.base_ID = base_ID;
        this.base_Name = base_Name;
        this.base_Price = base_Price;
    }

    public String getBase_Calories() {
        return base_Calories;
    }

    public void setBase_Calories(String base_Calories) {
        this.base_Calories = base_Calories;
    }

    public String getBase_ID() {
        return base_ID;
    }

    public void setBase_ID(String base_ID) {
        this.base_ID = base_ID;
    }

    public String getBase_Name() {
        return base_Name;
    }

    public void setBase_Name(String base_Name) {
        this.base_Name = base_Name;
    }

    public String getBase_Price() {
        return base_Price;
    }

    public void setBase_Price(String base_Price) {
        this.base_Price = base_Price;
    }
    
    
}
