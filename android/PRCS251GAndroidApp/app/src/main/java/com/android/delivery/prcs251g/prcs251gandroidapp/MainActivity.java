package com.android.delivery.prcs251g.prcs251gandroidapp;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;
import android.content.Intent;



// Author: Brian/Luke/Adam
public class MainActivity extends AppCompatActivity implements OnClickListener
{
    Session sess = Session.getInstance();
    ImportAPI menuItems = ImportAPI.getInstance();
    ArrayList arrEmployeeCopy = new ArrayList();
    APIConnection conn = new APIConnection();
    Button login = null;
    EditText username = null;
    EditText password = null;
    String usernameInput;
    String passwordInput;
    Context context;
    CharSequence text = "Incorrect username or password.";
    int duration = Toast.LENGTH_SHORT;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        login = (Button) findViewById(R.id.btnLogin);
        login.setOnClickListener(this);
        username = (EditText) findViewById(R.id.txtUsername);
        password = (EditText) findViewById(R.id.txtPassword);
        context = getApplicationContext();
        conn.start();
    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.btnLogin)
        {
            login();
        }
    }
    protected void login()
    {
        Employee driver = null;
        arrEmployeeCopy = menuItems.getEmployee();
        usernameInput = username.getText().toString().toLowerCase();
        passwordInput = password.getText().toString();
        for(int i = 0; i < arrEmployeeCopy.size(); i++)
        {
            Employee temp = (Employee) arrEmployeeCopy.get(i);
            if(temp.getEmployee_job_id().equals("2.0"))
            {
                if (temp.getEmployee_username().equals(usernameInput))
                {
                    driver = temp;
                    System.out.println("found " + usernameInput);
                }else
                {
                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        }
        if(driver != null)
        {
            String salt = driver.getEmployee_salt();
            String pepper = "yoqy3Gu+AiSetUAjuNcdgmCE/ybCEFKJyVWKpW0Jb4c=";
            StringHashGenerator hasher = new StringHashGenerator();
            String hashedPassword = hasher.SHA512(passwordInput, salt);
            hashedPassword = hasher.SHA512(hashedPassword, pepper);
            System.out.println(hashedPassword);
            if(hashedPassword.equals(driver.getEmployee_password()))
            {
                sess.setEmployeeID(driver.getEmployee_id());
                System.out.println("Logged in.");
                Intent i = new Intent(this, MapsActivity.class);
                startActivity(i);
            }else
            {
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
            }
        }
    }
}
