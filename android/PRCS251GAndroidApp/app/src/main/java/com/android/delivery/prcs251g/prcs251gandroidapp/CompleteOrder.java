package com.android.delivery.prcs251g.prcs251gandroidapp;

import android.os.AsyncTask;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bvyoung on 10/05/2017.
 */

public class CompleteOrder
{
    String orderID;
    String price;
    private WebServiceTask task = null;

    public CompleteOrder(String orderID, String price)
    {
        this.orderID = orderID;
        this.price = price;
    }

    public void start()
    {
        try
        {
            task = new WebServiceTask();
            task.execute();
        }catch(Exception ex)
        {

        }
    }

    public synchronized void runConnection()
    {
        RESTfulMethods restOrders = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orders");
        RESTfulMethods restHistory = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orderhistories");
        restOrders.DELETE("" + (int)Double.parseDouble(orderID));
        JSONString js = new JSONString();
        Boolean f = true;
        int i = 0;
        while (f)
        {
            try{
                restHistory.GET("" + i);
                i++;
            }catch(RuntimeException e){
                f=false;
            }
        }
        js.AddElement("ORDER_PRICE", price);
        js.AddElement("ORDER_HISTORY_ID", i);
        Date now = new Date();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String formatDateTime = formatter.format(now);
        js.AddElement("Date", formatDateTime);
        restHistory.POST(js.GetJSON());
    }

    private class WebServiceTask extends AsyncTask<Void,Void,Void>
    {
        protected Void doInBackground(Void... params)
        {
            runConnection();
            return null;
        }

        @Override
        protected void onPostExecute(Void v)
        {

        }
    }
}
