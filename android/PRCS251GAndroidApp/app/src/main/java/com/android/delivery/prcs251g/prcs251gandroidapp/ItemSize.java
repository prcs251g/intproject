/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam, Brian, Luke
 */
public class ItemSize {
    private String size_id;
    private String size_name;
    private String size_price;

    public ItemSize(String size_id, String size_name, String size_price) {
        this.size_id = size_id;
        this.size_name = size_name;
        this.size_price = size_price;
    }

    public String getSize_id() {
        return size_id;
    }

    public String getSize_name() {
        return size_name;
    }

    public String getSize_price() {
        return size_price;
    }

    public void setSize_id(String size_id) {
        this.size_id = size_id;
    }

    public void setSize_name(String size_name) {
        this.size_name = size_name;
    }

    public void setSize_price(String size_price) {
        this.size_price = size_price;
    }
    
}
