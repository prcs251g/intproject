/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.android.delivery.prcs251g.prcs251gandroidapp;

/**
 *
 * @author Adam, Brian, Luke
 */
public class Side {
    private String side_calories;
    private String side_ID;
    private String side_name;
    private String side_price;
    private String side_size;

    public Side(String side_ID, String side_name, String side_size,
            String side_price, String side_calories) {
        this.side_ID = side_ID;
        this.side_name = side_name;
        this.side_size = side_size;
        this.side_price = side_price;
        this.side_calories = side_calories;
    }

    public String getSide_calories() {
        return side_calories;
    }

    public String getSide_ID() {
        return side_ID;
    }

    public String getSide_name() {
        return side_name;
    }

    public String getSide_price() {
        return side_price;
    }

    public String getSide_size() {
        return side_size;
    }

    public void setSide_calories(String side_calories) {
        this.side_calories = side_calories;
    }

    public void setSide_ID(String side_ID) {
        this.side_ID = side_ID;
    }

    public void setSide_name(String side_name) {
        this.side_name = side_name;
    }

    public void setSide_price(String side_price) {
        this.side_price = side_price;
    }

    public void setSide_size(String side_size) {
        this.side_size = side_size;
    }
      
}
