package GUI.Chef;

import Classes.Bases;
import Classes.Crust;
import Classes.Drink;
import Classes.ImportAPI;
import Classes.ItemSize;
import Classes.JSONString;
import Classes.Order;
import Classes.OrderItem;
import Classes.Pizza;
import Classes.RESTfulMethods;
import Classes.Session;
import Classes.Side;
import Classes.SyncAPI;
import Classes.Topping;
import GUI.Common.LoginPage;
import java.util.ArrayList;
import javax.swing.DefaultListModel;

/**
 * CurrentOrder - Displays selected order information.
 * @author Luke Chilcott / Brian Young
 */
public class CurrentOrder extends javax.swing.JFrame 
{
    ArrayList arrOrderItemsCopy, arrOrderCopy, arrPizzaCopy, arrDrinkCopy, arrSideCopy, arrSizesCopy, arrBasesCopy, arrCrustsCopy, arrToppingCopy;
    ArrayList orderItemIDs = new ArrayList();
    ArrayList ItemTypes = new ArrayList();
    ArrayList ItemIDs = new ArrayList();
    DefaultListModel listModelOrderItems = new DefaultListModel();
    DefaultListModel listModelToppings = new DefaultListModel();
    private final SyncAPI sync = new SyncAPI();
    private final ImportAPI menuItems = ImportAPI.getInstance();
    private final Session session = Session.getInstance();
    Order selectedOrder;
    /**
     * Creates new form CurrentOrder
     */
    public CurrentOrder() 
    {
        this.setTitle("Current Order");
        sync.OrderSync();
        sync.OrderItemSync();
        sync.DrinkSync();
        sync.PizzaSync();
        sync.SideSync();
        sync.SizeSync();
        sync.BaseSync();
        sync.CrustSync();
        sync.ToppingSync();
        arrToppingCopy = menuItems.getTopping();
        arrSizesCopy = menuItems.getItemSize();
        arrPizzaCopy = menuItems.getPizza();
        arrSideCopy = menuItems.getSide();
        arrDrinkCopy = menuItems.getDrink();
        arrOrderItemsCopy = menuItems.getOrderItem();
        arrOrderCopy = menuItems.getOrder();
        arrCrustsCopy = menuItems.getCrust();
        arrBasesCopy = menuItems.getBases();
        initComponents();
        lstToppings.setVisible(false);
    }

    public void SelectedIndex(int index)
    {
        selectedOrder = (Order)arrOrderCopy.get(index);
        lblCurrentOrder.setText("Current Order: " + (int)Double.parseDouble(selectedOrder.getOrderID()));
        if(!selectedOrder.getItem1().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem1());
        }
        if(!selectedOrder.getItem2().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem2());
        }
        if(!selectedOrder.getItem3().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem3());
        }
        if(!selectedOrder.getItem4().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem4());
        }
        if(!selectedOrder.getItem5().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem5());
        }
        if(!selectedOrder.getItem6().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem6());
        }
        if(!selectedOrder.getItem7().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem7());
        }
        if(!selectedOrder.getItem8().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem8());
        }
        if(!selectedOrder.getItem9().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem9());
        }
        if(!selectedOrder.getItem10().equals("null"))
        {
            orderItemIDs.add(selectedOrder.getItem10());
        }
        RefreshListModel();
    }
    
    public void RefreshListModel()
    {
        String output = "";
        listModelOrderItems.clear();
        
        for(int i = 0; i < orderItemIDs.size(); i++)
        {
            for(int j = 0; j < arrOrderItemsCopy.size();j++)
            {
                OrderItem temp = (OrderItem)arrOrderItemsCopy.get(j);
                if(temp.getOrderItemID().equals(orderItemIDs.get(i)))
                {
                    if(!temp.getPizzaID().equals("null"))
                    {
                        for(int k = 0; k < arrPizzaCopy.size();k++)
                        {
                            Pizza tempPizza = (Pizza)arrPizzaCopy.get(k);
                            if(temp.getPizzaID().equals(tempPizza.getPizza_id()))
                            {
                                ItemTypes.add("PIZZA");
                                ItemIDs.add(tempPizza.getPizza_id());
                                output = tempPizza.getPizza_name();
                            }
                        }
                    }else
                    if(!temp.getDrinkID().equals("null"))
                    {
                        for(int k = 0; k < arrDrinkCopy.size();k++)
                        {
                            Drink tempDrink = (Drink)arrDrinkCopy.get(k);
                            if(temp.getDrinkID().equals(tempDrink.getDrink_ID()))
                            {
                                output = tempDrink.getDrink_name();
                                ItemTypes.add("DRINK");
                                ItemIDs.add(tempDrink.getDrink_ID());
                            }
                        }
                    }else
                    if(!temp.getSideID().equals("null"))
                    {
                        for(int k = 0; k < arrSideCopy.size();k++)
                        {
                            Side tempSide = (Side)arrSideCopy.get(k);
                            if(temp.getSideID().equals(tempSide.getSide_ID()))
                            {
                                output = tempSide.getSide_name();
                                ItemTypes.add("SIDE");
                                ItemIDs.add(tempSide.getSide_ID());
                            }
                        }
                    }
                    break;
                }
            }
            listModelOrderItems.addElement(output);
            output = "";
        }
    }
    
    private void ShowSelectedItem()
    {
        listModelToppings.clear();
        String id = "" + ItemIDs.get(lstOrderItems.getSelectedIndex());
        String type = "" + ItemTypes.get(lstOrderItems.getSelectedIndex());
        if(type.equals("PIZZA"))
        {
            lstToppings.setVisible(true);
            for(int i = 0; i < arrPizzaCopy.size(); i++)
            {
                Pizza temp = (Pizza)arrPizzaCopy.get(i);
                if(temp.getPizza_id().equals(id))
                {
                    for(int j = 0; j < arrSizesCopy.size(); j++)
                    {
                        ItemSize tempSize = (ItemSize)arrSizesCopy.get(j);
                        if(tempSize.getSize_id().equals(temp.getPizza_size_id()))   
                        {
                            lblItemSize.setText(CapsConv(tempSize.getSize_name()));
                        }
                    }
                    for(int j = 0; j < arrBasesCopy.size(); j++)
                    {
                        Bases tempBases = (Bases)arrBasesCopy.get(j);
                        if(tempBases.getBase_ID().equals(temp.getBase_id()))   
                        {
                            lblItemBase.setText(CapsConv(tempBases.getBase_Name()));
                        }
                    }
                    for(int j = 0; j < arrCrustsCopy.size(); j++)
                    {
                        Crust tempCrust = (Crust)arrCrustsCopy.get(j);
                        if(tempCrust.getCrust_ID().equals(temp.getCrust_id()))   
                        {
                            lblItemCrust.setText(CapsConv(tempCrust.getCrust_Name()));
                        }
                    }
                    String[] toppingIDs = {temp.getTopping1(), temp.getTopping2(), temp.getTopping3(), temp.getTopping4(), temp.getTopping5()};
                    for(int j = 0; j < 5; j++)
                    {
                        for(int k = 0; k < arrToppingCopy.size(); k++)
                        {
                            Topping tempTop = (Topping)arrToppingCopy.get(k);
                            if(toppingIDs[j].equals(tempTop.getToppingID()))
                            {
                                listModelToppings.addElement("" + CapsConv(tempTop.getToppingName()));
                            }
                        }
                    }
                }
            }
        }else
        if(type.equals("SIDE"))
        {
            lstToppings.setVisible(false);
            lblItemBase.setText("N/A");
            lblItemCrust.setText("N/A");
            for(int i = 0; i < arrSideCopy.size(); i++)
            {
                Side temp = (Side)arrSideCopy.get(i);
                if(temp.getSide_ID().equals(id))
                {
                    lblItemSize.setText(temp.getSide_size());
                }
            }
        }else
        if(type.equals("DRINK"))
        {
            lstToppings.setVisible(false);
            lblItemBase.setText("N/A");
            lblItemCrust.setText("N/A");
            for(int i = 0; i < arrDrinkCopy.size(); i++)
            {
                Drink temp = (Drink)arrDrinkCopy.get(i);
                if(temp.getDrink_ID().equals(id))
                {
                    lblItemSize.setText(temp.getDrink_size());
                }
            }
        }
    }
    
    private void CompleteSelectedOrder()
    {
        RESTfulMethods rest = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orders");
        JSONString js = new JSONString();
        js.AddElement("ORDER_PRICE", selectedOrder.getOrderPrice());
        js.AddElement("ORDER_ID", (int)Double.parseDouble(selectedOrder.getOrderID()));
        js.AddElement("PAYMENT_ID", (int)Double.parseDouble(selectedOrder.getPaymentID()));
        js.AddElement("CUSTOMER_ID", (int)Double.parseDouble(selectedOrder.getCustomerID()));
        String[] orderItemNames = {"ONE","TWO","THREE","FOUR","FIVE","SIX","SEVEN","EIGHT","NINE", "TEN"};
        String[] orderItemIDs = {selectedOrder.getItem1(),selectedOrder.getItem2(),selectedOrder.getItem3(),selectedOrder.getItem4(),selectedOrder.getItem5(),selectedOrder.getItem6(),selectedOrder.getItem7(),selectedOrder.getItem8(),selectedOrder.getItem9(),selectedOrder.getItem10()};
        for(int i = 0; i < selectedOrder.getNoItems(); i++)
        {
            if(!orderItemIDs[i].equals("null"))
            {
                js.AddElement("ORDER_ITEM_" + orderItemNames[i], (int)Double.parseDouble(orderItemIDs[i]));
            }
        }
        js.AddElement("STATUS", "DELIVER");
        js.AddElement("EMPLOYEE_ID", 0);
        js.AddElement("ELAPSED_TIME", selectedOrder.getTime());
        rest.PUT("" + (int)Double.parseDouble(selectedOrder.getOrderID()), js.GetJSON());
        SelectOrder addFrame = new SelectOrder();
        addFrame.setVisible(true);
        this.dispose();
    }
    
    private String CapsConv(String info){
      if (!info.isEmpty()){
        String firstStringLetter = info.substring(0,1).toUpperCase();
        String restofString = info.substring(1).toLowerCase();
        return firstStringLetter + restofString;
      }
      return null;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblCurrentOrder = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblItemInformation = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        lblItemSize = new javax.swing.JLabel();
        lblSizeLabel = new javax.swing.JLabel();
        lblItemBase = new javax.swing.JLabel();
        lblBase = new javax.swing.JLabel();
        lblCrust = new javax.swing.JLabel();
        lblItemCrust = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstToppings = new javax.swing.JList<>();
        lblTop = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstOrderItems = new javax.swing.JList<>();
        btnCompOrd = new javax.swing.JButton();
        btnRejOrd = new javax.swing.JToggleButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jLogOut = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        lblCurrentOrder.setFont(new java.awt.Font("SWItal", 3, 20)); // NOI18N
        lblCurrentOrder.setText("Current Order:");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lblItemInformation.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        lblItemInformation.setText("Item Information:");

        lblItemSize.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        lblItemSize.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblItemSize.setText("N/A");

        lblSizeLabel.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        lblSizeLabel.setText("Size :");

        lblItemBase.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        lblItemBase.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblItemBase.setText("N/A");

        lblBase.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        lblBase.setText("Base :");

        lblCrust.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        lblCrust.setText("Crust :");

        lblItemCrust.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        lblItemCrust.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        lblItemCrust.setText("N/A");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblSizeLabel)
                            .addComponent(lblBase))
                        .addGap(8, 8, 8)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblItemBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lblItemSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblCrust)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblItemCrust, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblSizeLabel, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lblItemSize))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblItemBase)
                    .addComponent(lblBase))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblItemCrust)
                    .addComponent(lblCrust)))
        );

        lstToppings.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lstToppings.setModel(listModelToppings);
        jScrollPane1.setViewportView(lstToppings);

        lblTop.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        lblTop.setText("Toppings:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblItemInformation)
                                    .addComponent(lblTop))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(lblItemInformation)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblTop)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                .addContainerGap())
        );

        lstOrderItems.setFont(new java.awt.Font("Tahoma", 0, 25)); // NOI18N
        lstOrderItems.setModel(listModelOrderItems);
        lstOrderItems.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstOrderItemsMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(lstOrderItems);

        btnCompOrd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnCompOrd.setText("Complete Order");
        btnCompOrd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompOrdActionPerformed(evt);
            }
        });

        btnRejOrd.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnRejOrd.setText("Reject Order");
        btnRejOrd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRejOrdActionPerformed(evt);
            }
        });

        jMenu1.setText("File");

        jLogOut.setText("Log Out");
        jLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jLogOutActionPerformed(evt);
            }
        });
        jMenu1.add(jLogOut);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCurrentOrder, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 324, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCompOrd, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRejOrd, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblCurrentOrder)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnRejOrd, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                            .addComponent(btnCompOrd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(1, 1, 1))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jLogOutActionPerformed
        session.ClearCurrent();
        LoginPage addFrame = new LoginPage();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLogOutActionPerformed

    private void lstOrderItemsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstOrderItemsMouseClicked
        ShowSelectedItem();
    }//GEN-LAST:event_lstOrderItemsMouseClicked

    private void btnCompOrdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompOrdActionPerformed
        CompleteSelectedOrder();
    }//GEN-LAST:event_btnCompOrdActionPerformed

    private void btnRejOrdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRejOrdActionPerformed
        SelectOrder addFrame = new SelectOrder();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnRejOrdActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CurrentOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CurrentOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CurrentOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CurrentOrder.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CurrentOrder().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCompOrd;
    private javax.swing.JToggleButton btnRejOrd;
    private javax.swing.JMenuItem jLogOut;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblBase;
    private javax.swing.JLabel lblCrust;
    private javax.swing.JLabel lblCurrentOrder;
    private javax.swing.JLabel lblItemBase;
    private javax.swing.JLabel lblItemCrust;
    private javax.swing.JLabel lblItemInformation;
    private javax.swing.JLabel lblItemSize;
    private javax.swing.JLabel lblSizeLabel;
    private javax.swing.JLabel lblTop;
    private javax.swing.JList<String> lstOrderItems;
    private javax.swing.JList<String> lstToppings;
    // End of variables declaration//GEN-END:variables
}
