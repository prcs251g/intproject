package GUI.Common;

import Classes.Employee;
import Classes.ImportAPI;
import Classes.Session;
import Classes.SyncAPI;
import GUI.Admin.Navigation;
import GUI.Chef.SelectOrder;
import classes.StringHashGenerator;
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 * LoginPage - Authenticates users before accessing the program.
 * @author Luke Chilcott / Brian Young
 */
public class LoginPage extends javax.swing.JFrame {
    private final Session session = Session.getInstance();
    private final ImportAPI API = ImportAPI.getInstance(); 
    private final SyncAPI sync = new SyncAPI();
    private final ArrayList arrEmployeeCopy;

    /**
     * Creates new form LoginPage
     */
    public LoginPage() {
        this.setTitle("Log in");
        SyncData();
        arrEmployeeCopy = API.getEmployee();
        initComponents();
    }
    
    private void SyncData(){
        sync.EmployeeSync();
        sync.JobSync();
    }
    
    private void LoginAttempt(){
        boolean loginCorrect = false;
        String user = txtUser.getText().toLowerCase();
        String password = String.valueOf(pfPass.getPassword());
        
        for (int i=0; i<arrEmployeeCopy.size(); i++){
            Employee temp = (Employee)arrEmployeeCopy.get(i); 
            if (user.equals(temp.getEmployee_username())){
                String salt = temp.getEmployee_salt();
                StringHashGenerator hasher = new StringHashGenerator();
                password = hasher.SHA512(password, salt);
                String salt2 = "yoqy3Gu+AiSetUAjuNcdgmCE/ybCEFKJyVWKpW0Jb4c=";
                password = hasher.SHA512(password, salt2);
                if (password.equals(temp.getEmployee_password())){
                    loginCorrect = true;
                    String jobID = temp.getEmployee_job_id();
                    switch (jobID) {
                        case "1.0":
                        {
                            session.setJobTitle("CHEF");
                            session.setUsername(user);
                            SelectOrder addFrame = new SelectOrder();
                            addFrame.setVisible(true);
                            this.dispose();
                            break;
                        }
                        case "3.0":
                        {
                            session.setJobTitle("ADMIN");
                            session.setUsername(user);
                            Navigation addFrame = new Navigation();
                            addFrame.setVisible(true);
                            this.dispose();
                            break;
                        }
                        case "4.0":
                        {
                            session.setJobTitle("MANAGER");
                            session.setUsername(user);
                            Navigation addFrame = new Navigation();
                            addFrame.setVisible(true);
                            this.dispose();
                            break;
                        }
                        default:
                        {
                            JOptionPane.showMessageDialog(null, "Insufficient account permissions", "Access Denied", JOptionPane.ERROR_MESSAGE);
                            break;
                        }
                    }
                }
            }
          
        }
        if (loginCorrect == false){
            JOptionPane.showMessageDialog(null, "Incorrect Username or Password", "Invaild Login Details", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtUser = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btnLogin = new javax.swing.JButton();
        pfPass = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel1.setText("Username:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 13)); // NOI18N
        jLabel2.setText("Password:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Please Login Below");

        btnLogin.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnLogin)
                    .addComponent(txtUser, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                    .addComponent(pfPass))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(88, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(83, 83, 83))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jLabel3)
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(pfPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addComponent(btnLogin)
                .addContainerGap(67, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        LoginAttempt();
    }//GEN-LAST:event_btnLoginActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPasswordField pfPass;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables
}
