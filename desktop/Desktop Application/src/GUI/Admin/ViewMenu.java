package GUI.Admin;

import Classes.Drink;
import Classes.JSONParser;
import Classes.Pizza;
import Classes.ImportAPI;
import Classes.ItemSize;
import Classes.RESTfulMethods;
import Classes.Session;
import Classes.Side;
import Classes.SyncAPI;
import Classes.Topping;
import GUI.Admin.AddEditMenuItems.AddPizza;
import GUI.Admin.AddEditMenuItems.AddDrink;
import GUI.Admin.AddEditMenuItems.AddTopping;
import GUI.Admin.AddEditMenuItems.AddSide;
import GUI.Admin.AddEditMenuItems.EditDrink;
import GUI.Admin.AddEditMenuItems.EditPizza;
import GUI.Admin.AddEditMenuItems.EditSide;
import GUI.Admin.AddEditMenuItems.EditTopping;
import GUI.Common.LoginPage;
import java.awt.Frame;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * ViewMenu - Displays Menu items information for Admin/Manager.
 * @author Luke Chilcott / Brian Young
 */
public class ViewMenu extends javax.swing.JFrame {
    private ArrayList arrPizzaCopy, arrItemSizeCopy, arrSideCopy, arrDrinkCopy,arrToppingCopy;
    private final DefaultListModel listModelPizzas, listModelSides,listModelDrinks,listModelToppingPrices;
    private final DefaultListModel listModelToppings;
    private final Session session = Session.getInstance();
    private final ImportAPI menuItems = ImportAPI.getInstance();
    private final SyncAPI sync = new SyncAPI();
    private final RESTfulMethods restsize = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/itemsizes");
    private final RESTfulMethods restBase = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/bases");
    private final RESTfulMethods restTop = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/toppings");
    private final RESTfulMethods restCrust = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251G/api/crusts");
    
    /**
     * Creates new form MenuList
     */
    public ViewMenu() {
        this.setTitle("Menu");
        SyncData();
        RefreshArrays();
        listModelPizzas = new DefaultListModel();
        listModelSides = new DefaultListModel();
        listModelDrinks = new DefaultListModel();
        listModelToppings = new DefaultListModel();
        listModelToppingPrices = new DefaultListModel();
        RefreshListModel();
        initComponents();
    }
    private void RefreshArrays(){
        arrPizzaCopy = menuItems.getPizza();
        arrItemSizeCopy = menuItems.getItemSize();
        arrSideCopy = menuItems.getSide();
        arrDrinkCopy = menuItems.getDrink();
        arrToppingCopy = menuItems.getTopping();
    }
    
    private void SyncData()
    {
        sync.PizzaSync();
        sync.SizeSync();
        sync.DrinkSync();
        sync.SideSync();
        sync.ToppingSync();
    }
    
    /** 
     * Instantiates list models (listModelPizzas/listModelSides/listModelDrinks/
     * listModelToppingPrices) with current data from Pizza/Sides/Drinks/Topping 
     * arrays respectively;
     */
    private void RefreshListModel()
    {
        String output = "";
        listModelPizzas.clear();
        listModelSides.clear();
        listModelDrinks.clear();
        listModelToppingPrices.clear();
        for (int i=0; i<arrPizzaCopy.size(); i++){  
            Pizza temp = (Pizza)arrPizzaCopy.get(i); 
            for (int j=0; j<arrItemSizeCopy.size(); j++){
                ItemSize tempSize = (ItemSize)arrItemSizeCopy.get(j);
                if (tempSize.getSize_id().equals(temp.getPizza_size_id())){
                    output = CapsConv(tempSize.getSize_name());
                }
            }
            output = output.concat(" " + temp.getPizza_name());
            listModelPizzas.addElement(output);
            output = "";
        }
        
        for (int i=0; i<arrSideCopy.size(); i++){
            Side temp = (Side)arrSideCopy.get(i);
            output = temp.getSide_size() + " " + temp.getSide_name();
            listModelSides.addElement(output);
            output = "";
        }
        
        for (int i=0; i<arrDrinkCopy.size(); i++){
            Drink temp = (Drink)arrDrinkCopy.get(i);
            output = temp.getDrink_size() + " " + temp.getDrink_name();
            listModelDrinks.addElement(output);   
            output = "";
        }
        
         for (int i=0; i<arrToppingCopy.size(); i++){
            Topping temp = (Topping)arrToppingCopy.get(i);
            output = i+1 + " | " + temp.getToppingName();
            listModelToppingPrices.addElement(output);   
            output = "";
        }
        
    }
    
    /**
     * Displays information of pizza selected in lstPizza.
     */
    private void showSelectedPizza(){
        try{
            Pizza objSelectedItem = null;
            objSelectedItem = (Pizza)arrPizzaCopy.get(lstPizza.getSelectedIndex());
            txtName.setText(objSelectedItem.getPizza_name()); 
            JSONParser bases = new JSONParser(restBase.GET());
            bases.Parse();
            txtBase.setText(CapsConv(bases.GetValue("BASE_ID", objSelectedItem.getBase_id(), "BASE_NAME")));
            JSONParser top = new JSONParser(restTop.GET());
            top.Parse();
            listModelToppings.clear();
            listModelToppings.addElement(CapsConv(top.GetValue("TOPPING_ID", objSelectedItem.getTopping1(), "TOPPING_NAME")));
            listModelToppings.addElement(CapsConv(top.GetValue("TOPPING_ID", objSelectedItem.getTopping2(), "TOPPING_NAME")));
            listModelToppings.addElement(CapsConv(top.GetValue("TOPPING_ID", objSelectedItem.getTopping3(), "TOPPING_NAME")));
            listModelToppings.addElement(CapsConv(top.GetValue("TOPPING_ID", objSelectedItem.getTopping4(), "TOPPING_NAME")));
            listModelToppings.addElement(CapsConv(top.GetValue("TOPPING_ID", objSelectedItem.getTopping5(), "TOPPING_NAME")));
            JSONParser sizes = new JSONParser(restsize.GET());
            sizes.Parse();
            txtSize.setText(CapsConv(sizes.GetValue("SIZE_ID", objSelectedItem.getPizza_size_id(), "SIZE_NAME")));
            JSONParser crust = new JSONParser(restCrust.GET());
            crust.Parse();
            txtCrust.setText(CapsConv(crust.GetValue("CRUST_ID", objSelectedItem.getCrust_id(),"CRUST_NAME")));
            txtPrice.setText("£ " + String.format("%.2f", Double.parseDouble(objSelectedItem.getPizza_price())));
            txtCal.setText(String.format("%.0f", Double.parseDouble(objSelectedItem.getTotalCalories())));
        }
        catch(Exception e){
        ClearLbls();
        JOptionPane.showMessageDialog(null, "Error retrieving Pizza data", "Error", JOptionPane.ERROR_MESSAGE);
        }    
    }
    
    /**
     * Displays information of side selected in lstSides.
     */
    private void showSelectedSide(){
        try{
            Side objSelectedItem = null;
            objSelectedItem = (Side)arrSideCopy.get(lstSides.getSelectedIndex());
            System.out.println("You selected a " + objSelectedItem.getSide_name());
            txtName.setText(objSelectedItem.getSide_name()); 
            txtSize.setText(objSelectedItem.getSide_size());
            txtPrice.setText("£ " + String.format("%.2f", Double.parseDouble(objSelectedItem.getSide_price())));
            txtCal.setText(String.format("%.0f", Double.parseDouble(objSelectedItem.getSide_calories())));
        }catch(Exception e){
            ClearLbls();
            JOptionPane.showMessageDialog(null, "Error retrieving selected Side data", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * Displays information of drink selected in lstDrinks.
     */    
    private void showSelectedDrink(){
        try{
            Drink objSelectedItem = null;
            objSelectedItem = (Drink)arrDrinkCopy.get(lstDrinks.getSelectedIndex());
            txtName.setText(objSelectedItem.getDrink_name());
            txtSize.setText(objSelectedItem.getDrink_size());
            txtPrice.setText("£ " + String.format("%.2f", Double.parseDouble(objSelectedItem.getDrink_price())));
            txtCal.setText(String.format("%.0f", Double.parseDouble(objSelectedItem.getDrink_calories())));
        }catch(Exception e){
            ClearLbls();
            JOptionPane.showMessageDialog(null, "Error retrieving selected Drink data", "Error", JOptionPane.ERROR_MESSAGE);
        }  
    }
    
    /**
     * Displays information of topping selected in lstToppings.
     */
    private void showSelectedTopping(){
        try{ 
            Topping objSelectedItem = null;
            objSelectedItem = (Topping)arrToppingCopy.get(lstToppings.getSelectedIndex());
            txtName.setText(objSelectedItem.getToppingName());
            txtPrice.setText("£ " + (String.format("%.2f", Double.parseDouble(objSelectedItem.getToppingPrice()))));
            txtCal.setText(objSelectedItem.getCalories() + " Cal");
            
        }
        catch(Exception e){
            ClearLbls();
            JOptionPane.showMessageDialog(null, "Error retrieving selected Topping data", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
  
    /**
     * Sets UI to only display information pertinent to Drink and Side object
     * (Name, Calories, Price, Size)
     */
    private void drinksideUI(){
        lblCrust.setVisible(false);
        lblBase.setVisible(false);
        txtCrust.setVisible(false);
        txtBase.setVisible(false);
        lblTopping.setVisible(false);
        lstTopping.setVisible(false);
        lblSize.setVisible(true);
        txtSize.setVisible(true);  
    }
    
    /**
     * Sets UI to display information pertinent to Pizza object
     * (Name, Calories, Price, Size, Crust, Base, Toppings)
     */
    private void pizzaUI(){
        lblCrust.setVisible(true);
        lblBase.setVisible(true);
        txtCrust.setVisible(true);
        txtBase.setVisible(true);
        lblTopping.setVisible(true);
        lstTopping.setVisible(true);
        lblSize.setVisible(true);
        txtSize.setVisible(true);
    }
    
    /**
     * Sets UI to only display information pertinent to Topping object
     * (Name, Calories, Price)
     */
    private void toppingUI(){
        lblCrust.setVisible(false);
        lblBase.setVisible(false);
        txtCrust.setVisible(false);
        txtBase.setVisible(false);
        lblTopping.setVisible(false);
        lstTopping.setVisible(false);
        lblSize.setVisible(false);
        txtSize.setVisible(false);
    }
    
    
    private void ClearSelection(){
            lstPizza.clearSelection();
            lstSides.clearSelection();
            lstDrinks.clearSelection();
            lstToppings.clearSelection();
    }
    
    /**
     * Resets labels to default values.
     */
    private void ClearLbls(){
        txtName.setText("N/A");
        txtPrice.setText("£ 0.00");
        txtSize.setText("N/A");
        txtCal.setText("0 Cal");
        txtBase.setText("N/A");
        txtCrust.setText("N/A");
        listModelToppings.clear();
    }
    
    private void BtnDisable(){
        btnEdit.setEnabled(false);
        btnRemove.setEnabled(false);
    }

    /**
     * Converts 1 letter of input to uppercase, rest to lowercase (TEST->Test)
     * @param info String of input to change
     * @return Changed input string
     */
    private String CapsConv(String info){
        if (!info.isEmpty()){
            String firstStringLetter = info.substring(0,1).toUpperCase();
            String restofString = info.substring(1).toLowerCase();
            return firstStringLetter + restofString;
        }
        return null;
    }
    
    /**
     * Clears current list model and reinstantiates with new data from the API
     * after menu item removal.
     */
    private void RemoveRefresh(){
        menuItems.ClearCurrent();
        SyncData();
        RefreshArrays();
        RefreshListModel();
        ClearLbls();
        BtnDisable();
    }
    
    /**
     * Sends deletes item command to API based on selectedIdex of JList and tabMenu.
     */
    private void RemoveItem(){
        int confirmDelete = JOptionPane.showConfirmDialog(null, "Are you sure you want to permanently delete this item", "Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmDelete == JOptionPane.YES_OPTION){
        int index = tabMenu.getSelectedIndex();
        try {
            switch (index) {
                case 0:
                {
                    RESTfulMethods restPizza = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251G/api/pizzas");
                    Pizza objSelectedItem = null;
                    objSelectedItem = (Pizza)arrPizzaCopy.get(lstPizza.getSelectedIndex());
                    restPizza.DELETE("" + (int)Double.parseDouble(objSelectedItem.getPizza_id()));
                    RemoveRefresh();
                    break;
                }
                case 1:
                {
                    RESTfulMethods restSide = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251G/api/sides");
                    Side objSelectedItem = null;
                    objSelectedItem = (Side)arrSideCopy.get(lstSides.getSelectedIndex());
                    restSide.DELETE("" + (int)Double.parseDouble(objSelectedItem.getSide_ID()));
                    RemoveRefresh();
                    break;
                }
                case 2:
                {
                    RESTfulMethods restDrink = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251G/api/drinks");
                    Drink objSelectedItem = null;
                    objSelectedItem = (Drink)arrDrinkCopy.get(lstDrinks.getSelectedIndex());
                    restDrink.DELETE("" + (int)Double.parseDouble(objSelectedItem.getDrink_ID()));
                    RemoveRefresh();
                    break;
                }
                case 3:
                {
                    RESTfulMethods restTop = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251G/api/toppings");
                    Topping objSelectedItem = null;
                    objSelectedItem = (Topping)arrToppingCopy.get(lstToppings.getSelectedIndex());
                    restTop.DELETE("" + (int)Double.parseDouble(objSelectedItem.getToppingID()));
                    RemoveRefresh();
                    break;
                }
                default:
                    break;
            }
        }
            catch(RuntimeException e){
                JOptionPane.showMessageDialog(null, "Item is currently part of an current active\n          order and cannot be deleted"
                + " ", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * Opens GUI to add Menu item based on tabMenu selectedIndex, closes current.
     */
    private void AddItem(){
        int index = tabMenu.getSelectedIndex();
        Frame addFrame = this;
        switch (index) {
            case 0:
                {
                    addFrame = new AddPizza();
                    break;
                }
            case 1:
                {
                    addFrame = new AddSide();
                    break;
                }
            case 2:
                {
                    addFrame = new AddDrink();
                    break;
                }
            case 3:
                {
                    addFrame = new AddTopping();
                    break;
                }
            default:
                break;
        }
            addFrame.setVisible(true);
            this.dispose();
    }
    
    /**
     * Opens GUI to edit Menu item based on tabMenu and JList SelectedIndex, closes current.
     */
    private void EditItem(){
        int index = tabMenu.getSelectedIndex();
        switch (index) {
            case 0:{
                EditPizza addFrame = new EditPizza();
                addFrame.SelectedIndex(lstPizza.getSelectedIndex());
                addFrame.setVisible(true);
                break;
            }
            case 1:{
                EditSide addFrame = new EditSide();
                addFrame.SelectedIndex(lstSides.getSelectedIndex());
                addFrame.setVisible(true);
                break;
            }
            case 2:{
                EditDrink addFrame = new EditDrink();
                addFrame.SelectedIndex(lstDrinks.getSelectedIndex());
                addFrame.setVisible(true);
                break;
            }
            case 3:{
                EditTopping addFrame = new EditTopping();
                addFrame.SelectedIndex(lstToppings.getSelectedIndex());
                addFrame.setVisible(true);
                break;
            }
            default:
                break;
        }
        this.dispose();
    }
    
    public void OpenAt(int index){
            tabMenu.setSelectedIndex(index);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tabMenu = new javax.swing.JTabbedPane();
        jPizzaPane = new javax.swing.JScrollPane();
        lstPizza = new javax.swing.JList<>();
        jSidePane = new javax.swing.JScrollPane();
        lstSides = new javax.swing.JList<>();
        jDrinkPane = new javax.swing.JScrollPane();
        lstDrinks = new javax.swing.JList<>();
        jToppingPane = new javax.swing.JScrollPane();
        lstToppings = new javax.swing.JList<>();
        lblName = new javax.swing.JLabel();
        lblPrice = new javax.swing.JLabel();
        lblTopping = new javax.swing.JLabel();
        lblBase = new javax.swing.JLabel();
        txtPrice = new javax.swing.JLabel();
        txtBase = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnAdd = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        txtName = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstTopping = new javax.swing.JList<>();
        lblCrust = new javax.swing.JLabel();
        lblSize = new javax.swing.JLabel();
        txtCrust = new javax.swing.JLabel();
        txtSize = new javax.swing.JLabel();
        lblCalories = new javax.swing.JLabel();
        txtCal = new javax.swing.JLabel();
        btnEdit = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jLogOut = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tabMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabMenuMouseClicked(evt);
            }
        });

        lstPizza.setModel(listModelPizzas);
        lstPizza.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstPizzaMouseClicked(evt);
            }
        });
        jPizzaPane.setViewportView(lstPizza);

        tabMenu.addTab("Pizza", jPizzaPane);

        lstSides.setModel(listModelSides);
        lstSides.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstSidesMouseClicked(evt);
            }
        });
        jSidePane.setViewportView(lstSides);

        tabMenu.addTab("Sides", jSidePane);

        lstDrinks.setModel(listModelDrinks);
        lstDrinks.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstDrinksMouseClicked(evt);
            }
        });
        jDrinkPane.setViewportView(lstDrinks);

        tabMenu.addTab("Drinks", jDrinkPane);

        lstToppings.setModel(listModelToppingPrices);
        lstToppings.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstToppingsMouseClicked(evt);
            }
        });
        jToppingPane.setViewportView(lstToppings);

        tabMenu.addTab("Toppings", jToppingPane);

        lblName.setText("Name :");

        lblPrice.setText("Price :");

        lblTopping.setText("Toppings :");

        lblBase.setText("Base :");

        txtPrice.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtPrice.setText("£ 0.00");

        txtBase.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtBase.setText("N/A");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Menu");

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnRemove.setText("Remove");
        btnRemove.setEnabled(false);
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        txtName.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtName.setText("N/A");

        lstTopping.setModel(listModelToppings);
        jScrollPane1.setViewportView(lstTopping);

        lblCrust.setText("Crust :");

        lblSize.setText("Size :");

        txtCrust.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtCrust.setText("N/A");

        txtSize.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtSize.setText("N/A");

        lblCalories.setText("Calories :");

        txtCal.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtCal.setText("0 kcal");

        btnEdit.setText("Edit");
        btnEdit.setEnabled(false);
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jMenu1.setText("File");

        jLogOut.setText("Log Out");
        jLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jLogOutActionPerformed(evt);
            }
        });
        jMenu1.add(jLogOut);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(tabMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 331, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblPrice)
                                    .addComponent(lblSize))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txtSize, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtName, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txtPrice, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblCrust)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCrust, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lblCalories)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCal, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(lblBase)
                                .addGap(1, 1, 1)
                                .addComponent(txtBase, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTopping)
                                    .addComponent(lblName))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(jLabel1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(38, 38, 38)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblName)
                            .addComponent(txtName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPrice)
                            .addComponent(lblPrice, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblCalories)
                            .addComponent(txtCal, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblSize)
                            .addComponent(txtSize))
                        .addGap(81, 81, 81)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCrust)
                            .addComponent(lblCrust))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblBase)
                            .addComponent(txtBase))
                        .addGap(19, 19, 19)
                        .addComponent(lblTopping)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                        .addGap(40, 40, 40))
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tabMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 412, Short.MAX_VALUE)))
                .addGap(11, 11, 11)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnRemove)
                    .addComponent(btnEdit)
                    .addComponent(btnBack))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jLogOutActionPerformed
        session.ClearCurrent();
        LoginPage addFrame = new LoginPage();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLogOutActionPerformed

    private void lstPizzaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstPizzaMouseClicked
        showSelectedPizza();
        pizzaUI();
        btnEdit.setEnabled(true);
        btnRemove.setEnabled(true);
    }//GEN-LAST:event_lstPizzaMouseClicked

    private void tabMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabMenuMouseClicked
        int index = tabMenu.getSelectedIndex();
        ClearSelection();
        BtnDisable();
        if (index == 0){
            pizzaUI();
        }
        else{
            if (index == 1 || index == 2)
                drinksideUI();
            if (index == 3)
                toppingUI();
        }
        ClearLbls();
    }//GEN-LAST:event_tabMenuMouseClicked

    private void lstSidesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstSidesMouseClicked
        showSelectedSide();
        drinksideUI();
        btnEdit.setEnabled(true);
        btnRemove.setEnabled(true);
    }//GEN-LAST:event_lstSidesMouseClicked

    private void lstDrinksMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstDrinksMouseClicked
        showSelectedDrink();
        drinksideUI();
        btnEdit.setEnabled(true);
        btnRemove.setEnabled(true);
    }//GEN-LAST:event_lstDrinksMouseClicked

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        RemoveItem();
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        AddItem();
    }//GEN-LAST:event_btnAddActionPerformed

    private void lstToppingsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstToppingsMouseClicked
        showSelectedTopping();
        toppingUI();
        btnEdit.setEnabled(true);
        btnRemove.setEnabled(true);
    }//GEN-LAST:event_lstToppingsMouseClicked

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        EditItem();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        Navigation addFrame = new Navigation();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewMenu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewMenu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnRemove;
    private javax.swing.JScrollPane jDrinkPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuItem jLogOut;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jPizzaPane;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jSidePane;
    private javax.swing.JScrollPane jToppingPane;
    private javax.swing.JLabel lblBase;
    private javax.swing.JLabel lblCalories;
    private javax.swing.JLabel lblCrust;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPrice;
    private javax.swing.JLabel lblSize;
    private javax.swing.JLabel lblTopping;
    private javax.swing.JList<String> lstDrinks;
    private javax.swing.JList<String> lstPizza;
    private javax.swing.JList<String> lstSides;
    private javax.swing.JList<String> lstTopping;
    private javax.swing.JList<String> lstToppings;
    private javax.swing.JTabbedPane tabMenu;
    private javax.swing.JLabel txtBase;
    private javax.swing.JLabel txtCal;
    private javax.swing.JLabel txtCrust;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtPrice;
    private javax.swing.JLabel txtSize;
    // End of variables declaration//GEN-END:variables
}
