package GUI.Admin;

import Classes.Employee;
import Classes.ImportAPI;
import GUI.Common.LoginPage;
import Classes.JSONParser;
import Classes.Job;
import Classes.RESTfulMethods;
import Classes.Session;
import Classes.SyncAPI;
import GUI.Admin.AddEditEmployees.AddEmployee;
import GUI.Admin.AddEditEmployees.EditEmployee;
import java.awt.Frame;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

/**
 * ViewEmployees - Displays Employee information for Admin/Manager.
 * @author Luke Chilcott / Brian Young
 */
public class ViewEmployees extends javax.swing.JFrame {
    private final DefaultListModel listModelEmployee;
    private ArrayList arrEmployeeCopy, arrEmployeeJobCopy;
    private final Session session = Session.getInstance();
    private final ImportAPI menuItems = ImportAPI.getInstance();
    private final SyncAPI sync = new SyncAPI();
    private final RESTfulMethods restEmployees = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251G/api/employees");
    private final RESTfulMethods restEmployeeJobs = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251G/api/employeejobs");
    
    /**
     * Creates new form AddAdmins
     */
    public ViewEmployees() {
        this.setTitle("Employees");
        SyncData();
        RefreshArrays();
        listModelEmployee = new DefaultListModel();
        RefreshListModel();
        initComponents();
    }
    
    private void RefreshArrays(){
        arrEmployeeCopy = menuItems.getEmployee();
        arrEmployeeJobCopy = menuItems.getJob();
    }
        
    private void SyncData(){
        sync.EmployeeSync();
        sync.JobSync();
    }
        
    /** 
     * Instantiates list model with current data from Employee/Job arrays;
     */
    private void RefreshListModel(){
        String output = "";
        listModelEmployee.clear();
        
        for (int i=0; i<arrEmployeeCopy.size(); i++)
        {          
            Employee temp = (Employee)arrEmployeeCopy.get(i); 
            for (int j=0; j<arrEmployeeJobCopy.size(); j++)
            {
                Job tempJob = (Job)arrEmployeeJobCopy.get(j);
                if (tempJob.getJob_id().equals(temp.getEmployee_job_id()))
                {
                    output = CapsConv(tempJob.getJob_name());
                }
            }
            output = output.concat(" " + temp.getEmployee_name());
            listModelEmployee.addElement(output);
            output = "";
        }
    }
    
    /**
     * Displays selected employees information, calculates whether user has
     * clearance to delete or edit selected employee. 
     */
    private void showSelectedEmployee(){
        try{
            Employee objSelectedItem = null;
            objSelectedItem = (Employee) arrEmployeeCopy.get(lstEmployee.getSelectedIndex());
            txtName.setText(objSelectedItem.getEmployee_name());
            txtUser.setText(objSelectedItem.getEmployee_username());
            txtID.setText("" + (int)Double.parseDouble(objSelectedItem.getEmployee_id()));
            JSONParser job = new JSONParser(restEmployeeJobs.GET());
            job.Parse();
            txtJob.setText(CapsConv(job.GetValue("JOB_ID", objSelectedItem.getEmployee_job_id(), "JOB_NAME")));
            // If Manager (ID.4) or Admin (ID.3) is selected, and you're not a manager/selected your own name, ability to edit/remove selected is disabled.
            if (((objSelectedItem.getEmployee_job_id().equals("4.0") || objSelectedItem.getEmployee_job_id().equals("3.0")) 
                    && !session.getJobTitle().equals("MANAGER")) || objSelectedItem.getEmployee_username().equals(session.getUsername())){
                btnEdit.setEnabled(false);
                btnRemove.setEnabled(false);
            }else{
                btnEdit.setEnabled(true);
                btnRemove.setEnabled(true);
            }
        }
        catch(NullPointerException e){
            ClearLbls();
            btnEdit.setEnabled(false);
            btnRemove.setEnabled(false); 
            JOptionPane.showMessageDialog(null, "Error retrieving account clearance", "Error", JOptionPane.ERROR_MESSAGE);  
        }
    }
     
    /**
     * Opens GUI to add Employee, closes current.
     */
    private void AddItem(){
            Frame addFrame = this;
            addFrame = new AddEmployee();
            addFrame.setVisible(true);
            this.dispose();
    }
    
    /**
     * Converts 1 letter of input to uppercase, rest to lowercase (TEST->Test)
     * @param info String of input to change
     * @return Changed input string
     */
    private String CapsConv(String info){
      if (!info.isEmpty()){
        String firstStringLetter = info.substring(0,1).toUpperCase();
        String restofString = info.substring(1).toLowerCase();
        return firstStringLetter + restofString;
      }
      return null;
    }
    
    /**
     * Resets labels to default values.
     */
    private void ClearLbls(){
        txtName.setText("N/A");
        txtUser.setText("N/A");
        txtJob.setText("N/A");
        txtID.setText("N/A");
    }
    
    /**
     * Opens GUI to edit Employee selected in lstEmployee, closes current.
     */
    private void EditItem(){
        EditEmployee addFrame = new EditEmployee();
        addFrame.SelectedIndex(lstEmployee.getSelectedIndex());
        addFrame.setVisible(true);
        this.dispose();
    }

    /**
     * Removes Employee selected in lstEmployee.
     */
    private void RemoveItem(){
        int confirmDelete = JOptionPane.showConfirmDialog(null, "Are you sure you want to permanently delete this Employee", "Confirmation", JOptionPane.YES_NO_OPTION);
        if (confirmDelete == JOptionPane.YES_OPTION){
            try{
                Employee objSelectedItem = null;
                objSelectedItem = (Employee)arrEmployeeCopy.get(lstEmployee.getSelectedIndex());
                restEmployees.DELETE("" + (int)Double.parseDouble(objSelectedItem.getEmployee_id()));
                RemoveRefresh();
            }   
            catch(RuntimeException e){
                JOptionPane.showMessageDialog(null, "Person currently part of an current active\n          order and cannot be deleted"
                + " ", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    /**
     * Clears current list model and reinstantiates with new data from the API
     * after employees removal.
     */
    private void RemoveRefresh(){
        menuItems.ClearCurrent();
        SyncData();
        RefreshArrays();
        RefreshListModel();
        ClearLbls();
        BtnDisable();
    }
             
    private void BtnDisable(){
        btnEdit.setEnabled(false);
        btnRemove.setEnabled(false);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdd = new javax.swing.JButton();
        btnRemove = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtName = new javax.swing.JLabel();
        txtJob = new javax.swing.JLabel();
        txtUser = new javax.swing.JLabel();
        txtID = new javax.swing.JLabel();
        btnEdit = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        lstEmployee = new javax.swing.JList<>();
        btnBack = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jLogOut = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnAdd.setText("Add");
        btnAdd.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAddMouseClicked(evt);
            }
        });

        btnRemove.setText("Remove");
        btnRemove.setEnabled(false);
        btnRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRemoveActionPerformed(evt);
            }
        });

        jLabel2.setText("Name :");

        jLabel3.setText("Username :");

        jLabel4.setText("Job Position :");

        jLabel5.setText("Employee ID :");

        txtName.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtName.setText("N/A");
        txtName.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);

        txtJob.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtJob.setText("N/A");
        txtJob.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);

        txtUser.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        txtUser.setText("N/A");
        txtUser.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);

        txtID.setText("N/A");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtID))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addComponent(txtJob, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2)
                    .addComponent(txtName, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtUser, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtJob, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtID)
                    .addComponent(jLabel5))
                .addContainerGap(131, Short.MAX_VALUE))
        );

        btnEdit.setText("Edit");
        btnEdit.setEnabled(false);
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        lstEmployee.setModel(listModelEmployee);
        lstEmployee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstEmployeeMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(lstEmployee);

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jMenu1.setText("File");

        jLogOut.setText("Log Out");
        jLogOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jLogOutActionPerformed(evt);
            }
        });
        jMenu1.add(jLogOut);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnRemove, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAdd)
                    .addComponent(btnRemove)
                    .addComponent(btnEdit)
                    .addComponent(btnBack))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLogOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jLogOutActionPerformed
        session.ClearCurrent();
        LoginPage addFrame = new LoginPage();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jLogOutActionPerformed

    private void lstEmployeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstEmployeeMouseClicked
        showSelectedEmployee();
    }//GEN-LAST:event_lstEmployeeMouseClicked

    private void btnAddMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAddMouseClicked
        AddItem();
    }//GEN-LAST:event_btnAddMouseClicked

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        EditItem();
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRemoveActionPerformed
        RemoveItem();
    }//GEN-LAST:event_btnRemoveActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        Navigation addFrame = new Navigation();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBackActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ViewEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ViewEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ViewEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ViewEmployees.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ViewEmployees().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnRemove;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenuItem jLogOut;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JList<String> lstEmployee;
    private javax.swing.JLabel txtID;
    private javax.swing.JLabel txtJob;
    private javax.swing.JLabel txtName;
    private javax.swing.JLabel txtUser;
    // End of variables declaration//GEN-END:variables
}
