package GUI.Admin.AddEditMenuItems;

import Classes.Bases;
import Classes.Crust;
import Classes.ImportAPI;
import Classes.ItemSize;
import Classes.JSONString;
import Classes.Pizza;
import Classes.RESTfulMethods;
import Classes.Session;
import Classes.SyncAPI;
import Classes.Topping;
import GUI.Admin.ViewMenu;
import GUI.Common.LoginPage;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;


/**
 * EditPizza - Edits and submits selected Pizza information to API.
 * @author Luke Chilcott / Brian Young
 */
public class EditPizza extends javax.swing.JFrame {
    private final ImportAPI API = ImportAPI.getInstance(); 
    private final Session session = Session.getInstance();
    private final SyncAPI sync = new SyncAPI();
    private final ArrayList arrToppingCopy, arrSizeCopy, arrBaseCopy, arrCrustCopy, arrPizzaCopy;
    private final ArrayList arrSelectedToppingCopy = new ArrayList();
    private final DefaultListModel listModelAvailTop, listModelAppliedTop;
    private int count = 0;
    private final ImportAPI menuItems = ImportAPI.getInstance();
    private Pizza objSelectedItem = null;
    private double topPrice = 0.0;
    private double crustPrice = 0.0;
    private double basePrice = 0.0;
    private double sizePrice = 0.0;
    private double crustCal = 0.0;
    private double baseCal = 0.0;
    private double topCal = 0.0;
    private double pSize = 1.0;
    
    /**
     * Creates new form EditPizza
     */
    
    
    public EditPizza() {
    sync.SizeSync();
    sync.BaseSync();
    sync.CrustSync();
    sync.ToppingSync();
    sync.PizzaSync();
    arrToppingCopy = API.getTopping();
    arrSizeCopy = API.getItemSize();
    arrBaseCopy = API.getBases();
    arrCrustCopy = API.getCrust();
    arrPizzaCopy = menuItems.getPizza();
    listModelAvailTop = new DefaultListModel();
    listModelAppliedTop = new DefaultListModel();
    initComponents();
    }
    
    public void SelectedIndex(int pizzaIndex)
    {
        objSelectedItem = (Pizza)arrPizzaCopy.get(pizzaIndex);  
        this.setTitle("Edit " + objSelectedItem.getPizza_name());
        txtName.setText(objSelectedItem.getPizza_name());  
        getSizes();
        getBases();
        getCrusts();
        getToppings();
        CalcPrice();
    }

    private void getSizes(){
        String output = "";
        cbxSize.removeAllItems();
            for (int i=0; i<arrSizeCopy.size(); i++)
            {
                ItemSize temp = (ItemSize)arrSizeCopy.get(i);
                output = temp.getSize_name();
                cbxSize.addItem(CapsConv(output)); 
                if (temp.getSize_id().equals(objSelectedItem.getPizza_size_id())){
                    cbxSize.setSelectedIndex(i);
                    sizePrice = Double.parseDouble(temp.getSize_price());
                }
                output = "";
            }
            System.out.println(arrSizeCopy);
    }
    
    private void getBases(){
       String output = "";
            for (int i=0; i<arrBaseCopy.size(); i++)
            {
                Bases temp = (Bases)arrBaseCopy.get(i);
                output = temp.getBase_Name();
                cbxBase.addItem(CapsConv(output));
                if (temp.getBase_ID().equals(objSelectedItem.getBase_id())){
                    cbxBase.setSelectedIndex(i);
                    basePrice = Double.parseDouble(temp.getBase_Price());
                }
                output = "";
            }
    }
        
    private void getCrusts(){
        String output = "";
            for (int i=0; i<arrCrustCopy.size(); i++)
            {
                Crust temp = (Crust)arrCrustCopy.get(i);
                output = temp.getCrust_Name();
                cbxCrust.addItem(CapsConv(output));
                if (temp.getCrust_ID().equals(objSelectedItem.getCrust_id())){
                    cbxCrust.setSelectedIndex(i);
                    crustPrice = Double.parseDouble(temp.getCrust_Price());
                }
                output = "";
            }
    }
    
    private void getToppings(){
        String output = "";
            for (int i=0; i<arrToppingCopy.size(); i++)
            {
                Topping temp = (Topping)arrToppingCopy.get(i);
                output = temp.getToppingName();
                System.out.println(output);
                if (temp.getToppingID().equals(objSelectedItem.getTopping1())||temp.getToppingID().equals(objSelectedItem.getTopping2())
                        ||temp.getToppingID().equals(objSelectedItem.getTopping3())||temp.getToppingID().equals(objSelectedItem.getTopping4())
                        ||temp.getToppingID().equals(objSelectedItem.getTopping5())){
                    topCal += Double.parseDouble(temp.getCalories());
                    arrSelectedToppingCopy.add(temp);
                    arrToppingCopy.remove(i);
                    listModelAppliedTop.addElement(CapsConv(output));
                    topPrice = topPrice + Double.parseDouble(temp.getToppingPrice());
                    count++;
                    i--;
                }else{
                    listModelAvailTop.addElement(CapsConv(output));
                }
                output = "";
            }
                                txtCal.setText("" + ((crustCal + baseCal + topCal) * pSize));
    }
    
    
    private String CapsConv(String info){
      if (!info.isEmpty()){
        String firstStringLetter = info.substring(0,1).toUpperCase();
        String restofString = info.substring(1).toLowerCase();
        return firstStringLetter + restofString;
      }
      return null;
    }
    

    private void CalcPrice(){
        txtTotalPrice.setText("" + (Math.round((sizePrice + basePrice + crustPrice + topPrice)*100.00))/100.00);
    }
    
    private void UpdateSizePrice(){
        for (int i=0; i<arrSizeCopy.size(); i++)
        {
            ItemSize temp = (ItemSize)arrSizeCopy.get(i);
            if (CapsConv(temp.getSize_name()).equals((String)cbxSize.getSelectedItem())){
                sizePrice = Double.parseDouble(temp.getSize_price());
                pSize = Double.parseDouble(temp.getSize_id());
                txtCal.setText("" + ((crustCal + baseCal + topCal) * pSize));
            }
        }  
        CalcPrice();
    }
    
    private void UpdateBasePrice()
    {
        for (int i=0; i<arrBaseCopy.size(); i++){
            Bases temp = (Bases)arrBaseCopy.get(i);
            if (CapsConv(temp.getBase_Name()).equals((String)cbxBase.getSelectedItem())){
                basePrice = Double.parseDouble(temp.getBase_Price());
                baseCal = Double.parseDouble(temp.getBase_Calories());
                txtCal.setText("" + ((crustCal + baseCal + topCal) * pSize));
            }
        }  
        CalcPrice();
    }
    
    private void UpdateCrustPrice(){
        for (int i=0; i<arrCrustCopy.size(); i++)
        {
            Crust temp = (Crust)arrCrustCopy.get(i);
            if (CapsConv(temp.getCrust_Name()).equals((String)cbxCrust.getSelectedItem())){
                crustPrice = Double.parseDouble(temp.getCrust_Price());
                crustCal = Double.parseDouble(temp.getCalories());
                txtCal.setText("" + ((crustCal + baseCal + topCal) * pSize));
            }
        }  
        CalcPrice();
        
    }
    
    private void UpdateTopPrice(){
        topPrice = 0.0;
        topCal = 0.0;
        for (int i=0; i<arrSelectedToppingCopy.size(); i++)
        {
           Topping temp = (Topping)arrSelectedToppingCopy.get(i);
           topPrice = topPrice + Double.parseDouble(temp.getToppingPrice());
           topCal += Double.parseDouble(temp.getCalories());
        }
        txtCal.setText("" + ((crustCal + baseCal + topCal) * pSize));
        if (arrSelectedToppingCopy.isEmpty()){
            txtCal.setText("" + ((crustCal + baseCal) * pSize));}
        CalcPrice();
    }
    
    private void EnableTopping(){
        try{
            btnArrLeft.setEnabled(true);
            Topping objSelectedItem = (Topping)arrToppingCopy.get(lstAvTopping.getSelectedIndex());
            arrSelectedToppingCopy.add(objSelectedItem);
            arrToppingCopy.remove(lstAvTopping.getSelectedIndex());
            listModelAppliedTop.addElement(CapsConv(objSelectedItem.getToppingName()));
            listModelAvailTop.remove(lstAvTopping.getSelectedIndex());
            count++;
            if (count >= 5){
                lstAvTopping.setEnabled(false);
                btnArrRight.setEnabled(false);
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(null, "No Topping Selected", "No Topping Selected", JOptionPane.ERROR_MESSAGE);
        }
    }
    private void DisableTopping(){
        try{
            Topping objSelectedItem = (Topping)arrSelectedToppingCopy.get(lstAppliedTopping.getSelectedIndex());
            arrToppingCopy.add(objSelectedItem);
            arrSelectedToppingCopy.remove(lstAppliedTopping.getSelectedIndex());
            listModelAvailTop.addElement(CapsConv(objSelectedItem.getToppingName()));
            listModelAppliedTop.remove(lstAppliedTopping.getSelectedIndex());
            count--;
            if(count <= 5){
                lstAvTopping.setEnabled(true);
                btnArrRight.setEnabled(true);
            }
            if (count == 0){
                btnArrLeft.setEnabled(false);
            }
        }
        catch(ArrayIndexOutOfBoundsException e){
            JOptionPane.showMessageDialog(null, "No Topping Selected", "No Topping Selected", JOptionPane.ERROR_MESSAGE);
        }
    }
    
     private void SubmitPizza(){
        RESTfulMethods restPizza = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/pizzas");
        JSONString JS = new JSONString();
        double totalCal = 0;
        JS.AddElement("PIZZA_NAME", txtName.getText());
        ItemSize sizeTemp = (ItemSize)arrSizeCopy.get(cbxSize.getSelectedIndex());
        JS.AddElement("PIZZA_SIZE_ID", sizeTemp.getSize_id());
        JS.AddElement("PIZZA_ID", Double.parseDouble(objSelectedItem.getPizza_id()));
        Crust crustTemp = (Crust)arrCrustCopy.get(cbxCrust.getSelectedIndex());
        JS.AddElement("PIZZA_CRUST_ID", crustTemp.getCrust_ID());
        Bases baseTemp = (Bases)arrBaseCopy.get(cbxBase.getSelectedIndex());
        JS.AddElement("PIZZA_BASE_ID", baseTemp.getBase_ID());
        Topping toppingTemp = null;
        String[] topNameArray = {"ONE", "TWO", "THREE", "FOUR", "FIVE"};
        totalCal += Double.parseDouble(crustTemp.getCalories()) + Double.parseDouble(baseTemp.getBase_Calories());
        for (int j = 0; j < arrSelectedToppingCopy.size(); j++)
        {
           toppingTemp = (Topping)arrSelectedToppingCopy.get(j);
           totalCal += Double.parseDouble(toppingTemp.getCalories());
           JS.AddElement("PIZZA_TOPPING_" + topNameArray[j] + "_ID", toppingTemp.getToppingID() );
        }

        try{
            totalCal = totalCal * Double.parseDouble(sizeTemp.getSize_id());
            JS.AddElement("PIZZA_CALORIES", totalCal);
            double totPrice = Math.round((Double.parseDouble(txtTotalPrice.getText())*100.00))/100.00;
            JS.AddElement("PIZZA_PRICE", totPrice);
            restPizza.PUT("" + (int)Double.parseDouble(objSelectedItem.getPizza_id()), JS.GetJSON()); 
            ViewMenu addFrame = new ViewMenu();
            addFrame.OpenAt(0);
            addFrame.setVisible(true);
            this.dispose();
        }
        catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "Non-numeric characters are not allow for Price and Calories", "Incorrect format", JOptionPane.ERROR_MESSAGE);
        }
        catch(RuntimeException e){
            JOptionPane.showMessageDialog(null, "This Pizza is currently part of an current active\n          order and cannot be edited"
                                + " ", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        lstAvTopping = new javax.swing.JList<>();
        jLabel5 = new javax.swing.JLabel();
        cbxBase = new javax.swing.JComboBox<>();
        cbxCrust = new javax.swing.JComboBox<>();
        jPanel3 = new javax.swing.JPanel();
        btnSubmit = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        txtTotalPrice = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        chkCustomPrice = new javax.swing.JCheckBox();
        txtCal = new javax.swing.JTextField();
        lblCal = new javax.swing.JLabel();
        lblCalUnit = new javax.swing.JLabel();
        txtTitle = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        cbxSize = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstAppliedTopping = new javax.swing.JList<>();
        jArrowPanel = new javax.swing.JPanel();
        btnArrRight = new javax.swing.JButton();
        btnArrLeft = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel4.setText("Base :");

        lstAvTopping.setModel(listModelAvailTop);
        lstAvTopping.setDragEnabled(true);
        lstAvTopping.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstAvToppingMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(lstAvTopping);

        jLabel5.setText("Crust :");

        cbxBase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxBaseActionPerformed(evt);
            }
        });

        cbxCrust.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxCrustActionPerformed(evt);
            }
        });

        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(18, Short.MAX_VALUE)
                .addComponent(btnSubmit)
                .addGap(18, 18, 18)
                .addComponent(btnCancel)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancel)
                    .addComponent(btnSubmit))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        txtTotalPrice.setEditable(false);
        txtTotalPrice.setText("0.00");

        jLabel6.setText("Total Price :  £");

        chkCustomPrice.setText("Custom Pizza Price");
        chkCustomPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chkCustomPriceActionPerformed(evt);
            }
        });

        txtCal.setEditable(false);

        lblCal.setText("Total Calories :");

        lblCalUnit.setText("kcal");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(chkCustomPrice)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblCal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCal, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblCalUnit)
                .addGap(4, 4, 4))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txtTotalPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCal)
                    .addComponent(lblCalUnit))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chkCustomPrice))
        );

        txtTitle.setFont(new java.awt.Font("SWItal", 3, 14)); // NOI18N
        txtTitle.setText("Edit Pizza");

        jLabel2.setText("Name :");

        txtName.setEditable(false);
        txtName.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtNameKeyPressed(evt);
            }
        });

        jLabel3.setText("Size :");

        cbxSize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbxSizeActionPerformed(evt);
            }
        });

        lstAppliedTopping.setModel(listModelAppliedTop);
        lstAppliedTopping.setDragEnabled(true);
        lstAppliedTopping.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstAppliedToppingMouseClicked(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                lstAppliedToppingMouseReleased(evt);
            }
        });
        jScrollPane2.setViewportView(lstAppliedTopping);

        btnArrRight.setText(">");
        btnArrRight.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArrRightActionPerformed(evt);
            }
        });

        btnArrLeft.setText("<");
        btnArrLeft.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnArrLeftActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jArrowPanelLayout = new javax.swing.GroupLayout(jArrowPanel);
        jArrowPanel.setLayout(jArrowPanelLayout);
        jArrowPanelLayout.setHorizontalGroup(
            jArrowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jArrowPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jArrowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnArrLeft, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnArrRight, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        jArrowPanelLayout.setVerticalGroup(
            jArrowPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jArrowPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnArrRight)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnArrLeft)
                .addGap(54, 54, 54))
        );

        jMenu1.setText("File");

        jMenuItem1.setText("Log Out");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 268, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbxSize, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cbxBase, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 143, Short.MAX_VALUE)
                                .addComponent(cbxCrust, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jArrowPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                        .addGap(9, 9, 9)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtTitle)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbxSize, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbxBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbxCrust, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jArrowPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(11, 11, 11)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        SubmitPizza();    
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        ViewMenu addFrame = new ViewMenu();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void chkCustomPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chkCustomPriceActionPerformed
        if (chkCustomPrice.isSelected())
        {
            txtTotalPrice.setEditable(true);
        } else {
            txtTotalPrice.setEditable(false);
            CalcPrice();
        }
    }//GEN-LAST:event_chkCustomPriceActionPerformed

    private void txtNameKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNameKeyPressed

    }//GEN-LAST:event_txtNameKeyPressed

    private void lstAppliedToppingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstAppliedToppingMouseClicked
        lstAvTopping.clearSelection();
    }//GEN-LAST:event_lstAppliedToppingMouseClicked

    private void lstAppliedToppingMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstAppliedToppingMouseReleased

    }//GEN-LAST:event_lstAppliedToppingMouseReleased

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        session.ClearCurrent();
        LoginPage addFrame = new LoginPage();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void cbxSizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxSizeActionPerformed
        UpdateSizePrice();
    }//GEN-LAST:event_cbxSizeActionPerformed

    private void cbxBaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxBaseActionPerformed
        UpdateBasePrice();
    }//GEN-LAST:event_cbxBaseActionPerformed

    private void cbxCrustActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbxCrustActionPerformed
        UpdateCrustPrice();       
    }//GEN-LAST:event_cbxCrustActionPerformed

    private void btnArrRightActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArrRightActionPerformed
        EnableTopping();
        UpdateTopPrice();
    }//GEN-LAST:event_btnArrRightActionPerformed

    private void btnArrLeftActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnArrLeftActionPerformed
        DisableTopping();
        UpdateTopPrice();
    }//GEN-LAST:event_btnArrLeftActionPerformed

    private void lstAvToppingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstAvToppingMouseClicked
        lstAppliedTopping.clearSelection();
    }//GEN-LAST:event_lstAvToppingMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddPizza.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddPizza.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddPizza.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddPizza.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EditPizza().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnArrLeft;
    private javax.swing.JButton btnArrRight;
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JComboBox<String> cbxBase;
    private javax.swing.JComboBox<String> cbxCrust;
    private javax.swing.JComboBox<String> cbxSize;
    private javax.swing.JCheckBox chkCustomPrice;
    private javax.swing.JPanel jArrowPanel;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblCal;
    private javax.swing.JLabel lblCalUnit;
    private javax.swing.JList<String> lstAppliedTopping;
    private javax.swing.JList<String> lstAvTopping;
    private javax.swing.JTextField txtCal;
    private javax.swing.JTextField txtName;
    private javax.swing.JLabel txtTitle;
    private javax.swing.JTextField txtTotalPrice;
    // End of variables declaration//GEN-END:variables
}
