package GUI.Admin.AddEditEmployees;

import Classes.Employee;
import Classes.ImportAPI;
import Classes.JSONString;
import Classes.Job;
import Classes.RESTfulMethods;
import Classes.Session;
import Classes.SyncAPI;
import Exceptions.PasswordException;
import GUI.Admin.ViewEmployees;
import GUI.Common.LoginPage;
import classes.StringHashGenerator;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 * AddEmployee Adds Employee to API from inputted data.
 * @author Luke Chilcott / Brian Young
 */
public class AddEmployee extends javax.swing.JFrame {
    private final ImportAPI API = ImportAPI.getInstance(); 
    private final SyncAPI sync = new SyncAPI();
    private final ArrayList arrJobCopy, arrEmployeeCopy;
    private final Employee objSelectedItem = null;
    private final Session session = Session.getInstance();

    /**
     * Creates new form AddAdmin
     */
    public AddEmployee() {
        this.setTitle("Add Employee");
        sync.JobSync();
        sync.EmployeeSync();
        arrJobCopy = API.getJob();
        arrEmployeeCopy = API.getEmployee();
        initComponents();
        getJobs();
    }
    
    /**
     * Get a list of available jobs, only adds Admin/Manager 
     * options if logged in account is a manager
     */
    private void getJobs(){
        String output = "";
        cbxJob.removeAllItems();
            for (int i=0; i<arrJobCopy.size(); i++)
            {
                Job temp = (Job)arrJobCopy.get(i);
                output = temp.getJob_name();
                if ((output.equals("MANAGER") || output.equals("ADMINISTRATOR")) && !session.getJobTitle().equals("MANAGER")){}
                else{ cbxJob.addItem(CapsConv(output)); }
                output = "";
            }     
    }
    
    private String CapsConv(String info){
      if (!info.isEmpty()){
        String firstStringLetter = info.substring(0,1).toUpperCase();
        String restofString = info.substring(1).toLowerCase();
        return firstStringLetter + restofString;
      }
      return null;
    }
    
    private void PasswordChk(){
        String passwordChk = String.valueOf(pfPass.getPassword());
        String passwordConChk = String.valueOf(pfConPass.getPassword());
         
        if (passwordChk.equals("") && (passwordConChk.equals(""))){
            pfConPass.setEnabled(false);}
        else{
            pfConPass.setEnabled(true);}
    }
    
    private void SubmitEmployee(){
        System.out.println(String.valueOf(pfPass.getPassword()));
        boolean dupeUser = false;
        for (int i=0; i<arrEmployeeCopy.size(); i++){
            Employee temp = (Employee)arrEmployeeCopy.get(i); 
            if (temp.getEmployee_username().equals(txtUser.getText().toLowerCase()))
                dupeUser = true;
        }
         
        if (dupeUser == false){
            if (!"".equals(String.valueOf(pfPass.getPassword())) && !"".equals(txtName.getText()) && !"".equals(txtUser.getText())){
                RESTfulMethods restEmployee = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/employees");
                JSONString JS = new JSONString();
                Boolean f = true;
                int i = 0;
                while (f)
                {
                    try{
                        restEmployee.GET("" + i);
                        i++;
                    }catch(RuntimeException e){
                        f=false;
                    }          
                }
        
                try{
                    String password = "";
                String encodedSalt = "";
                String passwordChk = String.valueOf(pfPass.getPassword());
                String passwordConChk = String.valueOf(pfConPass.getPassword());
                if (passwordChk.equals(passwordConChk)){
                    password = String.valueOf(pfPass.getPassword());
                    StringHashGenerator hasher = new StringHashGenerator();
                    final Random num = new SecureRandom();
                    byte[] salt = new byte[32];
                    num.nextBytes(salt);
                    encodedSalt = Base64.getEncoder().encodeToString(salt);
                    password = hasher.SHA512(password, encodedSalt);
                    String salt2 = "yoqy3Gu+AiSetUAjuNcdgmCE/ybCEFKJyVWKpW0Jb4c=";
                    password = hasher.SHA512(password, salt2);
                }else{
                    throw new PasswordException();
                }
                JS.AddElement("EMPLOYEE_NAME", txtName.getText());
                Job jobTemp = (Job)arrJobCopy.get(cbxJob.getSelectedIndex());
                JS.AddElement("EMPLOYEE_ID", i);
                JS.AddElement("EMPLOYEE_JOB_ID", (int)Double.parseDouble(jobTemp.getJob_id()));
                JS.AddElement("EMPLOYEE_USERNAME", txtUser.getText().toLowerCase());
                JS.AddElement("EMPLOYEE_PASSWORD", password);
                JS.AddElement("EMPLOYEE_SALT", encodedSalt);
                restEmployee.POST(JS.GetJSON()); 
                ViewEmployees addFrame = new ViewEmployees();
                addFrame.setVisible(true);
                this.dispose();
                }
                catch(PasswordException e){
                    JOptionPane.showMessageDialog(null, "Passwords do not match", "Password Confirmation Mismatch", JOptionPane.ERROR_MESSAGE);
                }   
            }else{
                JOptionPane.showMessageDialog(null, "Please fill all information fields", "Empty information field", JOptionPane.ERROR_MESSAGE);
            }
        }else{
            JOptionPane.showMessageDialog(null, "Account with this username already exists, please choose another", "Username match", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtTitle = new javax.swing.JLabel();
        btnSubmit = new javax.swing.JButton();
        btnCancel = new javax.swing.JButton();
        lblUser = new javax.swing.JLabel();
        lblName = new javax.swing.JLabel();
        lblPass = new javax.swing.JLabel();
        lblConfirmPass = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        txtUser = new javax.swing.JTextField();
        lblAccType = new javax.swing.JLabel();
        cbxJob = new javax.swing.JComboBox<>();
        pfPass = new javax.swing.JPasswordField();
        pfConPass = new javax.swing.JPasswordField();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txtTitle.setFont(new java.awt.Font("SWItal", 3, 14)); // NOI18N
        txtTitle.setText("Add Employee");

        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        btnCancel.setText("Cancel");
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });

        lblUser.setText("Username :");

        lblName.setText("Name :");

        lblPass.setText("Password :");

        lblConfirmPass.setText("Confirm Password :");

        lblAccType.setText("Account Type :");

        pfPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pfPassKeyReleased(evt);
            }
        });

        pfConPass.setEnabled(false);
        pfConPass.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                pfConPassKeyReleased(evt);
            }
        });

        jMenu1.setText("File");

        jMenuItem1.setText("Log Out");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnSubmit, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancel, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblAccType)
                                    .addComponent(lblName)
                                    .addComponent(lblUser))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(47, 47, 47)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txtName)
                                            .addComponent(cbxJob, 0, 120, Short.MAX_VALUE)))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblPass)
                                    .addComponent(lblConfirmPass))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(pfConPass, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
                                    .addComponent(pfPass))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblName)
                    .addComponent(txtName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAccType)
                    .addComponent(cbxJob, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUser)
                    .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pfPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblPass))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblConfirmPass)
                    .addComponent(pfConPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSubmit)
                    .addComponent(btnCancel))
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        session.ClearCurrent();
        LoginPage addFrame = new LoginPage();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        ViewEmployees addFrame = new ViewEmployees();
        addFrame.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnCancelActionPerformed

    private void pfPassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pfPassKeyReleased
        PasswordChk();
    }//GEN-LAST:event_pfPassKeyReleased

    private void pfConPassKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_pfConPassKeyReleased
        PasswordChk();
    }//GEN-LAST:event_pfConPassKeyReleased

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        SubmitEmployee();
    }//GEN-LAST:event_btnSubmitActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Windows look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AddEmployee.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AddEmployee().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSubmit;
    private javax.swing.JComboBox<String> cbxJob;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JLabel lblAccType;
    private javax.swing.JLabel lblConfirmPass;
    private javax.swing.JLabel lblName;
    private javax.swing.JLabel lblPass;
    private javax.swing.JLabel lblUser;
    private javax.swing.JPasswordField pfConPass;
    private javax.swing.JPasswordField pfPass;
    private javax.swing.JTextField txtName;
    private javax.swing.JLabel txtTitle;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables
}
