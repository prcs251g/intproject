package Classes;

/**
 * Session - Singleton class, Enables log-on authentication to be read 
 * across the application. 
 * @author Luke Chilcott / Brian Young
 */
public class Session 
{
    private String username = null;
    private String jobTitle = null;
    private static Session instance = new Session();
    
    private Session(){}
    
    public static Session getInstance()
    {
        return instance;
    }

    public void ClearCurrent()
    {
        this.username = null;
        this.jobTitle = null;
    }
    
    public boolean ChkAuth()
    {
        if (this.jobTitle==(null) || this.username==(null)){
            return true;
        }else{
            return false;
        }
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
    
    
}
