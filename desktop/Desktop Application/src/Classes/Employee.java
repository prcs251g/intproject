package Classes;

/**
 * Employee - Class to instantiate customer objects.
 * @author Luke Chilcott / Brian Young
 */
public class Employee {
    private String employee_id;
    private String employee_name;
    private String employee_username;
    private String employee_password;
    private String employee_salt;
    private String employee_job_id;

    public Employee(String employee_id, String employee_name, String employee_username, String employee_password, String employee_salt, String employee_job_id) {
        this.employee_id = employee_id;
        this.employee_name = employee_name;
        this.employee_username = employee_username;
        this.employee_password = employee_password;
        this.employee_salt = employee_salt;
        this.employee_job_id = employee_job_id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getEmployee_username() {
        return employee_username;
    }

    public void setEmployee_username(String employee_username) {
        this.employee_username = employee_username;
    }

    public String getEmployee_password() {
        return employee_password;
    }

    public void setEmployee_password(String employee_password) {
        this.employee_password = employee_password;
    }

    public String getEmployee_salt() {
        return employee_salt;
    }

    public void setEmployee_salt(String employee_salt) {
        this.employee_salt = employee_salt;
    }

    public String getEmployee_job_id() {
        return employee_job_id;
    }

    public void setEmployee_job_id(String employee_job_id) {
        this.employee_job_id = employee_job_id;
    }
    
}
