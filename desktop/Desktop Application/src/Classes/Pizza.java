package Classes;

/**
 * Pizza -  Class to instantiate Pizza objects.
 * @author Luke Chilcott / Brian Young
 */
public class Pizza {
    private String base_id;
    private String totalCalories;
    private String crust_id;
    private String pizza_id;
    private String pizza_name;
    private String pizza_price;
    private String pizza_size_id;
    private String topping5, topping4, topping3, topping2, topping1;
    
    public Pizza(String pizza_id, String pizza_name , String base_id, String crust_id, String pizza_size_id,
            String topping1, String topping2, String topping3, String topping4, String topping5,
            String pizza_price, String totalCalories  
            )
    {
       this.base_id = base_id;
       this.totalCalories = totalCalories;
       this.crust_id = crust_id;
       this.pizza_id = pizza_id;
       this.pizza_name = pizza_name;
       this.pizza_price = pizza_price;
       this.pizza_size_id = pizza_size_id;
       this.topping5 = topping5;
       this.topping4 = topping4;
       this.topping3 = topping3;
       this.topping2 = topping2;
       this.topping1 = topping1;
    }

    public String getBase_id() {
        return base_id;
    }

    public String getTotalCalories() {
        return totalCalories;
    }

    public String getCrust_id() {
        return crust_id;
    }

    public String getPizza_id() {
        return pizza_id;
    }

    public String getPizza_name() {
        return pizza_name;
    }

    public String getPizza_price() {
        return pizza_price;
    }

    public String getPizza_size_id() {
        return pizza_size_id;
    }

    public String getTopping5() {
        return topping5;
    }

    public String getTopping4() {
        return topping4;
    }

    public String getTopping3() {
        return topping3;
    }

    public String getTopping2() {
        return topping2;
    }

    public String getTopping1() {
        return topping1;
    }

    public void setBase_id(String base_id) {
        this.base_id = base_id;
    }

    public void setTotalCalories(String totalCalories) {
        this.totalCalories = totalCalories;
    }

    public void setCrust_id(String crust_id) {
        this.crust_id = crust_id;
    }

    public void setPizza_id(String pizza_id) {
        this.pizza_id = pizza_id;
    }

    public void setPizza_name(String pizza_name) {
        this.pizza_name = pizza_name;
    }

    public void setPizza_price(String pizza_price) {
        this.pizza_price = pizza_price;
    }

    public void setPizza_size_id(String pizza_size_id) {
        this.pizza_size_id = pizza_size_id;
    }

    public void setTopping5(String topping5) {
        this.topping5 = topping5;
    }

    public void setTopping4(String topping4) {
        this.topping4 = topping4;
    }

    public void setTopping3(String topping3) {
        this.topping3 = topping3;
    }

    public void setTopping2(String topping2) {
        this.topping2 = topping2;
    }

    public void setTopping1(String topping1) {
        this.topping1 = topping1;
    }
    
}
