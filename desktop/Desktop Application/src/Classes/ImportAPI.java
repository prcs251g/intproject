package Classes;

import java.util.ArrayList;

/**
 * ImportAPI - Stores information from SyncAPI to be accessed in application.
 * @author Luke Chilcott / Brian Young
 */
public class ImportAPI {
    private final ArrayList arrBases = new ArrayList<Bases>();
    private final ArrayList arrCity = new ArrayList<City>();
    private final ArrayList arrCrust = new ArrayList<Crust>();
    private final ArrayList arrCustomers = new ArrayList<Customers>();
    private final ArrayList arrDrink = new ArrayList<Drink>();
    private final ArrayList arrEmployee = new ArrayList<Employee>();
    private final ArrayList arrItemSize = new ArrayList<ItemSize>();
    private final ArrayList arrJob = new ArrayList<Job>();
    private final ArrayList arrPayment = new ArrayList<Payment>();
    private final ArrayList arrPizza = new ArrayList<Pizza>();
    private final ArrayList arrSide = new ArrayList<Side>();
    private final ArrayList arrTopping = new ArrayList<Topping>();
    private final ArrayList arrOrderItems = new ArrayList<OrderItem>();
    private final ArrayList arrOrders = new ArrayList<Order>();
    private final ArrayList arrOrderHistory = new ArrayList<OrderHistory>();
    private static final ImportAPI sess = new ImportAPI();
    private ImportAPI(){}
    
    public static ImportAPI getInstance(){
        return sess;
    }
    
    public void ClearCurrent(){
        arrBases.clear();
        arrCity.clear();
        arrCrust.clear();
        arrCustomers.clear();
        arrDrink.clear();
        arrEmployee.clear();
        arrItemSize.clear();
        arrJob.clear();
        arrPayment.clear();
        arrPizza.clear();
        arrSide.clear();
        arrTopping.clear();
        arrOrders.clear();
        arrOrderItems.clear();
        arrOrderHistory.clear();
    }

    public ArrayList<Bases> getBases() {
        ArrayList arrBasesCopy = new ArrayList<>();
        for (int i=0; i<arrBases.size(); i++)
        {
           Bases Temp = (Bases)arrBases.get(i); 
           arrBasesCopy.add(Temp);
        }
        return arrBasesCopy;       
    }

    public Boolean setBases(Bases newBase) {
        if(newBase != null)
        {
            arrBases.add(newBase);
            return true;
        }
        return false;
    }

    public ArrayList<City> getCity() {
        ArrayList arrCityCopy = new ArrayList<>();
        for (int i=0; i<arrCity.size(); i++)
        {
           City Temp = (City)arrCity.get(i); 
           arrCityCopy.add(Temp);
        }
        return arrCityCopy;       
    }

    public Boolean setCity(City newCity) {
        if(newCity != null)
        {
            arrCity.add(newCity);
            return true;
        }
        return false;
    }

    public ArrayList<Crust> getCrust() {
        ArrayList arrCrustCopy = new ArrayList<>();
        for (int i=0; i<arrCrust.size(); i++)
        {
           Crust Temp = (Crust)arrCrust.get(i); 
           arrCrustCopy.add(Temp);
        }
        return arrCrustCopy;     
    }

    public Boolean setCrust(Crust newCrust) {
        if(newCrust != null)
        {
            arrCrust.add(newCrust);
            return true;
        }
        return false;
    }

    public ArrayList<Customers> getCustomers() {
        ArrayList arrCustCopy = new ArrayList<>();
        for (int i=0; i<arrCustomers.size(); i++)
        {
           Customers Temp = (Customers)arrCustomers.get(i); 
           arrCustCopy.add(Temp);
        }
        return arrCustCopy; 
    }

    public Boolean setCustomers(Customers newCustomers) {
       if(newCustomers != null)
        {
            arrCustomers.add(newCustomers);
            return true;
        }
        return false;
    }

    public ArrayList<Drink> getDrink() {
        ArrayList arrDrinkCopy = new ArrayList<>();
        for (int i=0; i<arrDrink.size(); i++)
        {
           Drink Temp = (Drink)arrDrink.get(i); 
           arrDrinkCopy.add(Temp);
        }
        return arrDrinkCopy; 
    }
    
    public Boolean setDrink(Drink newDrink) {
        if(newDrink != null)
        {
            arrDrink.add(newDrink);
            return true;
        }
        return false;
    }

    public ArrayList<Employee> getEmployee() {
        ArrayList arrEmployeeCopy = new ArrayList<>();
        for (int i=0; i<arrEmployee.size(); i++)
        {
           Employee Temp = (Employee)arrEmployee.get(i); 
           arrEmployeeCopy.add(Temp);
        }
        return arrEmployeeCopy; 
    }

    public Boolean setEmployee(Employee newEmployee) {
        if(newEmployee != null)
        {
            arrEmployee.add(newEmployee);
            return true;
        }
        return false;
    }

    public ArrayList<ItemSize> getItemSize() {
        ArrayList arrItemSizeCopy = new ArrayList<>();
        for (int i=0; i<arrItemSize.size(); i++)
        {
           ItemSize Temp = (ItemSize)arrItemSize.get(i); 
           arrItemSizeCopy.add(Temp);
        }
        return arrItemSizeCopy; 

    }

    public Boolean setItemSize(ItemSize newItemSize) {
        if(newItemSize != null)
        {
            arrItemSize.add(newItemSize);
            return true;
        }
        return false;
    }

    public ArrayList<Job> getJob() {
        ArrayList arrJobCopy = new ArrayList<>();
        for (int i=0; i<arrJob.size(); i++)
        {
           Job Temp = (Job)arrJob.get(i); 
           arrJobCopy.add(Temp);
        }
        return arrJobCopy; 
    }

    public Boolean setJob(Job newJob) {
        if(newJob != null)
        {
            arrJob.add(newJob);
            return true;
        }
        return false;
    }

    public ArrayList<Payment> getPayment() {
        ArrayList arrPaymentCopy = new ArrayList<>();
        for (int i=0; i<arrPayment.size(); i++)
        {
           Payment Temp = (Payment)arrPayment.get(i); 
           arrPaymentCopy.add(Temp);
        }
        return arrPaymentCopy; 
    }

    public Boolean setPayment(Payment newPayment) {
        if(newPayment != null)
        {
            arrPayment.add(newPayment);
            return true;
        }
        return false;
    }

    public ArrayList<Pizza> getPizza() {
        ArrayList arrPizzaCopy = new ArrayList<>();
        for (int i=0; i<arrPizza.size(); i++)
        {
           Pizza Temp = (Pizza)arrPizza.get(i); 
           arrPizzaCopy.add(Temp);
        }
        return arrPizzaCopy; 
    }

    public Boolean setPizza(Pizza newPizza) {
        if(newPizza != null)
        {
            arrPizza.add(newPizza);
            return true;
        }
        return false;
    }

    public ArrayList<Side> getSide() {
        ArrayList arrSideCopy = new ArrayList<>();
        for (int i=0; i<arrSide.size(); i++)
        {
           Side Temp = (Side)arrSide.get(i); 
           arrSideCopy.add(Temp);
        }
        return arrSideCopy; 
    }

    public Boolean setSide(Side newSide) {
        if(newSide != null)
        {
            arrSide.add(newSide);
            return true;
        }
        return false;
    }

    public ArrayList<Topping> getTopping() {
        ArrayList arrToppingCopy = new ArrayList<>();
        for (int i=0; i<arrTopping.size(); i++)
        {
           Topping Temp = (Topping)arrTopping.get(i); 
           arrToppingCopy.add(Temp);
        }
        return arrToppingCopy; 
    }

    public Boolean setTopping(Topping newTopping) {
        if(newTopping != null)
        {
            arrTopping.add(newTopping);
            return true;
        }
        return false;
    }
    
    public ArrayList<OrderItem> getOrderItem()
    {
        ArrayList arrOrderItemCopy = new ArrayList<>();
        for (int i=0; i<arrOrderItems.size(); i++)
        {
           OrderItem Temp = (OrderItem)arrOrderItems.get(i); 
           arrOrderItemCopy.add(Temp);
        }
        return arrOrderItemCopy; 
    }
    
    public Boolean setOrderItem(OrderItem newOrderItem)
    {
        if(newOrderItem != null)
        {
            arrOrderItems.add(newOrderItem);
            return true;
        }
        return false;
    }
    
    public ArrayList<Order> getOrder()
    {
        ArrayList arrOrderCopy = new ArrayList<>();
        for (int i=0; i<arrOrders.size(); i++)
        {
           Order Temp = (Order)arrOrders.get(i); 
           arrOrderCopy.add(Temp);
        }
        return arrOrderCopy; 
    }
    
    public Boolean setOrder(Order newOrder)
    {
        if(newOrder != null)
        {
            arrOrders.add(newOrder);
            return true;
        }
        return false;
    }
    
    public Boolean setOrderHistory(OrderHistory newOrderHistory)
    {
        if(newOrderHistory != null)
        {
            arrOrderHistory.add(newOrderHistory);
            return true;
        }
        return false;
    }
    
    public ArrayList<OrderHistory> getOrderHistory()
    {
        ArrayList arrOrderHistoryCopy = new ArrayList<>();
        for (int i=0; i<arrOrderHistory.size(); i++)
        {
           OrderHistory Temp = (OrderHistory)arrOrderHistory.get(i); 
           arrOrderHistoryCopy.add(Temp);
        }
        return arrOrderHistoryCopy;
    }
}
