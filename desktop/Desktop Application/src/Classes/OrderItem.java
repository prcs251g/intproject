package Classes;

/**
 * OrderItem - Class to instantiate Order Item objects 
 * which are components of an order.
 * @author Luke Chilcott / Brian Young
 */
public class OrderItem 
{
    private String orderItemID;
    private String pizzaID;
    private String drinkID;
    private String sideID;
    
    public OrderItem(String orderItemID, String pizzaID, String drinkID, String sideID)
    {
        this.orderItemID = orderItemID;
        this.drinkID = drinkID;
        this.pizzaID = pizzaID;
        this.sideID = sideID;
    }

    public String getOrderItemID() 
    {
        return orderItemID;
    }

    public void setOrderItemID(String orderItemID) 
    {
        this.orderItemID = orderItemID;
    }

    public String getPizzaID() 
    {
        return pizzaID;
    }

    public void setPizzaID(String pizzaID) 
    {
        this.pizzaID = pizzaID;
    }

    public String getDrinkID() 
    {
        return drinkID;
    }

    public void setDrinkID(String drinkID) 
    {
        this.drinkID = drinkID;
    }

    public String getSideID() 
    {
        return sideID;
    }

    public void setSideID(String sideID) 
    {
        this.sideID = sideID;
    }
    
}
