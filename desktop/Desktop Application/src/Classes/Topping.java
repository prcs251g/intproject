package Classes;

/**
 * Topping - Class to instantiate Topping objects. 
 * @author Luke Chilcott / Brian Young
 */
public class Topping {
    private String calories;
    private String toppingID;
    private String toppingName;
    private String toppingPrice;
    
    public Topping(String toppingID, String toppingName, String toppingPrice,
            String calories){
        this.calories = calories;
        this.toppingID = toppingID;
        this.toppingName = toppingName;
        this.toppingPrice = toppingPrice;
}

    public String getCalories() {
        return calories;
    }

    public String getToppingID() {
        return toppingID;
    }

    public String getToppingName() {
        return toppingName;
    }

    public String getToppingPrice() {
        return toppingPrice;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public void setToppingID(String toppingID) {
        this.toppingID = toppingID;
    }

    public void setToppingName(String toppingName) {
        this.toppingName = toppingName;
    }

    public void setToppingPrice(String toppingPrice) {
        this.toppingPrice = toppingPrice;
    }
    
}
