package Classes;

/**
 * OrderHistory - Class to instantiate Order History objects.
 * @author Luke Chilcott / Brian Young
 */
public class OrderHistory
{
    private String orderHistoryId;
    private String price;
    private String time;
    
    public OrderHistory(String orderHistoryId, String price, String time)
    {
        this.orderHistoryId = orderHistoryId;
        this.price = price;
        this.time = time;
    }

    public String getOrderHistoryId() {
        return orderHistoryId;
    }

    public void setOrderHistoryId(String orderHistoryId) {
        this.orderHistoryId = orderHistoryId;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDate() {
        return time;
    }

    public void setDate(String time) {
        this.time = time;
    }
    
    
}
