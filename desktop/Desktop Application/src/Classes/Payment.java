package Classes;

/**
 * Payment - Class to instantiate payment method objects, Allows future program 
 * expansion in adding and editing different payment objects.
 * @author Luke Chilcott / Brian Young
 */
public class Payment {
    private String payment_id;
    private String payment_name;

    public Payment(String payment_id, String payment_name) {
        this.payment_id = payment_id;
        this.payment_name = payment_name;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }
    
}
