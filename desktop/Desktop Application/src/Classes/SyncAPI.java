package Classes;

import java.util.ArrayList;

/**
 * SyncAPI - Synchronises data from API as object into ImportAPI arrays.
 * @author Luke Chilcott / Brian Young
 */
public class SyncAPI {
  
    RESTfulMethods rest = new RESTfulMethods();
    ImportAPI data = ImportAPI.getInstance();
    JSONParser jp;
            
    public SyncAPI()
    {data.ClearCurrent();}
           
    public void BaseSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/bases");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList baseIDArray = jp.GetValueArray("BASE_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList baseArray = new ArrayList();
            baseArray.add(baseIDArray.get(i));
        for (int j=1; j<=4; j++){
            baseArray.add(jp.GetValue("BASE_ID", (baseIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Bases temp = new Bases(baseArray.get(0).toString(), baseArray.get(1).toString(), baseArray.get(2).toString(), baseArray.get(3).toString());
        data.setBases(temp);
    }
        jp.Clear();
    }
    
    public void CitySync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/cities");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList cityIDArray = jp.GetValueArray("CITY_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList cityArray = new ArrayList();
            cityArray.add(cityIDArray.get(i));
            for (int j=1; j<=2; j++){
                cityArray.add(jp.GetValue("CITY_ID", (cityIDArray.get(i)).toString(), jp.GetKeyName(j)));
            }
            City temp = new City(cityArray.get(0).toString(), cityArray.get(1).toString());
            data.setCity(temp);
        }
        jp.Clear();
    }
    
    public void CrustSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/crusts");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList crustIDArray = jp.GetValueArray("CRUST_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList crustArray = new ArrayList();
            crustArray.add(crustIDArray.get(i));
        for (int j=1; j<=4; j++){
            crustArray.add(jp.GetValue("CRUST_ID", (crustIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Crust temp = new Crust(crustArray.get(0).toString(), crustArray.get(1).toString(), crustArray.get(2).toString(), crustArray.get(3).toString());
        data.setCrust(temp);
    }
        jp.Clear();
    }
    
    public void CustSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/customers");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList custIDArray = jp.GetValueArray("CUSTOMER_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList custArray = new ArrayList();
            custArray.add(custIDArray.get(i));
        for (int j=1; j<=12; j++){
            custArray.add(jp.GetValue("CUSTOMER_ID", (custIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Customers temp = new Customers(custArray.get(0).toString(), custArray.get(1).toString(), custArray.get(2).toString(), custArray.get(3).toString(),
                custArray.get(4).toString(), custArray.get(5).toString(), custArray.get(6).toString(), custArray.get(7).toString(), custArray.get(8).toString(),
                custArray.get(9).toString(), custArray.get(10).toString(), custArray.get(11).toString());
        data.setCustomers(temp);
    }
        jp.Clear();
    }
    
    public void DrinkSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/drinks");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList drinkIDArray = jp.GetValueArray("DRINK_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList drinkArray = new ArrayList();
            drinkArray.add(drinkIDArray.get(i));
        for (int j=1; j<=5; j++){
            drinkArray.add(jp.GetValue("DRINK_ID", (drinkIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Drink temp = new Drink(drinkArray.get(0).toString(),drinkArray.get(1).toString(), drinkArray.get(2).toString(), 
                drinkArray.get(3).toString(), drinkArray.get(4).toString());
        data.setDrink(temp);
    }
        jp.Clear();
    }
        
    public void EmployeeSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/employees");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList employeeIDArray = jp.GetValueArray("EMPLOYEE_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList employeeArray = new ArrayList();
            employeeArray.add(employeeIDArray.get(i));
        for (int j=1; j<=5; j++){
            employeeArray.add(jp.GetValue("EMPLOYEE_ID", (employeeIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Employee temp = new Employee(employeeArray.get(0).toString(),employeeArray.get(1).toString(), employeeArray.get(2).toString(), 
                employeeArray.get(3).toString(), employeeArray.get(4).toString(), employeeArray.get(5).toString());
        data.setEmployee(temp);
    }
        jp.Clear();
    }
    
    public void SizeSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/itemsizes");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList sizeIDArray = jp.GetValueArray("SIZE_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList sizeArray = new ArrayList();
            sizeArray.add(sizeIDArray.get(i));
        for (int j=1; j<=3; j++){
            sizeArray.add(jp.GetValue("SIZE_ID", (sizeIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        ItemSize temp = new ItemSize(sizeArray.get(0).toString(),sizeArray.get(1).toString(),sizeArray.get(2).toString());
        data.setItemSize(temp);
    }
//    ArrayList Temp = data.getItemSize();
//        ItemSize G = (ItemSize)Temp.get(1);
//    System.out.println(Temp);
    jp.Clear();
    }
    
    public void JobSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/employeejobs");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList jobIDArray = jp.GetValueArray("JOB_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList jobArray = new ArrayList();
            jobArray.add(jobIDArray.get(i));
        for (int j=1; j<=2; j++){
            jobArray.add(jp.GetValue("JOB_ID", (jobIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Job temp = new Job(jobArray.get(0).toString(),jobArray.get(1).toString());
        data.setJob(temp);
    }   
    jp.Clear();
    }
       
    public void PaymentSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/payments");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList paymentIDArray = jp.GetValueArray("PAYMENT_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList paymentArray = new ArrayList();
            paymentArray.add(paymentIDArray.get(i));
        for (int j=1; j<=2; j++){
            paymentArray.add(jp.GetValue("PAYMENT_ID", (paymentIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Payment temp = new Payment(paymentArray.get(0).toString(),paymentArray.get(1).toString());
        data.setPayment(temp);
    }   
    jp.Clear();
    }
    
    public void PizzaSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/pizzas");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList pizzaIDArray = jp.GetValueArray("PIZZA_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList pizzaArray = new ArrayList();
            pizzaArray.add(pizzaIDArray.get(i));
        for (int j=1; j<=12; j++){
            pizzaArray.add(jp.GetValue("PIZZA_ID", (pizzaIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Pizza temp = new Pizza(pizzaArray.get(0).toString(), pizzaArray.get(1).toString(), pizzaArray.get(2).toString(), pizzaArray.get(3).toString(),
                pizzaArray.get(4).toString(), pizzaArray.get(5).toString(), pizzaArray.get(6).toString(), pizzaArray.get(7).toString(),
                pizzaArray.get(8).toString(), pizzaArray.get(9).toString(), pizzaArray.get(10).toString(), pizzaArray.get(11).toString());
        data.setPizza(temp);
    }   
    jp.Clear();
    }
    
    public void SideSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/sides");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList sideIDArray = jp.GetValueArray("SIDE_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList sideArray = new ArrayList();
            sideArray.add(sideIDArray.get(i));
        for (int j=1; j<=5; j++){
            sideArray.add(jp.GetValue("SIDE_ID", (sideIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Side temp = new Side(sideArray.get(0).toString(), sideArray.get(1).toString(), sideArray.get(2).toString(), 
                sideArray.get(3).toString(), sideArray.get(4).toString());
        data.setSide(temp);
    }   
    jp.Clear();
    }
    
        public void ToppingSync(){
        rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/toppings");
        jp = new JSONParser(rest.GET());
        jp.Parse();
        ArrayList toppingIDArray = jp.GetValueArray("TOPPING_ID");
        for (int i=0; i<jp.GetEntries();i++){
            ArrayList toppingArray = new ArrayList();
            toppingArray.add(toppingIDArray.get(i));
        for (int j=1; j<=4; j++){
            toppingArray.add(jp.GetValue("TOPPING_ID", (toppingIDArray.get(i)).toString(), jp.GetKeyName(j)));
        }
        Topping temp = new Topping(toppingArray.get(0).toString(), toppingArray.get(1).toString(), toppingArray.get(2).toString(), 
                toppingArray.get(3).toString());
        data.setTopping(temp);
    }   
    jp.Clear();
    }
        public void OrderItemSync()
        {
            rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orderitems");
            jp = new JSONParser(rest.GET());
            jp.Parse();
            ArrayList orderItemIDArray = jp.GetValueArray("ORDER_ITEM_ID");
            for (int i=0; i<jp.GetEntries();i++)
            {
                ArrayList orderItemArray = new ArrayList();
                orderItemArray.add(orderItemIDArray.get(i));
                for (int j=1; j<=4; j++)
                {
                    orderItemArray.add(jp.GetValue("ORDER_ITEM_ID", (orderItemIDArray.get(i)).toString(), jp.GetKeyName(j)));
                }
                OrderItem temp = new OrderItem(orderItemArray.get(0).toString(), orderItemArray.get(1).toString(), 
                            orderItemArray.get(2).toString(), orderItemArray.get(3).toString());
                data.setOrderItem(temp);
            }   
            jp.Clear();
        }
        
        public void OrderSync()
        {
            rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orders");
            jp = new JSONParser(rest.GET());
            jp.Parse();
            ArrayList orderIDArray = jp.GetValueArray("ORDER_ID");
            for (int i=0; i<jp.GetEntries();i++)
            {
                ArrayList orderArray = new ArrayList();
                orderArray.add(orderIDArray.get(i));
                for (int j=1; j<=17; j++)
                {
                    orderArray.add(jp.GetValue("ORDER_ID", (orderIDArray.get(i)).toString(), jp.GetKeyName(j)));
                }
                Order temp = new Order(orderArray.get(0).toString(), orderArray.get(1).toString(), orderArray.get(2).toString(),
                        orderArray.get(3).toString(),orderArray.get(4).toString(),orderArray.get(5).toString(),orderArray.get(6).toString(),
                        orderArray.get(7).toString(),orderArray.get(8).toString(),orderArray.get(9).toString(),orderArray.get(10).toString(),
                        orderArray.get(11).toString(),orderArray.get(12).toString(),orderArray.get(13).toString(),orderArray.get(14).toString(), 
                        orderArray.get(15).toString(),orderArray.get(16).toString());
                data.setOrder(temp);
            }   
            jp.Clear();
        }
       
        public void OrderHistorySync()
        {
            rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orderhistories");
            jp = new JSONParser(rest.GET());
            jp.Parse();
            ArrayList orderHistoryIDArray = jp.GetValueArray("ORDER_HISTORY_ID");
            for (int i=0; i<jp.GetEntries();i++)
            {
                ArrayList orderHistoryArray = new ArrayList();
                orderHistoryArray.add(orderHistoryIDArray.get(i));
                for (int j=1; j<=3; j++)
                {
                    orderHistoryArray.add(jp.GetValue("ORDER_HISTORY_ID", (orderHistoryIDArray.get(i)).toString(), jp.GetKeyName(j)));
                }
                OrderHistory temp = new OrderHistory(orderHistoryArray.get(0).toString(), orderHistoryArray.get(1).toString(),orderHistoryArray.get(2).toString() );
                data.setOrderHistory(temp);
            }   
            jp.Clear();
        }
}
