package Classes;

/**
 * Crust - Class to instantiate Pizza Crust objects.
 * @author Luke Chilcott / Brian Young
 */
public class Crust {
    private String calories;
    private String crust_ID;
    private String crust_Name;
    private String crust_Price;

    Crust(String crust_ID, String crust_Name, String crust_Price, String calories)
    {
        this.calories = calories;
        this.crust_ID = crust_ID;
        this.crust_Name = crust_Name;
        this.crust_Price = crust_Price;
    }

    public String getCalories() {
        return calories;
    }

    public String getCrust_ID() {
        return crust_ID;
    }

    public String getCrust_Name() {
        return crust_Name;
    }

    public String getCrust_Price() {
        return crust_Price;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public void setCrust_ID(String crust_ID) {
        this.crust_ID = crust_ID;
    }

    public void setCrust_Name(String crust_Name) {
        this.crust_Name = crust_Name;
    }

    public void setCrust_Price(String crust_Price) {
        this.crust_Price = crust_Price;
    }
    
    
}
