<%-- 
    Document   : OrderConfirmation
    Created on : 29-Mar-2017, 13:41:39
    Author     : aburch
--%>
<%@page import="java.time.format.DateTimeFormatter"%>
<%@page import="java.time.LocalDateTime"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Classes.JSONString"%>
<%@page import="Classes.JSONParser"%>
<%@page import="Classes.RESTfulMethods"%>
<%@page import="classes.OrderItem"%>
<%@page import="classes.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- code below taken from relevant java classes and API -->
        <%
            ArrayList arrOrderItems = new ArrayList();
            Session sess = Session.getInstance();
            RESTfulMethods rest = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orders");
            JSONString js = new JSONString();
            js.AddElement("margin", "m");
            if(sess.logged == 0)
            {
                String redirectURL = "Register.jsp";
                response.sendRedirect(redirectURL);
            }
            js.AddElement("CUSTOMER_ID", sess.id);
            Boolean d = true;
            int k = 0;
            while(d)
            {
                try
                {
                    rest.GET("" + k);
                    k++;
                }catch(RuntimeException e)
                {
                    d = false;
                    System.out.println(k);
                }
            }
            js.AddElement("ORDER_ID", k);
            String payment = request.getParameter("payment");
            if(payment.equals("cash"))
            {
                js.AddElement("PAYMENT_ID", 1);
            }
            if(payment.equals("card"))
            {
                js.AddElement("PAYMENT_ID", 2);
            }
            RESTfulMethods restItems = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/orderitems");
            for(int i = 0; i < sess.orderIDs.size(); i++)
            {
                JSONString item = new JSONString();
                item.AddElement("margin", "m");
                Boolean f = true;
                int j = 0;
                while(f)
                {
                    try
                    {
                        restItems.GET("" + j);
                        j++;
                    }catch(RuntimeException e)
                    {
                        f = false;
                        System.out.println(j);
                    }
                }
                item.AddElement("ORDER_ITEM_ID",j);
                OrderItem temp = (OrderItem)sess.orderItems.get(i);
                if(temp.getItemType().equals("PIZZA"))
                {
                    String id = sess.orderIDs.get(i).toString();
                    item.AddElement("PIZZA_ID", Double.parseDouble(id));
                }else
                if(temp.getItemType().equals("SIDE"))
                {
                    String id = sess.orderIDs.get(i).toString();
                    item.AddElement("SIDE_ID", Double.parseDouble(id));
                }else
                if(temp.getItemType().equals("DRINK"))
                {
                    String id = sess.orderIDs.get(i).toString();
                    item.AddElement("DRINK_ID", Double.parseDouble(id));
                }
                restItems.POST(item.GetJSON());
                arrOrderItems.add(j);
            }
            System.out.println(arrOrderItems);
            for(int i = 0; i < arrOrderItems.size(); i++)
            {
                if(i == 0)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_ONE", Double.parseDouble(id));
                }else
                if(i == 1)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_TWO", Double.parseDouble(id));
                }else
                if(i == 2)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_THREE", Double.parseDouble(id));
                }else
                if(i == 3)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_FOUR", Double.parseDouble(id));
                }else
                if(i == 4)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_FIVE", Double.parseDouble(id));
                }else
                if(i == 5)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_SIX", Double.parseDouble(id));
                }else
                if(i == 6)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_SEVEN", Double.parseDouble(id));
                }else
                if(i == 7)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_EIGHT", Double.parseDouble(id));
                }else
                if(i == 8)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_NINE", Double.parseDouble(id));
                }else
                if(i == 9)
                {
                    String id = arrOrderItems.get(i).toString();
                    js.AddElement("ORDER_ITEM_TEN", Double.parseDouble(id));
                }
            }
            js.AddElement("ORDER_PRICE", sess.totalPrice);
            js.AddElement("STATUS", "CHEF");
            LocalDateTime now = LocalDateTime.now();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String formatDateTime = now.format(formatter);
            js.AddElement("ELAPSED_TIME", formatDateTime);
            System.out.println(js.GetJSON());
            rest.POST(js.GetJSON());
        %>
        <title>Order Confirmation</title>
        <style type="text/css">
            div.main {width:200px; height:200px;}
            div.h {font-family:Papyrus; font-size:36px; font-weight:normal}
            div.i {font-family:Papyrus; font-size:16px; font-weight:normal}
            div.c {font-family:Papyrus; font-size:42px; font-weight:normal}
            div.r {font-family:arial; font-size:20px; font-weight:normal}
            div.divmenu {position:absolute; TOP:50px; LEFT:25%; WIDTH:300px; HEIGHT:200px;}
            div.divlogo {position:absolute; TOP:50px; LEFT:47%; WIDTH:200px; HEIGHT:200px; border-style:solid; border-width:5px; border-color:black}
            div.divbasket {position:absolute; TOP:50px; LEFT:70%; WIDTH:300px; HEIGHT:200px;}
            div.divlogon {position:absolute; TOP:10px; LEFT:90%; WIDTH:100px; HEIGHT:70px;}
            div.divconfirmed {position:absolute; TOP:300px; LEFT:30%; WIDTH:1100px; HEIGHT:70px;}
            div.divordertext {position:absolute; TOP:420px; LEFT:30%; WIDTH:200px; HEIGHT:170px;}
            div.divorder {position:absolute; TOP:475px; LEFT:30%; HEIGHT:475px; WIDTH:1000px; border-style:solid; border-width:2px; border-color:black}
            div.divitemorderback {position:absolute; TOP:475px; LEFT:30%; HEIGHT:475px; WIDTH:1000px; z-index: -1;}
            div.divplaceorder {position:absolute; TOP:1000px; LEFT:30%; HEIGHT:60px; WIDTH:110px; border-style:solid; border-width:2px; border-color:black}
            #confirmbutton {LEFT: 0px; TOP: 0px; width: 110px; height: 60px; background-color: #4CAF50; color: black; font-size:18px; font-weight:250}
        </style>
    <!-- Overall layout borrows heavily from the checkout page but without payment methods -->
    </head>
    <body background = images/backingmain1.jpg style="background-size: 4000px 3000px; background-repeat: no-repeat; max-width: 2000px;overflow:scroll;">
        <div class="main">
    <div class="divmenu"><div class="h"><a href="index.jsp">MENU</a></div></div>
    <div class="divlogo"><img src="images/Pizza Masterpiece Logo.png" height="200" width="200"></div>
    <!-- Total cost of what was selected, dynamic to change with the users choices -->
    <div class="divbasket"><div class="h"><a href="checkout.jsp">BASKET<%Session order = Session.getInstance(); out.println("(" + order.orderQuan + ")");%></a></div><% out.println("Total order price: £" + order.totalPrice);%></div>
    <div class="divlogon"><div class="i"><a href="Register.jsp"><%if(order.logged == 0){out.println("Login/Register");}else if(order.logged == 1 ){out.println("Welcome, " + order.username);}%></a></div></div>
    <div class="divconfirmed"><div class="c">THANK YOU FOR PLACING YOUR ORDER!</div></div> 
    <div class="divordertext"><div class="h">Your Order:</div></div>
    <div class="divorder"><div class="r"><%
            for (OrderItem object: order.orderItems) 
            {
                out.println(object.getItemSize() + " " + object.getItemName() + "     " + " - " + "£" + object.getItemPrice() + "<br>");
            }
    %></div></div>
    <div class="divitemorderback"><img src="images/backing3.jpg" height="477" width="1002"></div>
    <div class="divplaceorder"><form action="index.jsp" method="post"><button id="confirmbutton" type="orderbutton" value="Ordered" name="PlaceOrder" onclick="alert('Order placed!')">Confirm</button></div>
        </div>
        </body>
</html>
