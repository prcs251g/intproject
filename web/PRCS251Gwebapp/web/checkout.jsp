<%-- 
    Document   : checkout
    Created on : 10-Mar-2017, 12:39:30
    Author     : aburch
--%>
<%@page import="classes.OrderItem"%>
<%@page import="classes.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Checkout page</title>
        <link rel="stylesheet" type="text/css" href="css/checkout.css">
    </head>
    <!-- Top of the page is uniform across all the pages for the sake of the style -->
    <body background = images/backingmain1.jpg>
    <div class="main">
    <div class="divmenu"><div class="h"><a href="index.jsp">MENU</a></div></div>
    <div class="divlogo"><img src="images/Pizza Masterpiece Logo.png" height="200" width="200"></div>
    <!-- Total cost of what was selected, dynamic to change with the users choices -->
    <div class="divbasket"><div class="h"><a href="checkout.jsp">BASKET<%Session order = Session.getInstance(); out.println("(" + order.orderQuan + ")");%></a></div><% out.println("Total order price: £" + order.totalPrice);%></div>
    <div class="divbasketpicture"><img src="images/shopcart.jpg" height="100" width="100"></div>
    <div class="divlogon"><div class="i"><a href="Register.jsp"><%if(order.logged == 0){out.println("Login/Register");}else if(order.logged == 1 ){out.println("Welcome, " + order.username);}%></a></div></div>
    <div class="divtrolley"><img src="images/shopcart.jpg" height="220" width="220"></div>
    <div class="divordertext"><div class="l">Your Order:</div></div>
    <div class="divorder"><div class="r"><!-- code below taken from relevant java classes and API --><%
            int i = 0;
            for (OrderItem object: order.orderItems) 
            {
                out.print("<form action='remove.jsp' method='post'>");
                out.println(object.getItemSize() + " " + object.getItemName() + "     " + " - " + "£" + object.getItemPrice());
                out.print("<input type='image' src='images/removal.png' id='removebutton' value='submit' name='removebutton'>");
                out.println("<input type='hidden' value='"+object.getItemPrice()+"' name='itemprice'>");
                out.println("<input type='hidden' value='"+i+"' name='orderid'>");
                out.println("</button>");
                out.println("</form>");
                out.println("<br>");
                i++;
            }
    %></div></div>
    <!-- The orders will be in a self contained section with another area within it for the total cost of the order -->
    <div class="divitemorderback"><img src="images/backing3.jpg" height="477" width="1002"></div>
    <div class="divtotal"><% out.println("<p style='font-size:30px'><b>Total: £" + order.totalPrice + "</b></p>");%></div>
    <div class="divaddbutton"><form action="index.jsp" method="post"><button id="addbutton" type="addbutton">Add More</button></form></div>
    <div class="divcancelbutton"><form action="index.jsp" method="post"><button id="cancelbutton" type="submit" value="Ordered" name="PlaceOrder">Cancel Order</button></form></div>
    <div class="divrequired"><div class="i"><font color="red">Required Field</font></div></div>
    <div class="divpayment"><div class="l"><br>&nbsp;or</div></div>
    <div class="divitemcashback"><img src="images/backing3.jpg" height="162" width="1102"></div>
    <form action="OrderConfirmation.jsp" method="post">
        <!-- Area set aside for the payment method you would like to use -->
       <div class="divcash"><div class="l"><br>&nbsp;Cash On Delivery<input name="payment" type="checkbox" id="cboxcash" value="cash"></div></div>
       <div class="divcard"><div class="l"><br>&nbsp;Debit/Credit Card<input name="payment" type="checkbox" id="cboxcard" value="card"></div></div>
       <!-- button to place order will be clearly apart so it cant be pressed unintentionally -->
       <div class="divplaceorder"><div class="l"><button id="addbutton" type="submit">Place Order!</button></div></div></form>
        </div>
    </body>
</html>
