<%-- 
    Document   : Register
    Created on : 10-Mar-2017, 14:53:21
    Author     : aburch
--%>

<%@page import="classes.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            Session order = Session.getInstance();
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register</title>
        <link rel="stylesheet" type="text/css" href="css/register.css">
    </head>
    <!-- Top of the page is uniform across all the pages for the sake of the style -->
    <body background = images/backingmain1.jpg>
        <div class="divmenu"><div class="h"><a href="index.jsp">MENU</a></div></div>
        <div class="divlogo"><img src="images/Pizza Masterpiece Logo.png" height="200" width="200"></div>
        <!-- Total cost of what was selected, dynamic to change with the users choices -->
        <div class="divbasket"><div class="h"><a href="checkout.jsp">BASKET<%out.println("(" + order.orderQuan + ")");%></a></div><%out.println("Total order price: £" + order.totalPrice);%></div>
        <div class="divbasketpicture"><img src="images/shopcart.jpg" height="100" width="100"></div>
        <div class="divlogon"><div class="l"><a href="index.jsp">Return to main</a></div></div>
        <!-- Title classes will separate the sections for logging in and registering -->
        <div class="divtitle"><div class="t">Please input your details below:</div></div>
        <div class="divrequired"><font color="red">* Required Field</font> <% if(order.missingfields){out.println("<font style=\"color:red;\"><br>Missing required fields.</font>"); order.missingfields = false;}  %> </div>
        <div class="divsecondt"><div class="t">Please login below:</div></div>
        <form action="login.jsp" method="post">
        <!-- Login Fields in the same area of the page -->
            <div class="divfieldlogin1">Username:<br><input type="text" id="firstinput" name="username2" style="width: 300px"></div>
            <div class="divfieldlogin2">Password:<br><input type="password" id="firstinput" name="password2" style="width: 300px"></div>
            <div class="divfield14"><button id="loginbutton" type="submit" value="logged" name="login">Login</button></div>
        </form>
        <form action="registering.jsp" method="post">
        <!-- Registering fields laid out with different sizes based upon the needs of the input -->
            <div class="divfield1">First Name:<font color="red">*</font><br><input type="text" id="firstinput" name="forename" style="width: 300px"></div>
            <div class="divfield2">Last Name:<font color="red">*</font><br><input type="text" id="lastinput" name="surname"style="width: 300px"></div>
            <div class="divfield3">Address 1:<font color="red">*</font><br><input type="text" id="addinput" name="address1" style="width: 400px"></div>
            <div class="divfield4">Address 2:<br><input type="text" id="addinput2" name="address2" style="width: 400px"></div>
            <div class="divfield5">City:<font color="red">*</font><% if(order.invalidcity){out.println("<font style=\"color:red;\"> Unrecognised city.</font>"); order.invalidcity = false;}  %><br><input type="text" id="addinput2" name="city" style="width: 300px"></div>
            <div class="divfield6">Postcode:<font color="red">*</font><br><input type="text" id="postinput" name="postcode" style="width: 100px"></div>
            <div class="divfield7">E-mail Address:<% if(order.invalidemail){out.println("<font style=\"color:red;\"> E-mail must be a valid address.</font>"); order.invalidemail = false;}  %><br><input type="text" id="mailinput" name="email" style="width: 300px"></div>
            <div class="divfield8">Mobile Phone Number:<font color="red">*</font><br><input type="number" min="0" id="mobinput" name="mobilenumber" style="width: 200px"></div>
            <div class="divfield9">Username:<font color="red">*</font><br><input type="text" id="userinput" name="username" style="width: 300px"></div>
            <div class="divfield10">Password:<font color="red">*</font><br><input type="password" id="passinput" name="password" style="width: 300px"></div>
            <div class="divfield11">Allergies:</div>      
            <div class="divfield11one">Gluten<input type="checkbox" id="cboxgluten" value="checkboxgluten"><br></div>                                      
            <div class="divfield11two">Nuts<input type="checkbox" id="cboxnuts" value="checkboxnuts"><br></div>
            <div class="divfield11three">Maize<input type="checkbox" id="cboxmaize" value="checkboxmaize"><br></div>
            <div class="divfield11four">Lactose<input type="checkbox" id="cboxlactose" value="checkboxlactose"><br></div>                       
            <div class="divfield13"><button id="registerbutton" type="submit" value="logged" name="register">Register Account</button></div>
        </form>
    </body>
</html>
