function hideButton()
{
    document.getElementById('completebutton').style.visibility = 'hidden';
}
var toppingTotal = 0;
var currentToppings = ["","","","","",""];
function changePriceTopping(topping)
{
    if(toppingTotal >= 5)
    {
        alert("You have reached the maximum number of toppings at 5. Please deselect 1.");
        hideButton();
    }
    var test = 0;
    for(var i = 0; i < 5; i++)
    {
        if(currentToppings[i] === topping && test === 0)
        {
            toppingTotal = toppingTotal - 1;
            currentToppings[i] = "";
            calcTotal();
            test = 1;
        }else
        {
            if(currentToppings[i] === "" && test === 0)
            {
                toppingTotal = toppingTotal + 1;
                currentToppings[i] = topping;
                calcTotal();
                test = 1;
            }
        }
    }
    if(toppingTotal <= 5)
    {
        complete();
    }
}
function changePriceTopping2(topping)
{
    if(toppingTotal >= 5)
    {
        alert("You have reached the maximum number of toppings at 5. Please deselect 1.");
        hideButton();
    }
    var test = 0;
    for(var i = 0; i < 5; i++)
    {
        if(currentToppings[i] === topping && test === 0)
        {
            toppingTotal = toppingTotal - 2;
            currentToppings[i] = "";
            calcTotal();
            test = 1;
        }else
        {
            if(currentToppings[i] === "" && test === 0)
            {
                toppingTotal = toppingTotal + 2;
                currentToppings[i] = topping;
                calcTotal();
                test = 1;
            }
        }
    }
    if(toppingTotal <= 5)
    {
        complete();
    }
}
var size = 0;
function advanceProgressBarSize()
{
    var progress = document.getElementById("myProgress");
    if(size === 0)
    {
        if(progress.value < 33.33)
        {
            progress.value = 33.33;
        }else if(progress.value < 66.66)
        {
            progress.value = 66.66;
        }else if(progress.value < 99.99)
        {
            progress.value = 99.99;
            complete();
        }
        size = 1;
    }
}
var base = 0;
function advanceProgressBarBase()
{
    var progress = document.getElementById("myProgress");
    if(base === 0)
    {
        if(progress.value < 33.33)
        {
            progress.value = 33.33;
        }else if(progress.value < 66.66)
        {
            progress.value = 66.66;
        }else if(progress.value < 99.99)
        {
            progress.value = 99.99;
            complete();
        }
    base = 1;
    }
}
function complete()
{
    var button = document.getElementById("completebutton");
    button.style.visibility = 'visible';
}
var crust = 0;
function advanceProgressBarCrust()
{
    var progress = document.getElementById("myProgress");
    if(crust === 0)
    {
        if(progress.value < 33.33)
        {
            progress.value = 33.33;
        }else if(progress.value < 66.66)
        {
            progress.value = 66.66;
        }else if(progress.value < 99.99)
        {
            progress.value = 99.99;
            complete();
        }
    crust = 1;
    }
}
var sizeCost = 0;
var baseCost = 0;
var crustCost = 0;

function calcTotal()
{
    total = document.getElementById("test");
    var totalCost = 0;
    totalCost = sizeCost + baseCost + crustCost + toppingTotal;
    totalCost = roundToTwoDC(totalCost);
    total.value = "£" + totalCost;
}
function smallCost()
{
    var price = 0.00;
    price = price + 7.99;
    sizeCost = price;
    calcTotal();
    advanceProgressBarSize();
}

function regularCost()
{
    var price = 0.00;
    price = price + 9.99;
    sizeCost = price;
    calcTotal();
    advanceProgressBarSize();
}

function largeCost()
{
    var price = 0.00;
    price = price + 11.99;
    sizeCost = price;
    calcTotal();
    advanceProgressBarSize();
}

function zeroBaseCost()
{
    baseCost = 0.00;
    calcTotal();
    advanceProgressBarBase();
}

function specBaseCost()
{
    baseCost = 3.00;
    calcTotal();
    advanceProgressBarBase();
}

function zeroCrustCost()
{
    crustCost = 0.00;
    calcTotal();
    advanceProgressBarCrust();
}

function specCrustCost()
{
    crustCost = 3.00;
    calcTotal();
    advanceProgressBarCrust();
}
function roundToTwoDC(num) 
{    
    return +(Math.round(num + "e+2")  + "e-2");
}