<%-- 
    Document   : custom
    Created on : 10-Mar-2017, 14:46:55
    Author     : aburch
--%>
<%@page import="classes.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create your own</title>
        <link rel="stylesheet" type="text/css" href="css/custom.css">
        <script src="js/custom.js"></script>
    </head>
    <!-- Top of the page is uniform across all the pages for the sake of the style -->
    <body onload="hideButton();" background = images/backingmain1.jpg style="background-size: 2000px 2000px; background-repeat: no-repeat; max-width: 2000px;overflow:scroll;">
        <div class="main">
    <div class="divmenu"><div class="h"><a href="index.jsp">MENU</a></div></div>
    <div class="divlogo"><img src="images/Pizza Masterpiece Logo.png" height="200" width="200"></div>
    <!-- Total cost of what was selected, dynamic to change with the users choices -->
    <div class="divbasket"><div class="h"><a href="checkout.jsp">BASKET<%Session order = Session.getInstance(); out.println("(" + order.orderQuan + ")");%></a></div><% out.println("Total order price: £" + order.totalPrice);%></a></div></div>
    <div class="divbasketpicture"><img src="images/shopcart.jpg" height="100" width="100"></div>
    <div class="divlogon"><div class="i"><a href="Register.jsp"><%if(order.logged == 0){out.println("Login/Register");}else if(order.logged == 1 ){out.println("Welcome, " + order.username);}%></a></div></div>
    <!-- Title class followed by the modules for the lists a user can pick from -->
    <div class="divtitle"><div class="t">Build Your Own:</div></div>
    <!-- Progress bar for a visual representation of the progress the user has made to a full pizza -->
    <div class="divprogressbar"><progress value="0" id="myProgress" min="0" max="100"></progress></div>
    <!-- An individual module that contains all the options for one part of a pizza that a user can choose from -->
    <div class="divsizetitle"><div class="s">Size:</div></div>
    <div class="divsizeback"><img src="images/backing3.jpg" height="302" width="302"></div>
    <form action='MakeCustomPizza.jsp' method='get'>
    <div class="divsize"><div class="i">
                                <input type="radio" name="size" value="1" onclick="smallCost();"> Small(£7.99)<br><br><br>
                                <input type="radio" name="size" value="2" onclick="regularCost();"> Regular(£9.99)<br><br><br>
                                <input type="radio" name="size" value="3" onclick="largeCost();"> Large(£11.99)
                                </div></div>
                        </div>
    <!-- An individual module that contains all the options for one part of a pizza that a user can choose from -->
    <div class="divbasetitle"><div class="s">Base:</div></div>
    <div class="divbaseback"><img src="images/backing3.jpg" height="302" width="302"></div>
    <div class="divbase"><div class="i">
                                <input type="radio" name="base" value="1" onclick="zeroBaseCost();"> Regular<br><br><br>
                                <input type="radio" name="base" value="2" onclick="specBaseCost();"> Thin Crust(+£3)<br><br><br>
                                <input type="radio" name="base" value="3" onclick="specBaseCost();"> Deep Dish(+£3)<br><br><br>
                                <input type="radio" name="base" value="4" onclick="specBaseCost();"> Gluten Free(+£3)</div>
                         </div>
    <!-- An individual module that contains all the options for one part of a pizza that a user can choose from -->
    <div class="divcrusttitle"><div class="s">Crust:</div></div>
    <div class="divcrustback"><img src="images/backing3.jpg" height="302" width="302"></div>
    <div class="divcrust"><div class="i">
                                 <input type="radio" name="crust" value="1" onclick="zeroCrustCost();">Regular<br><br>
                                 <input type="radio" name="crust" value="2" onclick="specCrustCost();">Stuffed Crust w/Cheese(+£3)<br><br>
                                 <input type="radio" name="crust" value="3" onclick="specCrustCost();">Stuffed Crust w/Tomato Sauce(+£3)<br><br>
                                 <input type="radio" name="crust" value="4" onclick="specCrustCost();">Stuffed Crust w/Hot Dog(+£3)<br><br>
                                 <input type="radio" name="crust" value="5" onclick="specCrustCost();">Cheese+Garlic Bites(+£3)<br><br>
                                 <input type="radio" name="crust" value="6" onclick="specCrustCost();">Mini Burgers(+£3)</div>
                         </div>
    <div class="divcustom"><img src="images/custompizza.png" height="300" width="300"></div>
    <!-- An individual module that contains all the options for toppings, more substantial to house all the options available  -->
    <div class="divtoppingstitle"><div class="s">Toppings (£1 for single, £2 for double):</div></div>
    <div class="divtoppingsback"><img src="images/backing3.jpg" height="902" width="1002"></div>
    <div class="divtoppings"><div class="divtoppings1">Extra Tomatoes:&nbsp;<input onclick="changePriceTopping('Tomato');"name='topping1' type="checkbox" id="cboxtoms" value="1">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Tomato2');" type="checkbox" id="cboxtoms2" value="1,2"><br><img src="images/toppingtomatoes.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings2">Pepperoni:&nbsp;<input onclick="changePriceTopping('Pepperoni');" name='topping2' type="checkbox" id="cboxroni" value="2">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Pepperoni2');" type="checkbox" id="cboxroni2" value="2,2"><br><img src="images/toppingpepperoni.jpg" height="50" width="50"></div>
                             <div class="divtoppings3">Ham:&nbsp;<input onclick="changePriceTopping('Ham');" name='topping3' type="checkbox" id="cboxham" value="3">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Ham2');" type="checkbox" id="cboxham2" value="3,2"><br><img src="images/toppingham.jpg" height="50" width="50"></div>
                             <div class="divtoppings4">Pineapple:&nbsp;<input onclick="changePriceTopping('Pineapple');" name='topping4' type="checkbox" id="cboxpine" value="4">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Pineapple2');" type="checkbox" id="cboxpine2" value="4,2"><br><img src="images/toppingpineapple.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings5">Onions:&nbsp;<input onclick="changePriceTopping('Onion');" name='topping5' type="checkbox" id="cboxoni" value="5">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Onion2');" type="checkbox" id="cboxoni2" value="5,2"><br><img src="images/toppingonion.png" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings6">Jalepenos:&nbsp;<input onclick="changePriceTopping('Jalepenos');" name='topping6' type="checkbox" id="cboxjal" value="6">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Jalepenos2');" type="checkbox" id="cboxjal2" value="6,2"><br><img src="images/toppingjalapeno.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"><img src="images/hot.jpg" height="50" width="50"></div>
                             <div class="divtoppings7">Peppers:&nbsp;<input onclick="changePriceTopping('Peppers');" name='topping7' type="checkbox" id="cboxpepp" value="7">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Peppers2');" type="checkbox" id="cboxpepp2" value="7,2"><br><img src="images/toppingpepper.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings8">Ground Beef:&nbsp;<input onclick="changePriceTopping('Beef');" name='topping8' type="checkbox" id="cboxbeef" value="8">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Beef2');" type="checkbox" id="cboxbeef2" value="8,2"><br><img src="images/toppingbeef.jpg" height="50" width="50"></div>
                             <div class="divtoppings9">Pulled Pork:&nbsp;<input onclick="changePriceTopping('Pork');" name='topping9' type="checkbox" id="cboxpork" value="9">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Pork2');" type="checkbox" id="cboxpork2" value="9,2"><br><img src="images/toppingpulledpork.jpg" height="50" width="50"></div>
                             <div class="divtoppings10">Sweetcorn:&nbsp;<input onclick="changePriceTopping('Sweetcorn');" name='topping10' type="checkbox" id="cboxcorn" value="10">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Sweetcorn2');" type="checkbox" id="cboxcorn2" value="10,2"><br><img src="images/toppingsweetcorn.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings11">Chicken:&nbsp;<input onclick="changePriceTopping('Chicken');" name='topping11' type="checkbox" id="cboxchi" value="11">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Chicken2');" type="checkbox" id="cboxchi2" value="11,2"><br><img src="images/toppingchicken.jpg" height="50" width="50"></div>
                             <div class="divtoppings12">Mushrooms:&nbsp;<input onclick="changePriceTopping('Mushroom');" name='topping12' type="checkbox" id="cboxmush" value="12">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Mushroom2');" type="checkbox" id="cboxmush2" value="12,2"><br><img src="images/toppingmushroom.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings13">Bacon:&nbsp;<input onclick="changePriceTopping('Bacon');" name='topping13' type="checkbox" id="cboxbaco" value="13">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Bacon2');" type="checkbox" id="cboxbaco2" value="13,2"><br><img src="images/toppingbacon.jpg" height="50" width="50"></div>
                             <div class="divtoppings14">Sausage:&nbsp;<input onclick="changePriceTopping('Sausage');" name='topping14' type="checkbox" id="cboxsaus" value="14">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Sausage2');" type="checkbox" id="cboxsaus2" value="14,2"><br><img src="images/toppingsausages.jpg" height="50" width="50"></div>
                             <div class="divtoppings15">Black Olives:&nbsp;<input onclick="changePriceTopping('BOlives');" name='topping15' type="checkbox" id="cboxbol" value="15">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('BOlives2');" type="checkbox" id="cboxbol2" value="15,2"><br><img src="images/toppingblackolive.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings16">Green Olives:&nbsp;<input onclick="changePriceTopping('GOlives');" name='topping16' type="checkbox" id="cboxgol" value="16">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('GOlives2');" type="checkbox" id="cboxgol2" value="16,2"><br><img src="images/toppinggreenolive.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings17">Mozzarella:&nbsp;<input onclick="changePriceTopping('Mozzarella');" name='topping17' type="checkbox" id="cboxmoz" value="17">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Mozzarella2');" type="checkbox" id="cboxmoz2" value="17,2"><br><img src="images/toppingmozzerella.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings18">Tuna:&nbsp;<input onclick="changePriceTopping('Tuna');" name='topping18' type="checkbox" id="cboxtuna" value="18">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Tuna2');" type="checkbox" id="cboxtuna2" value="18,2"><br><img src="images/toppingtuna.jpg" height="50" width="50"></div>
                             <div class="divtoppings19">Anchovy:&nbsp;<input onclick="changePriceTopping('Anchovy');" name='topping19' type="checkbox" id="cboxanch" value="19">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Anchovy2');" type="checkbox" id="cboxanch2" value="19,2"><br><img src="images/toppinganchovy.jpg" height="50" width="50"></div>
                             <div class="divtoppings20">Pickles:&nbsp;<input onclick="changePriceTopping('Pickles');" name='topping20' type="checkbox" id="cboxpick" value="20">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Pickles2');" type="checkbox" id="cboxpick2" value="20,2"><br><img src="images/toppingpickle.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings21">Meatballs:&nbsp;<input onclick="changePriceTopping('Meatballs');" name='topping21' type="checkbox" id="cboxmeat" value="21">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Meatballs2');" type="checkbox" id="cboxmeat2" value="21,2"><br><img src="images/toppingmeatballs.jpg" height="50" width="50"></div>
                             <div class="divtoppings22">Rocket:&nbsp;<input onclick="changePriceTopping('Rocket');" name='topping22' type="checkbox" id="cboxrock" value="22">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Rocket2');" type="checkbox" id="cboxrock2" value="22,2"><br><img src="images/toppingrocket.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings23">Spinach:&nbsp;<input onclick="changePriceTopping('Spinach');" name='topping23' type="checkbox" id="cboxspin" value="23">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Spinach2');" type="checkbox" id="cboxspin2" value="23,2"><br><img src="images/toppingspinach.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings24">Broccoli:&nbsp;<input onclick="changePriceTopping('Broccoli');" name='topping24' type="checkbox" id="cboxbroc" value="24">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Broccoli2');" type="checkbox" id="cboxbroc2" value="24,2"><br><img src="images/toppingbroccoli.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings25">Roasted Garlic:&nbsp;<input onclick="changePriceTopping('Garlic');" name='topping25' type="checkbox" id="cboxgar" value="25">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Garlic2');" type="checkbox" id="cboxgar2" value="25,2"><br><img src="images/toppinggarlic.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings26">Zucchini:&nbsp;<input onclick="changePriceTopping('Zucchini');" name='topping26' type="checkbox" id="cboxzucc" value="26">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Zucchini2');" type="checkbox" id="cboxzucc2" value="26,2"><br><img src="images/toppingzucchini.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings27">Scotch Bonnets:&nbsp;<input onclick="changePriceTopping('Bonnets');" name='topping27' type="checkbox" id="cboxbonn" value="27">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Bonnets2');" type="checkbox" id="cboxbonn2" value="27,2"><br><img src="images/toppuin.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"><img src="images/hot.jpg" height="50" width="50"></div>
                             <div class="divtoppings28">Burger Cheese:&nbsp;<input onclick="changePriceTopping('BurgerCheese');" name='topping28' type="checkbox" id="cboxburg" value="28">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('BurgerCheese2');" type="checkbox" id="cboxburg2" value="28,2"><br><img src="images/toppingburgercheese.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings29">Salami:&nbsp;<input onclick="changePriceTopping('Salami');" name='topping29' type="checkbox" id="cboxsali" value="29">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Salami2');" type="checkbox" id="cboxsali2" value="29,2"><br><img src="images/toppingsalami.png" height="50" width="50"></div>
                             <div class="divtoppings30">Chorizo:&nbsp;<input onclick="changePriceTopping('Chorizo');" name='topping30' type="checkbox" id="cboxchor" value="30">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Chorizo2');" type="checkbox" id="cboxchor2" value="30,2"><br><img src="images/toppingchorizo.jpg" height="50" width="50"><img src="images/hot.jpg" height="50" width="50"></div>
                             <div class="divtoppings31">Parmesan Cheese:&nbsp;<input onclick="changePriceTopping('Parmesan');" name='topping31' type="checkbox" id="cboxparm" value="31">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Parmesan2');" type="checkbox" id="cboxparm2" value="31,2"><br><img src="images/toppingparmesan.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings32">Habaneros:&nbsp;<input onclick="changePriceTopping('Habeneros');" name='topping32' type="checkbox" id="cboxhab" value="32">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Habeneros2');" type="checkbox" id="cboxhab2" value="32,2"><br><img src="images/toppinghabeneros.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"><img src="images/hot.jpg" height="50" width="50"></div>
                             <div class="divtoppings33">Ghost peppers:&nbsp;<input onclick="changePriceTopping('GhostPeppers');" name='topping33' type="checkbox" id="cboxgho" value="33">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('GhostPeppers2');" type="checkbox" id="cboxgho2" value="33,2"><br><img src="images/toppingghostpepper.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"><img src="images/hot.jpg" height="50" width="50"></div>
                             <div class="divtoppings34">Nachos:&nbsp;<input onclick="changePriceTopping('Nachos');" name='topping34' type="checkbox" id="cboxnach" value="34">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Nachos2');" type="checkbox" id="cboxnach2" value="34,2"><br><img src="images/toppingnachos.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings35">Guacamole:&nbsp;<input onclick="changePriceTopping('Guacomole');" name='topping35' type="checkbox" id="cboxguac" value="35">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Guacomole2');" type="checkbox" id="cboxguac2" value="35,2"><br><img src="images/toppingguacamole.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
                             <div class="divtoppings36">Garlic Mayo:&nbsp;<input onclick="changePriceTopping('Mayo');" name='topping36' type="checkbox" id="cboxmayo" value="36">&nbsp;X2:&nbsp;<input onclick="changePriceTopping2('Mayo2');" type="checkbox" id="cboxmayo2" value="36,2"><br><img src="images/toppinggarlicmayo.jpg" height="50" width="50"><img src="images/vegetarian.png" height="50" width="50"></div>
    </div>
    <div class="divtotal"><div class="t"><input name="price" id="test" value="£0.00"></div></div>
    <div class="divcompletebutton"><button id="completebutton" type="submit">Add To Order</button></div>
    </form>
    <!-- Total cost of what was selected, dynamic to change with the users choices -->
     <div class="divtotaltitle"><div class="s">Total Cost:</div></div>
     <div class="divtotalback"><img src="images/backing3.jpg" height="102" width="402"></div>
     <div class="divclearbutton"><button id="clearbutton" onClick="history.go(0)" value="refresh">Clear Choices</button></div>
    </div>
    </body>
</html>
