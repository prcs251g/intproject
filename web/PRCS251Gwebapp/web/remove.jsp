<%-- 
    Document   : remove
    Created on : 26-Apr-2017, 11:33:39
    Author     : bvyoung
--%>

<%@page import="classes.Session"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <%
            int orderid = Integer.parseInt(request.getParameter("orderid"));
            System.out.println(request.getParameter("itemprice"));
            double price = Double.parseDouble(request.getParameter("itemprice"));
            System.out.println(price);
            System.out.println(orderid);
            Session sess = Session.getInstance();
            sess.orderItems.remove(orderid);
            sess.orderQuan--;
            sess.totalPrice = sess.totalPrice - price;
            sess.totalPrice = (double)(Math.round(sess.totalPrice*100))/100;
            sess.orderIDs.remove(orderid);
            String redirectURL = "checkout.jsp";
            response.sendRedirect(redirectURL);
            %>
    </body>
</html>
