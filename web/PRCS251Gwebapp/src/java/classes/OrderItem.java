/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

/**
 * OrderItem --- program to set/get the attributes of the various pizzas, sides and drinks, includes price calculation.
 * @author    Brian Young/Adam Burch
 */
public class OrderItem
{
    public String getItemName() 
    {
        return itemName;
    }

    public void setItemName(String itemName) 
    {
        this.itemName = itemName;
    }

    public String getItemSize() 
    {
        return itemSize;
    }

    public void setItemSize(String itemSize) 
    {
        this.itemSize = itemSize;
    }

    public double getItemPrice() 
    {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice)
    {
        this.itemPrice = itemPrice;
    }
    
    public String getItemType()
    {
        return this.itemType;
    }
    
    public void setItemType(String itemType)
    {
        this.itemType = itemType;
    }
    
    private String itemType;
    private String itemName;
    private String itemSize;
    private double itemPrice;
    
    public OrderItem(String itemName, String itemSize)
    {
        this.itemName = itemName;
        this.itemSize = itemSize;
        calculatePrice();
    }
    
    private void calculatePrice()
    {
        switch (itemName)
        {
            case "Margherita": itemPrice = 10.99;
                               itemType = "PIZZA";
            break;
            case "Pepperoni": itemPrice = 11.99;
                              itemType = "PIZZA";
            break;
            case "Meat Feast": itemPrice = 13.99;
                              itemType = "PIZZA";
            break;
            case "Spicy Hot": itemPrice = 12.99;
                             itemType = "PIZZA";
            break;
            case "BBQ Supreme": itemPrice = 13.99;
                               itemType = "PIZZA";
            break;
            case "Hawaiian": itemPrice = 10.99;
                             itemType = "PIZZA";
            break;
            case "Farmhouse": itemPrice = 11.99;
                              itemType = "PIZZA";
            break;
            case "Mexican Sizzler": itemPrice = 12.99;
                                   itemType = "PIZZA";
            break;
            case "Chicago": itemPrice = 13.99;
                                  itemType = "PIZZA";
            break;
            case "Milano": itemPrice = 14.99;
                           itemType = "PIZZA";
            break;
            case "Chicken Delight": itemPrice = 10.99;
                                   itemType = "PIZZA";
            break;
            case "The Works": itemPrice = 15.99;
                             itemType = "PIZZA";
            break;
            case "Garlic Bread": itemPrice = 4.99;
                                itemType = "SIDE";
            break;
            case "Chicken Wing": itemPrice = 3.99;
                          itemType = "SIDE";
            break;
            case "Chicken Strips": itemPrice = 3.99;
                                  itemType = "SIDE";
            break;
            case "Dough Balls": itemPrice = 3.99;
                               itemType = "SIDE";
            break;
            case "Coca Cola": itemPrice = 1.99;
                             itemType = "DRINK";
            break;
            case "Sprite": itemPrice = 1.99;
                           itemType = "DRINK";
            break;
            case "Dr Pepper": itemPrice = 1.99;
                             itemType = "DRINK";
            break;
            case "Tango": itemPrice = 1.99;
                          itemType = "DRINK";
            break;
            default:
            break;
        }
        switch(itemSize)
        {
            case "Regular": itemPrice = itemPrice + 2.00;
            break;
            case "Large": itemPrice = itemPrice + 5.00;
            break;
            case "6pcs": itemPrice = itemPrice + 1.00;
            break;
            case "8pcs": itemPrice = itemPrice + 2.00;
            break;
            case "1.25l Bottle": itemPrice = itemPrice + 1.00;
            break;
            case "2l Bottle": itemPrice = itemPrice + 2.00;
            break;
            default:
            break;
        }
        itemPrice = (double)(Math.round(itemPrice*100))/100;
    }
}
