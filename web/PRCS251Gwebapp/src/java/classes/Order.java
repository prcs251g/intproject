/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.util.ArrayList;
import java.util.List;

/**
 * Order --- program to put order items into an array list, price and quantity.
 * @author    Brian Young/Adam Burch
 */
public class Order 
{
    private static Order order = new Order();
    
    private Order() {}
    
    public static Order getInstance()
    {
        return order;
    }
    
    public List<OrderItem> orderItems = new ArrayList<OrderItem>();
    public double totalPrice;
    public int orderQuan;
}
