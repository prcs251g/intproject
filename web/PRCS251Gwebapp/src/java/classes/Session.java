/**
 *
 * @author bvyoung
 */
package classes;
import java.util.ArrayList;
import java.util.List;

/**
 * Session --- program to start the session.
 * @author    Brian Young/Adam Burch
 */

public class Session 
{
    public ArrayList orderIDs = new ArrayList<>();
    public List<OrderItem> orderItems = new ArrayList<OrderItem>();
    public double totalPrice;
    public int orderQuan;
    public int logged;
    public String username;
    public String id;
    public boolean invalidemail;
    public boolean invalidcity;
    public boolean missingfields;
    
    private static final Session sess = new Session();
    
    private Session(){}
    
    public static Session getInstance()
    {
        return sess;
    }
}
