function changePrice(sel,type)
{
	var tempPrice;
	var prices = [10.99,11.99,13.99,12.99,13.99,10.99,11.99,12.99,13.99,14.99,10.99,15.99];
	var elements = ["MargheritaPrice","PepperoniPrice","MeatFeastPrice","SpicyHotPrice","BBQSupremePrice","HawaiianPrice",
            "FarmhousePrice","MexicanSizzlerPrice","ChicagoDuluxePrice","MilanoPrice","ChickenDelightPrice","TheWorksPrice"];
	var Price = document.getElementById(elements[type]);
	if(sel.options[sel.selectedIndex].value === "Small")
	{
		Price.innerHTML = "&pound;" + prices[type];
	}else 
	if(sel.options[sel.selectedIndex].value === "Regular")
	{
		tempPrice = prices[type] + 2.00;
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}else
	if(sel.options[sel.selectedIndex].value === "Large")
	{
		tempPrice = prices[type] + 5.00;
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}	
}

function changePrice2(sel,type)
{
	var tempPrice;
	var prices = [4.99,3.99,3.99,3.99,1.99,1.99,1.99,1.99];
	var elements = ["GarlicBreadPrice","WingsPrice","StripsPrice","DoughPrice","CocaColaPrice","SpritePrice",
            "DrPepperPrice","TangoPrice"];
	var Price = document.getElementById(elements[type]);
	if(sel.options[sel.selectedIndex].value === "4pcs")
	{
                tempPrice = prices[type];
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}else 
        if(sel.options[sel.selectedIndex].value === "500ml Bottle")
	{
                tempPrice = prices[type];
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}else
	if(sel.options[sel.selectedIndex].value === "6pcs")
	{
		tempPrice = prices[type] + 1.00;
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}else 
	if(sel.options[sel.selectedIndex].value === "1.25l Bottle")
	{
		tempPrice = prices[type] + 1.00;
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}else
	if(sel.options[sel.selectedIndex].value === "8pcs")
	{
		tempPrice = prices[type] + 2.00;
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}else 
	if(sel.options[sel.selectedIndex].value === "2l Bottle")
	{
		tempPrice = prices[type] + 2.00;
                tempPrice = roundToTwoDC(tempPrice);
		Price.innerHTML = "&pound;" + tempPrice;
	}	
}

function roundToTwoDC(num) 
{    
    return +(Math.round(num + "e+2")  + "e-2");
}