<%-- 
    Document   : MakeCustomPizza
    Created on : 20-Apr-2017, 13:45:16
    Author     : bvyoung
--%>

<%@page import="classes.Session"%>
<%@page import="classes.OrderItem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="Classes.JSONParser"%>
<%@page import="Classes.JSONString"%>
<%@page import="Classes.RESTfulMethods"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Server</title>
    </head>
    <body>
        <% 
        RESTfulMethods rest = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/pizzas");
        RESTfulMethods restsize = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/itemsizes");
        JSONString js = new JSONString();
        JSONParser jp = new JSONParser(rest.GET());
        JSONParser jps = new JSONParser(restsize.GET());
        Session sess = Session.getInstance();
        jp.Parse();
        jps.Parse();
        Boolean f = true;
        int i = 0;
        while(f)
        {
            try
            {
                rest.GET("" + i);
                i++;
            }catch(RuntimeException e)
            {
                f = false;
                out.println(i);
            }
        }
        int id = i;  
        String name = "CUSTOM";
        out.println("New Pizza ID:" + id);
        out.println("New Pizza Name:" + name);
        String size = request.getParameter("size");
        out.println("Size id:" + size);
        String base = request.getParameter("base");
        out.println("Base id:" +base);
        String crust = request.getParameter("crust");
        out.println("Crust id:" +crust);
        String price = request.getParameter("price");
        price = price.substring(2);
        int topping1 = 0;
        int topping2 = 0;
        int topping3 = 0;
        int topping4 = 0;
        int topping5 = 0;
        
        js.AddElement("PIZZA_NAME", name);  
        js.AddElement("PIZZA_BASE_ID", Integer.parseInt(base));
        js.AddElement("PIZZA_CRUST_ID", Integer.parseInt(crust));
        js.AddElement("PIZZA_SIZE_ID", Integer.parseInt(size));
        js.AddElement("PIZZA_ID", id);
        int k = 1;
        for(int j = 1; j <= 36; j++)
        {
            String top = request.getParameter("topping" + j);
            if(top != null)
            {
                if(k == 1)
                {
                    js.AddElement("PIZZA_TOPPING_ONE_ID", j);
                    topping1 = j;
                    k++;
                }else
                if(k == 2)
                {
                    js.AddElement("PIZZA_TOPPING_TWO_ID", j);
                    topping2 = j;
                    k++;
                }else
                if(k == 3)
                {
                    js.AddElement("PIZZA_TOPPING_THREE_ID", j);
                    topping3 = j;
                    k++;
                }else
                if(k == 4)
                {
                    js.AddElement("PIZZA_TOPPING_FOUR_ID", j);
                    topping4 = j;
                    k++;
                }else
                if(k == 5)
                {
                    js.AddElement("PIZZA_TOPPING_FIVE_ID", j);
                    topping5 = j;
                    k++;
                }   
            }
        }
        js.AddElement("PIZZA_PRICE", Double.parseDouble(price));
        out.println(js.GetJSON());
        rest.POST(js.GetJSON());
        String itemsize;
        itemsize = jps.GetValue("SIZE_ID", size, "SIZE_NAME");
        OrderItem temp = new OrderItem(name, itemsize);
        temp.setItemType("PIZZA");
        temp.setItemPrice(Double.parseDouble(price));
        if(sess.orderItems.size() < 10)
        {
        sess.orderItems.add(temp);
        sess.totalPrice = sess.totalPrice + Double.parseDouble(price);
        sess.totalPrice = (double)(Math.round(sess.totalPrice*100))/100;
        sess.orderQuan = sess.orderQuan + 1;
        sess.orderIDs.add(id);
        }
        String redirectURL = "index.jsp";
        response.sendRedirect(redirectURL);
        %>
    </body>
</html>
