<%-- 
    Document   : blank
    Created on : 11-Apr-2017, 11:50:44
    Author     : aburch
--%>
<%@page import="javax.mail.internet.AddressException"%>
<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="classes.Session"%>
<%@page import="classes.StringHashGenerator"%>
<%@page import="Classes.JSONParser"%>
<%@page import="Classes.JSONString"%>
<%@page import="Classes.RESTfulMethods"%>
<%@page import="java.security.SecureRandom"%>
<%@page import="java.util.Base64"%>
<%@page import="java.util.Random"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Server</title>
<html>
    </head>
    <body>
        <%
           Session sess = Session.getInstance();
           RESTfulMethods rest = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/customers");
           RESTfulMethods restCity = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/cities");
           String pepper = "yoqy3Gu+AiSetUAjuNcdgmCE/ybCEFKJyVWKpW0Jb4c=";
           String cityjson = restCity.GET();
           out.println("rest =" + rest.GET());
           String forename = request.getParameter("forename");
           String surname = request.getParameter("surname");
           String address1 = request.getParameter("address1");
           String address2 = request.getParameter("address2");
           String postcode = request.getParameter("postcode");
           String email = request.getParameter("email");
           String city = request.getParameter("city");
           String mobile2 = request.getParameter("mobilenumber");
           String username = request.getParameter("username");
           String password = request.getParameter("password");
           boolean result = false;
           if(forename.equals("") || surname.equals("") || address1.equals("") || address2.equals("") || postcode.equals("") ||
                   email.equals("") || city.equals("") || mobile2.equals("") || username.equals("") || password.equals(""))
           {
               result = true;
           }
           if(result)
           {
               sess.missingfields =  true;
               String redirectURL = "Register.jsp";
               response.sendRedirect(redirectURL);
           }else
           {
           result = false;
           try 
           {
               InternetAddress emailAddr = new InternetAddress(email);
               emailAddr.validate();
           } catch (AddressException ex) 
           {
               result = true;
           }
           if(result)
           {
               sess.invalidemail =  true;
               String redirectURL = "Register.jsp";
               response.sendRedirect(redirectURL);
           }else
           {
           result = false;
           JSONParser jp = new JSONParser(cityjson);
           jp.Parse();
           System.out.println(jp.GetValue("CITY_NAME", "LONDON", "CITY_ID"));
           String cityid = jp.GetValue("CITY_NAME", city.toUpperCase(), "CITY_ID");
           double city_id = 0.0;
           try
           {
           city_id = Double.parseDouble(cityid);
           }catch(NumberFormatException ex)
           {
               result = true;
           }
           if(result)
           {
               sess.invalidcity =  true;
               String redirectURL = "Register.jsp";
               response.sendRedirect(redirectURL);
           }else
           {
           String mobile = "44";
           mobile = mobile.concat(mobile2);
           long mobilenumber = Long.parseLong(mobile);
           username = username.toLowerCase();
           StringHashGenerator hasher = new StringHashGenerator();
           final Random r = new SecureRandom();
           byte[] salt = new byte[32];
           r.nextBytes(salt);
           String encodedSalt = Base64.getEncoder().encodeToString(salt);
           System.out.println(encodedSalt);
           System.out.println(password);
           password = hasher.SHA512(password, encodedSalt);
           System.out.println(password);
           password = hasher.SHA512(password, pepper);
           System.out.println(password);
           JSONString js = new JSONString();
           js.AddElement("CUSTOMER_FORENAME", forename);
           js.AddElement("CUSTOMER_SURNAME", surname);
           js.AddElement("CUSTOMER_ADDRESS_ONE", address1);
           js.AddElement("CUSTOMER_ADDRESS_TWO", address2);
           js.AddElement("CUSTOMER_POSTCODE", postcode);
           js.AddElement("CUSTOMER_EMAIL", email);
           js.AddElement("CUSTOMER_MOBILE_NUMBER", mobilenumber);
           js.AddElement("CUSTOMER_USERNAME", username);
           js.AddElement("CUSTOMER_PASSWORD", password);
           js.AddElement("CUSTOMER_CITY_ID", city_id);
           js.AddElement("CUSTOMER_SALT", encodedSalt);
           Boolean f = true;
           int i = 0;
           while(f)
           {
               try
               {
                rest.GET("" + i);
                i++;
               }catch(RuntimeException ex)
               {
                   f = false;
                   out.println(i);
               }
           }
           js.AddElement("CUSTOMER_ID", "" + i);
           String json = js.GetJSON();
           System.out.println(json);
           rest.POST(json);
           sess.username = username;
           sess.id = "" + i;
           String redirectURL = "index.jsp";
           response.sendRedirect(redirectURL);
           }}}
        %>
    </body>
</html>
