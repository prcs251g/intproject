<%-- 
    Document   : login
    Created on : 19-Apr-2017, 10:51:48
    Author     : bvyoung
--%>
<%@page import="classes.Session"%>
<%@page import="classes.StringHashGenerator"%>
<%@page import="Classes.JSONParser"%>
<%@page import="Classes.JSONString"%>
<%@page import="Classes.RESTfulMethods"%>
<%@page import="java.security.SecureRandom"%>
<%@page import="java.util.Base64"%>
<%@page import="java.util.Random"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Server</title>
    </head>
    <body>
        <%
            Session sess = Session.getInstance();
            RESTfulMethods rest = new RESTfulMethods("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/customers");
            String json = rest.GET();
            JSONParser jp = new JSONParser(json);
            jp.Parse();
            String pepper = "yoqy3Gu+AiSetUAjuNcdgmCE/ybCEFKJyVWKpW0Jb4c=";
            String username = request.getParameter("username2");
            username = username.toLowerCase();
            String password = request.getParameter("password2");
            String salt = jp.GetValue("CUSTOMER_USERNAME", username, "CUSTOMER_SALT");
            String storedPassword = jp.GetValue("CUSTOMER_USERNAME",username, "CUSTOMER_PASSWORD");
            StringHashGenerator hasher = new StringHashGenerator();
            String saltedPassword = hasher.SHA512(password, salt);
            String pepperedPassword = hasher.SHA512(saltedPassword, pepper);
            if(storedPassword.equals(pepperedPassword))
            {
                out.println("This is amazing :)");
                sess.logged = 1;
                sess.username = username;
                sess.id = jp.GetValue("CUSTOMER_USERNAME",username,"CUSTOMER_ID");
                String redirectURL = "index.jsp";
                response.sendRedirect(redirectURL);
            }else
            {
                String redirectURL = "Register.jsp";
                response.sendRedirect(redirectURL);
            }
        %>
    </body>
</html>
