<%@page import="Classes.JSONParser"%>
<%@page import="Classes.RESTfulMethods"%>
<%@page import="classes.OrderItem"%>
<%@page import="classes.Session"%>
<%@page import="java.text.DecimalFormat"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>PRCS251G Project</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <script src="js/index.js"></script>
        <script src="jquery-3.1.1.min.js"></script>
    </head>
    <!-- code below taken from relevant java classes and API -->
    <%
        Session order = Session.getInstance();
        String ordered = request.getParameter("PlaceOrder");
        String logged = request.getParameter("register");
        if(logged != null)
        {
            order.logged = 1;
        }
        if(ordered != null)
        {
            order.totalPrice = 0.00;
            order.orderQuan = 0;
            order.orderItems.clear();
            order.orderIDs.clear();
            order.username = "";
            order.logged = 0;
            logged = null;
        }
        String Pizza = request.getParameter("Submit");
        String Size = request.getParameter(Pizza);
        if(Pizza != null)
            {
            System.out.println("foo");
            OrderItem temp = new OrderItem(Pizza, Size);
            System.out.println("hoo");
            if(order.orderItems.size() < 10)
            {
            order.orderItems.add(temp);
            order.totalPrice = order.totalPrice + temp.getItemPrice();
            order.totalPrice = (double)(Math.round(order.totalPrice*100))/100;
            order.orderQuan = order.orderQuan + 1;
            }
            RESTfulMethods rest = new RESTfulMethods();
            JSONParser jp;
            String id = "";
            if(temp.getItemType().equals("PIZZA"))
            {
                rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/pizzas");
                jp = new JSONParser(rest.GET());
                jp.Parse();
                id = jp.GetValue("PIZZA_NAME",Pizza.toUpperCase() , "PIZZA_ID");
            }else
            if(temp.getItemType().equals("SIDE"))
            {
                rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/sides");
                jp = new JSONParser(rest.GET());
                jp.Parse();
                id = jp.GetValue("SIDE_NAME",Pizza.toUpperCase() , "SIDE_ID");
                
            }else
            if(temp.getItemType().equals("DRINK"))
            {
                rest.setUrlString("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251g/api/drinks");
                jp = new JSONParser(rest.GET());
                jp.Parse();
                id = jp.GetValue("DRINK_NAME",Pizza.toUpperCase() , "DRINK_ID");
            }
            else
            {
                jp = new JSONParser("");
            }
            double intid = Double.parseDouble(id);
            if(Size.equals("Small"))
            {
                intid = intid-2;
                        
            }else
            if(Size.equals("Regular"))
            {
                intid = intid-1;
            }else
            if(Size.equals("4pcs"))
            {
                intid = intid-2;
            }else
            if(Size.equals("6pcs"))
            {
                intid = intid-1;
            }else
            if(Size.equals("500ml Bottle"))
            {
                intid = intid-2;
            }else
            if(Size.equals("1.25l Bottle"))
            {
                intid = intid-1;
            }
            System.out.println(intid);
            id = ""+intid;
            System.out.println(id);
            if(order.orderIDs.size() < 10)
            {
            order.orderIDs.add(id);
            }
            System.out.println(order.orderIDs);
            }
        %>
        <!-- Top of the page is uniform across all the pages for the sake of the style -->
    <body background = images/backingmain1.jpg style="background-size: 2000px 2000px; background-repeat: no-repeat; max-width: 2000px;overflow:scroll;">
        <div class="main">
    <div class="divmenu"><div class="h"><a href="custom.jsp">Create your own?<br>CLICK HERE!</a></div></div>
    <div class="divmenupicture"><img src="images/custompizza.png" height="100" width="100"></div>
    <div class="divlogo"><img src="images/Pizza Masterpiece Logo.png" height="200" width="200"></div>
    <!-- Total cost of what was selected, dynamic to change with the users choices -->
    <div class="divbasket"><div class="h"><a href="checkout.jsp">BASKET<%out.println("(" + order.orderQuan + ")");%></a></div><%out.println("Total order price: �" + order.totalPrice);%></div>
    <div class="divbasketpicture"><img src="images/shopcart.jpg" height="100" width="100"></div>
    <div class="divlogon"><div class="l"><a href="Register.jsp"><%if(order.logged == 0){out.println("Login/Register");}else if(order.logged == 1 ){out.println("Welcome, " + order.username);}%></a></div></div>   
    <div class="divpizza"></div>
    <!-- Divider followed by individual modules for all the set pizza menu items, all the same size with the same spacing -->
    <div class="divpizzatext"><div class="i" >Pizza</div></div>
    <div class="divitem1"><img src="images/Marg.jpg" height="205" width="275"></div>
    <div class="divitem1veg"><img src="images/vegetarian.png" height="45" width="47"></div>
    <div class="divitem1nut"><img src="images/nutfree.jpg" height="45" width="47"></div>
    <div class="divitem1subback"><img src="images/backing3.jpg" height="74" width="278"></div>
    <div class="divitem1sub"><div class="i" >Margherita</div>                
                <div class="i">
                <form name="pizza" action="index.jsp" method="post">
                <select name="Margherita" id="MargheritaOptions" onchange="changePrice(this,'0');">
                    
                    <option value="Small">Small - 10"(�10.99)</option>
                    <option value="Regular">Regular - 14"(�12.99)</option>
                    <option value="Large">Large - 18"(�15.99)</option>
                </select>
                <text id="MargheritaPrice">�10.99</text>    
                <button id="buttonadd" type="submit" value="Margherita" name="Submit">Add To Order</button>
                </form>   
                </div>
             </div>
    
    <div class="divitem2"><img src="images/pepperoni.jpg" height="205" width="275"></div>
         <div class="divitem2subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem2sub"><div class="i">Pepperoni</div>
                <div class="i">
                <form action="index.jsp" method="post">
                <select name="Pepperoni" id="PepperoniOptions" onchange="changePrice(this,'1');">
                    
                    <option value="Small">Small - 10"(�11.99)</option>
                    <option value="Regular">Regular - 14"(�13.99)</option>
                    <option value="Large">Large - 18"(�16.99)</option>
                </select>
                <price id="PepperoniPrice">�11.99</price>
                <button id="buttonadd" type="submit" value="Pepperoni" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
                
    <div class="divitem3"><img src="images/meat feast.jpg" height="205" width="275"></div>
         <div class="divitem3subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem3sub"><div class="i">Meat Feast</div>
                <div class="i">
                <form action="index.jsp" method="post">
                <select name="Meat Feast" id="MeatFeastOptions" onchange="changePrice(this,'2');">
                    
                    <option value="Small">Small - 10"(�13.99)</option>
                    <option value="Regular">Regular - 14"(�15.99)</option>
                    <option value="Large">Large - 18"(�18.99)</option>
                </select>
                    <price id="MeatFeastPrice">�13.99</price>
                <button id="buttonadd" type="submit" value="Meat Feast" name="Submit">Add To Order</button>
                </form>
                </div>
         </div>
    
    <div class="divitem4"><img src="images/spicy.jpg" height="205" width="275"></div>
    <div class="divitem4hot"><img src="images/hot.jpg" height="45" width="47"></div>
         <div class="divitem4subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem4sub"><div class="i">Spicy Hot</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Spicy Hot" id="SpicyHotOptions" onchange="changePrice(this,'3');">
                    
                    <option value="Small">Small - 10"(�12.99)</option>
                    <option value="Regular">Regular - 14"(�14.99)</option>
                    <option value="Large">Large - 18"(�17.99)</option>
                </select>
                <price id="SpicyHotPrice">�12.99</price> 
                <button id="buttonadd" type="submit" value="Spicy Hot" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
    
    <div class="divitem5"><img src="images/BBQ.jpg" height="205" width="275"></div>
            <div class="divitem5subback"><img src="images/backing3.jpg" height="74" width="278"></div>
            <div class="divitem5sub"><div class="i">BBQ Supreme</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="BBQ Supreme" id="BBQSupremeOptions" onChange="changePrice(this,'4');">
                    
                    <option value="Small">Small - 10"(�13.99)</option>
                    <option value="Regular">Regular - 14"(�15.99)</option>
                    <option value="Large">Large - 18"(�18.99)</option>
                </select>
                <price id="BBQSupremePrice">�13.99</price> 
                <button id="buttonadd" type="submit" value="BBQ Supreme" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem6"><img src="images/hawaiian.jpg" height="205" width="275"></div>
         <div class="divitem6subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem6sub"><div class="i">Hawaiian</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Hawaiian" id="HawaiianOptions" onChange="changePrice(this,'5');">
                    
                    <option value="Small">Small - 10"(�10.99)</option>
                    <option value="Regular">Regular - 14"(�12.99)</option>
                    <option value="Large">Large - 18"(�15.99)</option>
                </select>
                        <price id="HawaiianPrice">�10.99</price>
                <button id="buttonadd" type="submit" value="Hawaiian" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
    
    <div class="divitem7"><img src="images/farmhouse.jpg" height="205" width="275"></div>
         <div class="divitem7subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem7sub"><div class="i">Farmhouse</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Farmhouse" id="FarmhouseOptions" onChange="changePrice(this,'6');">
                    
                    <option value="Small">Small - 10"(�11.99)</option>
                    <option value="Regular">Regular - 14"(�13.99)</option>
                    <option value="Large">Large - 18"(�16.99)</option>
                </select>
                <price id="FarmhousePrice">�11.99</price>
                <button id="buttonadd" type="submit" value="Farmhouse" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem8"><img src="images/mexican.jpeg" height="205" width="275"></div>
    <div class="divitem8hot"><img src="images/hot.jpg" height="45" width="47"></div>
         <div class="divitem8subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem8sub"><div class="i">Mexican Sizzler</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Mexican Sizzler" id="MexicanSizzlerOptions" onChange="changePrice(this,'7');">
                    
                    <option value="Small">Small - 10"(�12.99)</option>
                    <option value="Regular">Regular - 14"(�14.99)</option>
                    <option value="Large">Large - 18"(�17.99)</option>
                </select>
                <price id="MexicanSizzlerPrice">�12.99</price>
                <button id="buttonadd" type="submit" value="Mexican Sizzler" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem9"><img src="images/chicago.jpg" height="205" width="275"></div>
    <div class="divitem9veg"><img src="images/vegetarian.png" height="45" width="47"></div>
         <div class="divitem9subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem9sub"><div class="i">Chicago Duluxe</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Chicago" id="ChicagoDuluxeOptions" onChange="changePrice(this,'8');">
                    
                    <option value="Small">Small - 10"(�13.99)</option>
                    <option value="Regular">Regular - 14"(�15.99)</option>
                    <option value="Large">Large - 18"(�18.99)</option>
                </select>
                <price id="ChicagoDuluxePrice">�13.99 
                <button id="buttonadd" type="submit" value="Chicago" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
    
    <div class="divitem10"><img src="images/milan.jpg" height="205" width="275"></div>
    <div class="divitem10veg"><img src="images/vegetarian.png" height="45" width="47"></div>
         <div class="divitem10subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem10sub"><div class="i">Milano</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Milano" id="MilanoOptions" onChange="changePrice(this,'9');">
                    
                    <option value="Small">Small - 10"(�14.99)</option>
                    <option value="Regular">Regular - 14"(�16.99)</option>
                    <option value="Large">Large - 18"(�19.99)</option>
                </select>
                        <price id="MilanoPrice">�14.99</price>
                <button id="buttonadd" type="submit"  value="Milano" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem11"><img src="images/chicken.jpg" height="205" width="275"></div>
         <div class="divitem11subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem11sub"><div class="i">Chicken Delight</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Chicken Delight" id="ChickenDelightOptions" onChange="changePrice(this,'10');">
                    
                    <option value="Small">Small - 10"(�10.99)</option>
                    <option value="Regular">Regular - 14"(�12.99)</option>
                    <option value="Large">Large - 18"(�15.99)</option>
                </select>
                        <price id="ChickenDelightPrice">�10.99</price>
                <button id="buttonadd" type="submit" value="Chicken Delight" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem12"><img src="images/works.JPG" height="205" width="275"></div>
         <div class="divitem12subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem12sub"><div class="i">The Works</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="The Works" id="TheWorksOptions" onChange="changePrice(this,'11');">
                    
                    <option value="Small">Small - 10"(�15.99)</option>
                    <option value="Regular">Regular - 14"(�17.99)</option>
                    <option value="Large">Large - 18"(�20.99)</option>
                </select>
                    <price id="TheWorksPrice">�15.99</price> 
                <button id="buttonadd" type="submit" value="The Works" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
    
    <!-- Divider followed by individual modules for all the side menu items, all the same size with the same spacing -->
    <div class="divsides"></div>
    <div class="divsidestext"><div class="i" >Sides</div></div>
    
    <div class="divitem13"><img src="images/garlicbread.jpg" height="205" width="275"></div>
    <div class="divitem13veg"><img src="images/vegetarian.png" height="45" width="47"></div>
         <div class="divitem13subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem13sub"><div class="i">Garlic Bread</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Garlic Bread" id="GarlicBreadOptions" onChange="changePrice2(this,'0');">
                    <option value="4pcs">Small - 4pcs(�4.99)</option>
                    <option value="6pcs">Regular - 6pcs(�5.99)</option>
                    <option value="8pcs">Large - 8pcs(�6.99)</option>
                </select>
                <price id="GarlicBreadPrice">�4.99</price>
                <button id="buttonadd" type="submit" value="Garlic Bread" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
    
    <div class="divitem14"><img src="images/wings.jpg" height="205" width="275"></div>
    <div class="divitem14glut"><img src="images/glutenfree.jpg" height="45" width="47"></div>
         <div class="divitem14subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem14sub"><div class="i">Chicken Wings</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Chicken Wing" id="WingsOptions" onChange="changePrice2(this,'1');">
                    <option value="4pcs">Small - 4pcs(�3.99)</option>
                    <option value="6pcs">Regular - 6pcs(�4.99)</option>
                    <option value="8pcs">Large - 8pcs(�5.99)</option>
                </select>
                <price id="WingsPrice">�3.99</price>
                <button id="buttonadd" type="submit" value="Chicken Wing" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
    
    <div class="divitem15"><img src="images/strips.jpg" height="205" width="275"></div>
         <div class="divitem15subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem15sub"><div class="i">Chicken Strips</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Chicken Strips" id="StripsOptions" onChange="changePrice2(this,'2');">
                    <option value="4pcs">Small - 4pcs(�3.99)</option>
                    <option value="6pcs">Regular - 6pcs(�4.99)</option>
                    <option value="8pcs">Large - 8pcs(�5.99)</option>
                </select>
                <price id="StripsPrice">�3.99</price>
                <button id="buttonadd" type="submit" value="Chicken Strips" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem16"><img src="images/dough.jpg" height="205" width="275"></div>
    <div class="divitem16veg"><img src="images/vegetarian.png" height="45" width="47"></div>
         <div class="divitem16subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem16sub"><div class="i">Dough Balls</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Dough Balls" id="DoughOptions" onChange="changePrice2(this,'3');">
                    <option value="4pcs">Small - 4pcs(�3.99)</option>
                    <option value="6pcs">Regular - 6pcs(�4.99)</option>
                    <option value="8pcs">Large - 8pcs(�5.99)</option>
                </select>
                <price id="DoughPrice">�3.99</price>
                <button id="buttonadd" type="submit" value="Dough Balls" name="Submit">Add To Order</button>
                </form>
                </div>
             </div>
    
    <!-- Divider followed by individual modules for all the drink menu items, all the same size with the same spacing -->
    <div class="divdrinks"></div>
    <div class="divdrinkstext"><div class="i" >Drinks</div></div>
    
    <div class="divitem17"><img src="images/cocacola.jpg" height="205" width="275"></div>
         <div class="divitem17subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem17sub"><div class="i">Coca-Cola</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Coca Cola" id="CocaColaOptions" onChange="changePrice2(this,'4');">
                    <option value="500ml Bottle">500ml Bottle(�1.99)</option>
                    <option value="1.25l Bottle">1.25l Bottle(�2.99)</option>
                    <option value="2l Bottle">2l Bottle(�3.99)</option>
                </select>
                <price id="CocaColaPrice">�1.99</price>
                <button id="buttonadd" type="submit" value="Coca Cola" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem18"><img src="images/sprite.jpg" height="205" width="275"></div>
         <div class="divitem18subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem18sub"><div class="i">Sprite</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Sprite" id="SpriteOptions" onChange="changePrice2(this,'5');">
                    <option value="500ml Bottle">500ml Bottle(�1.99)</option>
                    <option value="1.25l Bottle">1.25l Bottle(�2.99)</option>
                    <option value="2l Bottle">2l Bottle(�3.99)</option>
                </select>
                <price id="SpritePrice">�1.99</price>
                <button id="buttonadd" type="submit" value="Sprite" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem19"><img src="images/drpepper.jpg" height="205" width="275"></div>
         <div class="divitem19subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem19sub"><div class="i">Dr Pepper</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Dr Pepper" id="DrPepperOptions" onChange="changePrice2(this,'6');">
                    <option value="500ml Bottle">500ml Bottle(�1.99)</option>
                    <option value="1.25l Bottle">1.25l Bottle(�2.99)</option>
                    <option value="2l Bottle">2l Bottle(�3.99)</option>
                </select>
                <price id="DrPepperPrice">�1.99</price>
                <button id="buttonadd" type="submit" value="Dr Pepper" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
    
    <div class="divitem20"><img src="images/tango.jpg" height="205" width="275"></div>
         <div class="divitem20subback"><img src="images/backing3.jpg" height="74" width="278"></div>
         <div class="divitem20sub"><div class="i">Tango</div>
                <div class="i">
                    <form action="index.jsp" method="post">
                <select name="Tango" id="TangoOptions" onChange="changePrice2(this,'7');">
                    <option value="500ml Bottle">500ml Bottle(�1.99)</option>
                    <option value="1.25l Bottle">1.25l Bottle(�2.99)</option>
                    <option value="2l Bottle">2l Bottle(�3.99)</option>
                </select>
                <price id="TangoPrice">�1.99</price>
                <button id="buttonadd" type="submit" value="Tango" name="Submit">Add To Order</button>
                    </form>
                </div>
             </div>
        </div>
        
    </body>
 
</html>

