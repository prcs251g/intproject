﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class EMPLOYEEJOBsController : ApiController
    {
        private Entities4 db = new Entities4();

        public EMPLOYEEJOBsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/EMPLOYEEJOBs
        public IQueryable<EMPLOYEEJOB> GetEMPLOYEEJOBs()
        {
            return db.EMPLOYEEJOBs;
        }

        // GET: api/EMPLOYEEJOBs/5
        [ResponseType(typeof(EMPLOYEEJOB))]
        public IHttpActionResult GetEMPLOYEEJOB(decimal id)
        {
            EMPLOYEEJOB eMPLOYEEJOB = db.EMPLOYEEJOBs.Find(id);
            if (eMPLOYEEJOB == null)
            {
                return NotFound();
            }

            return Ok(eMPLOYEEJOB);
        }

        // PUT: api/EMPLOYEEJOBs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutEMPLOYEEJOB(decimal id, EMPLOYEEJOB eMPLOYEEJOB)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != eMPLOYEEJOB.JOB_ID)
            {
                return BadRequest();
            }

            db.Entry(eMPLOYEEJOB).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EMPLOYEEJOBExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/EMPLOYEEJOBs
        [ResponseType(typeof(EMPLOYEEJOB))]
        public IHttpActionResult PostEMPLOYEEJOB(EMPLOYEEJOB eMPLOYEEJOB)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.EMPLOYEEJOBs.Add(eMPLOYEEJOB);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (EMPLOYEEJOBExists(eMPLOYEEJOB.JOB_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = eMPLOYEEJOB.JOB_ID }, eMPLOYEEJOB);
        }

        // DELETE: api/EMPLOYEEJOBs/5
        [ResponseType(typeof(EMPLOYEEJOB))]
        public IHttpActionResult DeleteEMPLOYEEJOB(decimal id)
        {
            EMPLOYEEJOB eMPLOYEEJOB = db.EMPLOYEEJOBs.Find(id);
            if (eMPLOYEEJOB == null)
            {
                return NotFound();
            }

            db.EMPLOYEEJOBs.Remove(eMPLOYEEJOB);
            db.SaveChanges();

            return Ok(eMPLOYEEJOB);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool EMPLOYEEJOBExists(decimal id)
        {
            return db.EMPLOYEEJOBs.Count(e => e.JOB_ID == id) > 0;
        }
    }
}