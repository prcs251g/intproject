﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ORDERsController : ApiController
    {
        private Entities4 db = new Entities4();

        public ORDERsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/ORDERs
        public IQueryable<ORDER> GetORDERS()
        {
            return db.ORDERS;
        }

        // GET: api/ORDERs/5
        [ResponseType(typeof(ORDER))]
        public IHttpActionResult GetORDER(decimal id)
        {
            ORDER oRDER = db.ORDERS.Find(id);
            if (oRDER == null)
            {
                return NotFound();
            }

            return Ok(oRDER);
        }

        // PUT: api/ORDERs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutORDER(decimal id, ORDER oRDER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != oRDER.ORDER_ID)
            {
                return BadRequest();
            }

            db.Entry(oRDER).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDERExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ORDERs
        [ResponseType(typeof(ORDER))]
        public IHttpActionResult PostORDER(ORDER oRDER)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ORDERS.Add(oRDER);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ORDERExists(oRDER.ORDER_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = oRDER.ORDER_ID }, oRDER);
        }

        // DELETE: api/ORDERs/5
        [ResponseType(typeof(ORDER))]
        public IHttpActionResult DeleteORDER(decimal id)
        {
            ORDER oRDER = db.ORDERS.Find(id);
            if (oRDER == null)
            {
                return NotFound();
            }

            db.ORDERS.Remove(oRDER);
            db.SaveChanges();

            return Ok(oRDER);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ORDERExists(decimal id)
        {
            return db.ORDERS.Count(e => e.ORDER_ID == id) > 0;
        }
    }
}