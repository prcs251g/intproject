﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class CRUSTsController : ApiController
    {
        private Entities4 db = new Entities4();

        public CRUSTsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/CRUSTs
        public IQueryable<CRUST> GetCRUSTs()
        {
            return db.CRUSTs;
        }

        // GET: api/CRUSTs/5
        [ResponseType(typeof(CRUST))]
        public IHttpActionResult GetCRUST(decimal id)
        {
            CRUST cRUST = db.CRUSTs.Find(id);
            if (cRUST == null)
            {
                return NotFound();
            }

            return Ok(cRUST);
        }

        // PUT: api/CRUSTs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCRUST(decimal id, CRUST cRUST)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cRUST.CRUST_ID)
            {
                return BadRequest();
            }

            db.Entry(cRUST).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CRUSTExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CRUSTs
        [ResponseType(typeof(CRUST))]
        public IHttpActionResult PostCRUST(CRUST cRUST)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CRUSTs.Add(cRUST);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CRUSTExists(cRUST.CRUST_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cRUST.CRUST_ID }, cRUST);
        }

        // DELETE: api/CRUSTs/5
        [ResponseType(typeof(CRUST))]
        public IHttpActionResult DeleteCRUST(decimal id)
        {
            CRUST cRUST = db.CRUSTs.Find(id);
            if (cRUST == null)
            {
                return NotFound();
            }

            db.CRUSTs.Remove(cRUST);
            db.SaveChanges();

            return Ok(cRUST);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CRUSTExists(decimal id)
        {
            return db.CRUSTs.Count(e => e.CRUST_ID == id) > 0;
        }
    }
}