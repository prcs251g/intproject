﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class TOPPINGsController : ApiController
    {
        private Entities4 db = new Entities4();

        public TOPPINGsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/TOPPINGs
        public IQueryable<TOPPING> GetTOPPINGs()
        {
            return db.TOPPINGs;
        }

        // GET: api/TOPPINGs/5
        [ResponseType(typeof(TOPPING))]
        public IHttpActionResult GetTOPPING(decimal id)
        {
            TOPPING tOPPING = db.TOPPINGs.Find(id);
            if (tOPPING == null)
            {
                return NotFound();
            }

            return Ok(tOPPING);
        }

        // PUT: api/TOPPINGs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTOPPING(decimal id, TOPPING tOPPING)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tOPPING.TOPPING_ID)
            {
                return BadRequest();
            }

            db.Entry(tOPPING).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TOPPINGExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TOPPINGs
        [ResponseType(typeof(TOPPING))]
        public IHttpActionResult PostTOPPING(TOPPING tOPPING)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TOPPINGs.Add(tOPPING);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TOPPINGExists(tOPPING.TOPPING_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tOPPING.TOPPING_ID }, tOPPING);
        }

        // DELETE: api/TOPPINGs/5
        [ResponseType(typeof(TOPPING))]
        public IHttpActionResult DeleteTOPPING(decimal id)
        {
            TOPPING tOPPING = db.TOPPINGs.Find(id);
            if (tOPPING == null)
            {
                return NotFound();
            }

            db.TOPPINGs.Remove(tOPPING);
            db.SaveChanges();

            return Ok(tOPPING);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TOPPINGExists(decimal id)
        {
            return db.TOPPINGs.Count(e => e.TOPPING_ID == id) > 0;
        }
    }
}