﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ITEMSIZEsController : ApiController
    {
        private Entities4 db = new Entities4();

        public ITEMSIZEsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/ITEMSIZEs
        public IQueryable<ITEMSIZE> GetITEMSIZEs()
        {
            return db.ITEMSIZEs;
        }

        // GET: api/ITEMSIZEs/5
        [ResponseType(typeof(ITEMSIZE))]
        public IHttpActionResult GetITEMSIZE(decimal id)
        {
            ITEMSIZE iTEMSIZE = db.ITEMSIZEs.Find(id);
            if (iTEMSIZE == null)
            {
                return NotFound();
            }

            return Ok(iTEMSIZE);
        }

        // PUT: api/ITEMSIZEs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutITEMSIZE(decimal id, ITEMSIZE iTEMSIZE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != iTEMSIZE.SIZE_ID)
            {
                return BadRequest();
            }

            db.Entry(iTEMSIZE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ITEMSIZEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ITEMSIZEs
        [ResponseType(typeof(ITEMSIZE))]
        public IHttpActionResult PostITEMSIZE(ITEMSIZE iTEMSIZE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ITEMSIZEs.Add(iTEMSIZE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ITEMSIZEExists(iTEMSIZE.SIZE_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = iTEMSIZE.SIZE_ID }, iTEMSIZE);
        }

        // DELETE: api/ITEMSIZEs/5
        [ResponseType(typeof(ITEMSIZE))]
        public IHttpActionResult DeleteITEMSIZE(decimal id)
        {
            ITEMSIZE iTEMSIZE = db.ITEMSIZEs.Find(id);
            if (iTEMSIZE == null)
            {
                return NotFound();
            }

            db.ITEMSIZEs.Remove(iTEMSIZE);
            db.SaveChanges();

            return Ok(iTEMSIZE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ITEMSIZEExists(decimal id)
        {
            return db.ITEMSIZEs.Count(e => e.SIZE_ID == id) > 0;
        }
    }
}