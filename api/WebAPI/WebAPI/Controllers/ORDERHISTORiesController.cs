﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ORDERHISTORiesController : ApiController
    {
        private Entities4 db = new Entities4();

        public ORDERHISTORiesController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/ORDERHISTORies
        public IQueryable<ORDERHISTORY> GetORDERHISTORies()
        {
            return db.ORDERHISTORies;
        }

        // GET: api/ORDERHISTORies/5
        [ResponseType(typeof(ORDERHISTORY))]
        public IHttpActionResult GetORDERHISTORY(decimal id)
        {
            ORDERHISTORY oRDERHISTORY = db.ORDERHISTORies.Find(id);
            if (oRDERHISTORY == null)
            {
                return NotFound();
            }

            return Ok(oRDERHISTORY);
        }

        // PUT: api/ORDERHISTORies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutORDERHISTORY(decimal id, ORDERHISTORY oRDERHISTORY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != oRDERHISTORY.ORDER_HISTORY_ID)
            {
                return BadRequest();
            }

            db.Entry(oRDERHISTORY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDERHISTORYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ORDERHISTORies
        [ResponseType(typeof(ORDERHISTORY))]
        public IHttpActionResult PostORDERHISTORY(ORDERHISTORY oRDERHISTORY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ORDERHISTORies.Add(oRDERHISTORY);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ORDERHISTORYExists(oRDERHISTORY.ORDER_HISTORY_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = oRDERHISTORY.ORDER_HISTORY_ID }, oRDERHISTORY);
        }

        // DELETE: api/ORDERHISTORies/5
        [ResponseType(typeof(ORDERHISTORY))]
        public IHttpActionResult DeleteORDERHISTORY(decimal id)
        {
            ORDERHISTORY oRDERHISTORY = db.ORDERHISTORies.Find(id);
            if (oRDERHISTORY == null)
            {
                return NotFound();
            }

            db.ORDERHISTORies.Remove(oRDERHISTORY);
            db.SaveChanges();

            return Ok(oRDERHISTORY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ORDERHISTORYExists(decimal id)
        {
            return db.ORDERHISTORies.Count(e => e.ORDER_HISTORY_ID == id) > 0;
        }
    }
}