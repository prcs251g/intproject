﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class BASEsController : ApiController
    {
        private Entities4 db = new Entities4();

        public BASEsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/BASEs
        public IQueryable<BASE> GetBASEs()
        {
            return db.BASEs;
        }

        // GET: api/BASEs/5
        [ResponseType(typeof(BASE))]
        public IHttpActionResult GetBASE(decimal id)
        {
            BASE bASE = db.BASEs.Find(id);
            if (bASE == null)
            {
                return NotFound();
            }

            return Ok(bASE);
        }

        // PUT: api/BASEs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBASE(decimal id, BASE bASE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != bASE.BASE_ID)
            {
                return BadRequest();
            }

            db.Entry(bASE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BASEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/BASEs
        [ResponseType(typeof(BASE))]
        public IHttpActionResult PostBASE(BASE bASE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.BASEs.Add(bASE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (BASEExists(bASE.BASE_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = bASE.BASE_ID }, bASE);
        }

        // DELETE: api/BASEs/5
        [ResponseType(typeof(BASE))]
        public IHttpActionResult DeleteBASE(decimal id)
        {
            BASE bASE = db.BASEs.Find(id);
            if (bASE == null)
            {
                return NotFound();
            }

            db.BASEs.Remove(bASE);
            db.SaveChanges();

            return Ok(bASE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BASEExists(decimal id)
        {
            return db.BASEs.Count(e => e.BASE_ID == id) > 0;
        }
    }
}