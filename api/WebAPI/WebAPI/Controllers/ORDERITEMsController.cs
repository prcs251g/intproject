﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class ORDERITEMsController : ApiController
    {
        private Entities4 db = new Entities4();

        public ORDERITEMsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/ORDERITEMs
        public IQueryable<ORDERITEM> GetORDERITEMs()
        {
            return db.ORDERITEMs;
        }

        // GET: api/ORDERITEMs/5
        [ResponseType(typeof(ORDERITEM))]
        public IHttpActionResult GetORDERITEM(decimal id)
        {
            ORDERITEM oRDERITEM = db.ORDERITEMs.Find(id);
            if (oRDERITEM == null)
            {
                return NotFound();
            }

            return Ok(oRDERITEM);
        }

        // PUT: api/ORDERITEMs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutORDERITEM(decimal id, ORDERITEM oRDERITEM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != oRDERITEM.ORDER_ITEM_ID)
            {
                return BadRequest();
            }

            db.Entry(oRDERITEM).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ORDERITEMExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/ORDERITEMs
        [ResponseType(typeof(ORDERITEM))]
        public IHttpActionResult PostORDERITEM(ORDERITEM oRDERITEM)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.ORDERITEMs.Add(oRDERITEM);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (ORDERITEMExists(oRDERITEM.ORDER_ITEM_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = oRDERITEM.ORDER_ITEM_ID }, oRDERITEM);
        }

        // DELETE: api/ORDERITEMs/5
        [ResponseType(typeof(ORDERITEM))]
        public IHttpActionResult DeleteORDERITEM(decimal id)
        {
            ORDERITEM oRDERITEM = db.ORDERITEMs.Find(id);
            if (oRDERITEM == null)
            {
                return NotFound();
            }

            db.ORDERITEMs.Remove(oRDERITEM);
            db.SaveChanges();

            return Ok(oRDERITEM);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ORDERITEMExists(decimal id)
        {
            return db.ORDERITEMs.Count(e => e.ORDER_ITEM_ID == id) > 0;
        }
    }
}