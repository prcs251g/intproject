﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class CITiesController : ApiController
    {
        private Entities4 db = new Entities4();

        public CITiesController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/CITies
        public IQueryable<CITY> GetCITies()
        {
            return db.CITies;
        }

        // GET: api/CITies/5
        [ResponseType(typeof(CITY))]
        public IHttpActionResult GetCITY(decimal id)
        {
            CITY cITY = db.CITies.Find(id);
            if (cITY == null)
            {
                return NotFound();
            }

            return Ok(cITY);
        }

        // PUT: api/CITies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCITY(decimal id, CITY cITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cITY.CITY_ID)
            {
                return BadRequest();
            }

            db.Entry(cITY).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CITYExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CITies
        [ResponseType(typeof(CITY))]
        public IHttpActionResult PostCITY(CITY cITY)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.CITies.Add(cITY);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (CITYExists(cITY.CITY_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = cITY.CITY_ID }, cITY);
        }

        // DELETE: api/CITies/5
        [ResponseType(typeof(CITY))]
        public IHttpActionResult DeleteCITY(decimal id)
        {
            CITY cITY = db.CITies.Find(id);
            if (cITY == null)
            {
                return NotFound();
            }

            db.CITies.Remove(cITY);
            db.SaveChanges();

            return Ok(cITY);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CITYExists(decimal id)
        {
            return db.CITies.Count(e => e.CITY_ID == id) > 0;
        }
    }
}