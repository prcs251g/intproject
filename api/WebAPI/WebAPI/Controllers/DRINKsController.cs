﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class DRINKsController : ApiController
    {
        private Entities4 db = new Entities4();

        public DRINKsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/DRINKs
        public IQueryable<DRINK> GetDRINKS()
        {
            return db.DRINKS;
        }

        // GET: api/DRINKs/5
        [ResponseType(typeof(DRINK))]
        public IHttpActionResult GetDRINK(decimal id)
        {
            DRINK dRINK = db.DRINKS.Find(id);
            if (dRINK == null)
            {
                return NotFound();
            }

            return Ok(dRINK);
        }

        // PUT: api/DRINKs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDRINK(decimal id, DRINK dRINK)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != dRINK.DRINK_ID)
            {
                return BadRequest();
            }

            db.Entry(dRINK).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DRINKExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DRINKs
        [ResponseType(typeof(DRINK))]
        public IHttpActionResult PostDRINK(DRINK dRINK)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DRINKS.Add(dRINK);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (DRINKExists(dRINK.DRINK_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = dRINK.DRINK_ID }, dRINK);
        }

        // DELETE: api/DRINKs/5
        [ResponseType(typeof(DRINK))]
        public IHttpActionResult DeleteDRINK(decimal id)
        {
            DRINK dRINK = db.DRINKS.Find(id);
            if (dRINK == null)
            {
                return NotFound();
            }

            db.DRINKS.Remove(dRINK);
            db.SaveChanges();

            return Ok(dRINK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DRINKExists(decimal id)
        {
            return db.DRINKS.Count(e => e.DRINK_ID == id) > 0;
        }
    }
}