﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class SIDEsController : ApiController
    {
        private Entities4 db = new Entities4();

        public SIDEsController()
        {
            db.Configuration.ProxyCreationEnabled = false;
        }

        // GET: api/SIDEs
        public IQueryable<SIDE> GetSIDES()
        {
            return db.SIDES;
        }

        // GET: api/SIDEs/5
        [ResponseType(typeof(SIDE))]
        public IHttpActionResult GetSIDE(decimal id)
        {
            SIDE sIDE = db.SIDES.Find(id);
            if (sIDE == null)
            {
                return NotFound();
            }

            return Ok(sIDE);
        }

        // PUT: api/SIDEs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSIDE(decimal id, SIDE sIDE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != sIDE.SIDE_ID)
            {
                return BadRequest();
            }

            db.Entry(sIDE).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SIDEExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SIDEs
        [ResponseType(typeof(SIDE))]
        public IHttpActionResult PostSIDE(SIDE sIDE)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.SIDES.Add(sIDE);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (SIDEExists(sIDE.SIDE_ID))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = sIDE.SIDE_ID }, sIDE);
        }

        // DELETE: api/SIDEs/5
        [ResponseType(typeof(SIDE))]
        public IHttpActionResult DeleteSIDE(decimal id)
        {
            SIDE sIDE = db.SIDES.Find(id);
            if (sIDE == null)
            {
                return NotFound();
            }

            db.SIDES.Remove(sIDE);
            db.SaveChanges();

            return Ok(sIDE);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SIDEExists(decimal id)
        {
            return db.SIDES.Count(e => e.SIDE_ID == id) > 0;
        }
    }
}