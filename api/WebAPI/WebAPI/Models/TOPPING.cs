//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TOPPING
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TOPPING()
        {
            this.PIZZAs = new HashSet<PIZZA>();
            this.PIZZAs1 = new HashSet<PIZZA>();
            this.PIZZAs2 = new HashSet<PIZZA>();
            this.PIZZAs3 = new HashSet<PIZZA>();
            this.PIZZAs4 = new HashSet<PIZZA>();
        }
    
        public decimal TOPPING_ID { get; set; }
        public string TOPPING_NAME { get; set; }
        public Nullable<decimal> TOPPING_PRICE { get; set; }
        public Nullable<decimal> TOPPING_CALORIES { get; set; }
        public string VEG { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PIZZA> PIZZAs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PIZZA> PIZZAs1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PIZZA> PIZZAs2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PIZZA> PIZZAs3 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PIZZA> PIZZAs4 { get; set; }
    }
}
